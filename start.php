<?php
/**
 * Zircon CMS - A PHP CMS For Web Artisans
 *
 * @package  CMS
 * @version  3.0
 * @author   Ramon Barros <contato@ramon-barros.com>
 * @link     http://ramon-barros.com
 */

/*
|--------------------------------------------------------------------------
| Start Session CLI
|--------------------------------------------------------------------------
|
| To run tests must begin Session for mistakes
|
*/
if (Request::cli() and Config::get('session.driver') !== '')
{
    Session::load();
}

/*
|--------------------------------------------------------------------------
| Auto-Loader Mappings
|--------------------------------------------------------------------------
|
| Laravel uses a simple array of class to path mappings to drive the class
| auto-loader. This simple approach helps avoid the performance problems
| of searching through directories by convention.
|
| Registering a mapping couldn't be easier. Just pass an array of class
| to path maps into the "map" function of Autoloader. Then, when you
| want to use that class, just use it. It's simple!
|
*/

Autoloader::map(array(

));

/*
|--------------------------------------------------------------------------
| Auto-Loader Directories
|--------------------------------------------------------------------------
|
| The Laravel auto-loader can search directories for files using the PSR-0
| naming convention. This convention basically organizes classes by using
| the class namespace to indicate the directory structure.
|
*/

Autoloader::directories(array(
    Bundle::path('zircon').'models',
    Bundle::path('zircon').'models'.DS.'admin',
    Bundle::path('zircon').'models'.DS.'modulos',
));

/*
|--------------------------------------------------------------------------
| Namespaces
|--------------------------------------------------------------------------
|
*/
Autoloader::namespaces(array(
    'Dash' => Bundle::path('zircon').'libraries'.DS.'Dash',
    'Core' => Bundle::path('zircon').'libraries'.DS.'Core',
));

Autoloader::alias('Core\\Config','Zircon_Config');
Autoloader::alias('Core\\Model\\Eloquent', 'Eloquent');
Autoloader::alias('Core\\Form\\Form', 'Form');
Autoloader::alias('Core\\Generator\\Migration','Zircon_Migration');
Autoloader::alias('Core\\Base\\Controller', 'Zircon_Base_Controller');
Autoloader::alias('Core\\Base\\CommentParser','CommentParser');
Autoloader::alias('Core\\Model\\Modulo', 'Zircon_Model_Modulo');
Autoloader::alias('Core\\Model\\ModuloInstall', 'Zircon_Modulo_Install');
Autoloader::alias('Core\\Model\\Users', 'Zircon_Model_Users');