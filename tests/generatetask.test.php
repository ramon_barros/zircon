<?php
/**
* Admin Generate Task Test
*
* @category Tests
*
*/

require_once Bundle::path('zircon') . '/tasks/generate.php';

class TestAdminGenerateTask extends AbstractDataAccessTest
{

  public static $model;
  public static $controller;
  public static $migration;
  public static $view;
  public static $test;

  public function setup()
  {
    // We don't care about the echos
    ob_start();

    self::$controller = Bundle::path('zircon') . 'controllers'.DS.'modulos'.DS;
    self::$model = Bundle::path('zircon') . 'models'.DS.'modulos'.DS;
    self::$view = Bundle::path('zircon') . 'views'.DS.Config::get("zircon::settings.view").DS.Config::get("zircon::settings.template").DS;
    self::$migration = Bundle::path('zircon') . 'migrations'.DS;
    self::$test = Bundle::path('zircon') . 'tests'.DS;

    $this->generate = new Zircon_Generate_Task;
  }

  public function assertPreConditions()
  {	
      $this->assertTrue(
              class_exists($class = 'Zircon_Generate_Task'),
              'Class not found: '.$class
      );
  }

  // @group controller
  public function testCreateControllerFiles()
  {
    $this->generate->controller(array(
      'TestGenerate'
    ));

    $file = File::latest(self::$controller);
    $this->assertFileExists((string)$file);
    File::delete((string)$file);
  }

  // @group controller
  public function testCommandCreateControllerFiles()
  {
    
    \Laravel\CLI\Command::run(
      array(
          'zircon::generate:controller' 
          ,'TestGenerate'
      ));

    $file = File::latest(self::$controller);
    $this->assertFileExists((string)$file);
    File::delete((string)$file);
  }

  // @group model
  public function testCreateModelFiles()
  {
    $this->generate->model(array(
      'TestGenerate'
    ));

    $file = File::latest(self::$model);
    $this->assertFileExists((string)$file);
    File::delete((string)$file);
  }

  // @group model
  public function testCreateModelWithMethodsFiles()
  {
    $this->generate->model(array(
      'TestGenerate'
      ,'getTest:private'
      ,'getTestTest:public:static'
    ));

    $file = File::latest(self::$model);
    $this->assertFileExists((string)$file);
    $contents = (string)File::get($file);
    $this->assertContains("private function getTest()", $contents);
    $this->assertContains("public static function getTestTest()", $contents);
    File::delete((string)$file);
  }

  // @group model
  public function testCommandCreateModelFiles()
  {
    
    \Laravel\CLI\Command::run(
      array(
          'zircon::generate:model' 
          ,'TestGenerate'
      ));

    $file = File::latest(self::$model);
    $this->assertFileExists((string)$file);
    File::delete((string)$file);
  }

  // @group view
  public function testCreateViewFiles()
  {
    $this->generate->view(array(
       'test.index'
       ,'test.zircon.show'
       ,'test'
    ));

    $this->assertFileExists(self::$view . 'test/index.blade.php');
    $this->assertFileExists(self::$view . 'test/zircon/show.blade.php');
    $this->assertFileExists(self::$view . 'test.blade.php');
    
    File::delete(self::$view . 'test/index.blade.php');
    File::delete(self::$view . 'test/zircon/show.blade.php');
    File::delete(self::$view . 'test.blade.php');
    File::rmdir(self::$view.'test/');
  }

  // @group view
  public function testCommandCreateViewFiles()
  {
    \Laravel\CLI\Command::run(
      array(
       'zircon::generate:view' 
       ,'test.index'
       ,'test.zircon.show'
       ,'test'
    ));

    $this->assertFileExists(self::$view . 'test/index.blade.php');
    $this->assertFileExists(self::$view . 'test/zircon/show.blade.php');
    $this->assertFileExists(self::$view . 'test.blade.php');
    
    File::delete(self::$view . 'test/index.blade.php');
    File::delete(self::$view . 'test/zircon/show.blade.php');
    File::delete(self::$view . 'test.blade.php');
    File::rmdir(self::$view.'test/');
  }

  // @group migrations
  public function testCreateMigrationFiles()
  {
    $this->generate->migration(array(
      'create_users'
    ));

    $file = File::latest(self::$migration);
    $this->assertFileExists((string)$file);
    File::delete((string)$file);
  }

  // @group migrations
  public function testCommandCreateMigrationFiles()
  {
    
    \Laravel\CLI\Command::run(
      array(
          'zircon::generate:migration' 
          ,'create_users'
      ));

    $file = File::latest(self::$migration);
    $this->assertFileExists((string)$file);
    File::delete((string)$file);
  }

  // @group migrations
  public function testCreateMigrationFilesWithColumns()
  {
    $this->generate->migration(array(
      'create_users_with_columns'
      ,'id:integer'
      ,'column1:string[250]'
      ,'column2:string:nullable'
      ,"produto_id:foreign:references['id']:on['produtos']:on_delete['restrict']"
    ));

    $file = File::latest(self::$migration);
    $this->assertFileExists((string)$file);
    
    $contents = (string)File::get($file);

    $this->assertContains("\$table->increments('id')->unsigned();", $contents);
    $this->assertContains("\$table->string('column1',250);", $contents);
    $this->assertContains("\$table->string('column2')->nullable();", $contents);
    $this->assertContains("\$table->foreign('produto_id')->references('id')->on('produtos')->on_delete('restrict')", $contents);
    $this->assertContains("\$table->timestamps();", $contents);
    $this->assertContains("Schema::drop('users_with_columns');", $contents);
    File::delete((string)$file);
  }

  // @group resource
  public function testCreateResourceFiles()
  {
    $this->generate->resource(array(
        'testgenerate'
       ,'with_tests'
    ));

    $this->assertFileExists(self::$controller . 'testgenerates.php');
    $this->assertFileExists(self::$model . 'testgenerate.php');

    $this->assertFileExists(self::$view . 'testgenerate/index.blade.php');
    $this->assertFileExists(self::$view . 'testgenerate/show.blade.php');
    $this->assertFileExists(self::$view . 'testgenerate/edit.blade.php');
    $this->assertFileExists(self::$view . 'testgenerate/new.blade.php');
    
    $this->assertFileExists(self::$test . 'controllers/testgenerates.test.php');
    $contents = (string)File::get(self::$test . 'controllers/testgenerates.test.php');

    $this->assertContains("\$response = Controller::call('Testgenerates@index');", $contents);
    $this->assertContains("\$this->assertEquals('200', \$response->foundation->getStatusCode());", $contents);
    $this->assertContains("\$this->assertRegExp('/.+/', (string)\$response, 'There should be some content in the index view.');", $contents);

    $this->assertNotContains("public function test_restful()", $contents);

    File::delete(self::$controller . 'testgenerates.php');
    File::delete(self::$model . 'testgenerate.php');
    File::rmdir(self::$view.'testgenerate/');
    File::rmdir(self::$test . 'controllers/');
  }

  public function tearDown()
  {
    ob_end_clean();

    //File::delete(Bundle::path('zircon') . 'controllers'.DS.'modulos'.DS.'testegenerate.php');
  }
}