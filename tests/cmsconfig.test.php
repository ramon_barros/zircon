<?php
/**
* Controllers Test
*
* @category Tests
*
*/

use \Core\Config as Zircon_Config;

class TestConfigCms extends AbstractDataAccessTest
{

  private $instance;

  public function assertPreConditions()
  {	
      $this->assertTrue(
              class_exists($class = '\Core\Config'),
              'Class not found: '.$class
      );
  }

  public function testInstantiationWithoutArgumentsShouldWork(){
  	$this->instance = new Zircon_Config();
    $this->assertInstanceOf('\Core\Config', $this->instance);
  }

  /**
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage Você deve informar uma chave para retornar a configuração.
     */
    public function testSetWithInvalidDataShouldThrownAnException()
    {
        $invalidSet = '';
        Zircon_Config::cms($invalidSet);
    }

  /**
  * @depends testInstantiationWithoutArgumentsShouldWork
  */
  public function testReturnFileConfig(){
    $this->assertEquals(Zircon_Config::cms("zircon::settings.title"), "CMS");
    $this->assertEquals(Zircon_Config::cms("application.key"), "");
  }

  /**
  * @depends testInstantiationWithoutArgumentsShouldWork
  */
  public function testReturnInvalidArgumentFileConfig(){
    $this->assertEquals(Zircon_Config::cms("zircon::settings.null"), "");
  }
}