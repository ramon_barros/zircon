<?php
/**
* Controllers Test
*
* @category Tests
*
*/

class TestCommentParser extends AbstractDataAccessTest
{

  public function assertPreConditions()
  {	
    $this->assertTrue(
            class_exists($class = 'CommentParser'),
            'Class not found: '.$class
    );
  }

  public function testReturnCommentClass(){
    //$className = 'Produtos';
    //$reflectionClass = new \ReflectionClass($className); 
    ////$param = CommentParser::parse($reflectionClass->getMethod('up')->getDocComment());    
    $comment = '/**
                 * Cria a tabela do modulo
                 * @relation relacao1
                 * @return boolean retorna se a tabela existe
                 */';
    $param = CommentParser::parse($comment);
    $comp = array(
         "relation" => array("relacao1")
        ,"return" => array(
           "type" => "boolean"
          ,"description" => "retorna se a tabela existe"
        )
    );
    $this->assertEquals($comp, $param);
  }
  public function testeMultipliParam(){
    $comment = '/**
                 * Cria a tabela do modulo
                 * @relation relacao1|relacao2
                 * @return boolean retorna se a tabela existe
                 */';
    $param = CommentParser::parse($comment);
    $comp = array(
       "relation" => array("relacao1","relacao2")
      ,"return" => array(
         "type" => "boolean"
        ,"description" => "retorna se a tabela existe"
      )
    );
    $this->assertEquals($comp, $param);
  }
}