<?php
/**
* Models Test
*
* @category Tests
*
*/

require_once Bundle::path('zircon') . '/tasks/generate.php';

class TestModels extends AbstractDataAccessTest
{
  public static $model;

  public function setup()
  {
    self::$model = Bundle::path('zircon') . 'models'.DS.'modulos'.DS;
  }

  public function assertPreConditions()
  {	
      $this->assertTrue(
              class_exists($class = 'Eloquent'),
              'Class not found: '.$class
      );
  }

  public function testModuloExtendsEloquent(){
    \Laravel\CLI\Command::run(
      array(
          'zircon::generate:model' 
          ,'TestExtendsEloquent'
          ,'up:public:static'
          ,'down:public:static'
          ,'permissions:public:static'
          ,'columns:public:static'
          ,'paginate:public:static'
          ,'rules:public:static'
          ,'search:public:static'
      ));
  
    $reflection = new ReflectionClass('TestExtendsEloquent');
    $this->assertEquals($reflection->getParentClass()->getName(), 'Core\Model\Eloquent');
    
    $file = File::latest(self::$model);
    $this->assertFileExists((string)$file);
    $contents = (string)File::get($file);
    $this->assertContains("class TestExtendsEloquent extends Eloquent", $contents);
    $this->assertContains("public static function up()", $contents);
    $this->assertContains("public static function down()", $contents);
    $this->assertContains("public static function permissions()", $contents);
    $this->assertContains("public static function columns()", $contents);
    $this->assertContains("public static function paginate()", $contents);
    $this->assertContains("public static function rules()", $contents);
    $this->assertContains("public static function search()", $contents);
    File::delete((string)$file);
  }

  public function testModuloExtendsEloquentWithoutMethods(){
    \Laravel\CLI\Command::run(
      array(
          'zircon::generate:model' 
          ,'TestExtendsEloquent'
      ));
  
    $reflection = new ReflectionClass('TestExtendsEloquent');
    $this->assertEquals($reflection->getParentClass()->getName(), 'Core\Model\Eloquent');
      
    $reflection = new ReflectionClass('Eloquent');
    foreach ($reflection->getInterfaceNames() as $InterfaceNames) {
        $interface = new ReflectionClass($InterfaceNames);
        foreach ($interface->getMethods() as $InterfaceMethod) {
            $interfaces[]=$InterfaceMethod->getName();
        }
    }

    $file = File::latest(self::$model);
    $this->assertFileExists((string)$file);
    $this->assertTrue(is_array($interfaces));
    
    File::delete((string)$file);
  }
}