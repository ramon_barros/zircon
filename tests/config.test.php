<?php
/**
* Config Test
*
* @category Tests
*
*/

class TestAdminConfig extends PHPUnit_Framework_TestCase
{
    public function testFileConfigExists(){
    	$config = Bundle::path('zircon').'config/settings.php';
    	$this->assertTrue(file_exists($config));
    }
}