<?php

/**
 * Load the Markdown library.
 */
if(!file_exists(Bundle::path('docs').'libraries'.DS.'markdown.php')){
    require_once Bundle::path('zircon').'libraries'.DS.'Core'.DS.'markdown.php';
}else{
	require_once Bundle::path('docs').'libraries'.DS.'markdown.php';
}

/**
 * Get the root path for the documentation Markdown.
 *
 * @return string
 */
function cms_doc_root()
{
	return Bundle::path('zircon').'docs'.DS.'documentation/';
}

/**
 * Get the parsed Markdown contents of a given page.
 *
 * @param  string  $page
 * @return string
 */
function cms_document($page)
{
	return Markdown(file_get_contents(cms_doc_root().$page.'.md'));
}

/**
 * Determine if a documentation page exists.
 *
 * @param  string  $page
 * @return bool
 */
function cms_document_exists($page)
{
	return file_exists(cms_doc_root().$page.'.md');
}

/**
 * Attach the sidebar to the documentation template.
 */
View::composer('zircon::docs.template', function($view)
{
	$view->with('sidebar', cms_document('contents'));
});

/**
 * Handle the documentation homepage.
 *
 * This page contains the "introduction" to Laravel.
 */
Route::get('(:bundle)/docs', function()
{
	return View::make('zircon::docs.page')->with('content', cms_document('home'));
});

/**
 * Handle documentation routes for sections and pages.
 *
 * @param  string  $section
 * @param  string  $page
 * @return mixed
 */
Route::get('(:bundle)/docs/(:any)/(:any?)', function($section, $page = null)
{
	$file = rtrim(implode('/', func_get_args()), '/');

	// If no page was specified, but a "home" page exists for the section,
	// we'll set the file to the home page so that the proper page is
	// displayed back out to the client for the requested doc page.
	if (is_null($page) and cms_document_exists($file.'/home'))
	{
		$file .= '/home';
	}

	if (cms_document_exists($file))
	{
		return View::make('zircon::docs.page')->with('content', cms_document($file));
	}
	else
	{
		return Response::error('404');
	}
});