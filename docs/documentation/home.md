# Documentação do CMS

- [Introdução](#introducao)
- [Informações sobre a licença](#cms-license)

<a name="introducao"></a>
## Introdução

Bem-vindo a documentação do CMS. Esta documentação é um guia para auxilia-lo na instalação e criação de um cms da forma mais simples e organizada. Recomendo a leitura da documentação em ordem, uma vez que nos permite estabelecer progressivamente conceitos que serão utilizados em documentos posteriores. 

<a name="cms-license"></a>
## Informações sobre a licença

Zircon é um software open-source licenciado sob a [MIT License](http://www.opensource.org/licenses/mit-license.php).