# Views

## Conteúdo

- [O básico](#the-basics)

<a name="the-basics"></a>
## O básico

As views do CMS são criadas na pasta **zircon/views/templates/** e no arquivo de configuração do cms é possível
configurar qual template gostaria de utilizar.

	/**
     * Pasta onde contem os temas
     */
    ,'view' => 'templates'

    /**
     * Tema selecionado
     */
    ,'template' => 'default'

    /**
     * layout utilizado
     */
    ,'layout' => 'layouts.admin'

Ao alterar a chave template para 'bootstrap' o cms carrega os templates da pasta **zircon/views/templates/bootstrap/**.
Quando for necessário passar alguma view diferente das padrões da classe Zircon_Core_Base_Controller você deve fazer desta forma dentro do controller do modulo:

	/**
	 * Modulo de exemplo
	 */
	class Zircon_Modulos_Contatos_Controller extends Zircon_Core_Base_Controller {

		public function get_index(){
			$this->admin['content'] = $this->template('zircon.list');
			return $this->admin;
		}
	}

A função $this->template(); esta esta dentro da classe Zircon_Core_Base_Controller e retorna a seguinte string:
	
	/**
	* Monta o caminho onde se encontra a view.
	* @param  string $view nome da view
	* @return string       retorna o caminho da view a ser passado para View::make
	*/
	public function template($view){
		return "zircon::{$this->view}.{$this->template}.{$view}";
	}

	/**
	 * Tema selecionado
	 */
	'template' => 'default',

	views
	|___templates
		 |___bootstrap
		 |___default
		 	 |___admin //Aqui você adiciona as pastas e as views para o cms
		 	 |	 |___users
		 	 |	 |  |___list.blade.php // Não alterar
		 	 |	 |
		 	 |	 |__list.blade.php
		 	 |
		 	 |___layouts // Não alterar
		 	 |___modulo // Não alterar
	

Você pode utilizar esta função para carregar a view no modulo:

	$this->template('zircon.list');
	// Retorna
	zircon::templates.default.zircon.list // templates/default/zircon/list.blade.php

	$this->template('zircon.users.list');
	// Retorna
	zircon::templates.default.zircon.users.list // templates/default/zircon/users/list.blade.php

