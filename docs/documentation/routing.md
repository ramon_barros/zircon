# Rotas

## Conteúdo

- [Básico](/zircon/docs/routing#the-basics)

<a name="the-basics"></a>
## Básico

As rotas para os modulos criados são identificadas automaticamente não sendo necessárias configurar no arquivo **routes.php**.

O CMS contém as seguintes rotas:

- Página de Login `zircon/auth` 
- Home `zircon/` ou `zircon/dashboard`
- Página de Usuários `zircon/users`
- Página de Modulos `zircon/modulos`
- Modulos instalados `zircon/modulos/+nome do modulo`