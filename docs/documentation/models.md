
# Models

## Contents

- [Models](#models)
- [Estrutura](#structure)
- [Propriedades](#var)
- [Métodos Obrigatórios](#method-obligatory)
- [key()](#key)
- [up()](#up)
- [down()](#down)
- [permissions()](#permissions)
- [columns()](#columns)
- [paginate()](#paginate)
- [rules()](#rules)
- [search()](#search)
- [Métodos Opcionais](#method-optional)

<a name="models"></a>
## Models

Com os models do cms você pode criar as tabelas do bando de dados, manipular as informações do bando de dados e outros serviços e retornar para o controller que encarregará de passar as informações para as views.

<a name="structure"></a>
## Estrutura
	
Ao criar o seu model você deve extender para a classe Eloquent do Laravel para poder utilizar as funções de consulta ao bando de dados, entre outros recursos.

	class Nome_do_Model extends Eloquent {

	}

Existem alguns métodos da Classe Eloquent que você não deve criar com o mesmo nome, 
assim você poderá utilizar todos os recursos do Laravel, segue alguns deles:

- fill
- accessible
- create
- update
- find
- push
- save
- delete
- timestamp
- query
- sync
- table
- first
- get

Para mais informações [veja](http://laravel.com/api/namespace-Laravel.Database.Eloquent.html)


<a name="var"></a>
## Propriedades

O model deve conter as seguintes propriedades estáticas e publicas:
	
	/**
     * Indica se o model tem as colunas de atualização (updated_at) e criação (created_at).
     *
     * @var bool
     */
    public static $timestamps = true;

	/**
     * Primary key
     * @var string
     */
    public static $key = 'id';
    
    /**
     * Tabela 
     * @var string
     */
    public static $table = 'tabela_do_model';

    /**
     * Mostra o modulo no menu principal
     * @var boolean
     */
    public static $menu = true;

    /**
     * Editor utilizado
     * @var string
     */
	public static $editor = 'wysiwyg';

    /**
     * Nome do modulo
     * @var string
     */
    public static $modulo = 'nome_do_model';

    /**
     * Mostra o campo de busca
     * @var boolean
     */
	public static $search = true;

	/**
	 * Informa se após um envio de formulário (cadastro ou edição) será retornado para a
	 * listagem dos registros ou continua na mesma página
	 * @var boolean
	 */
	public static $referrer = false;

	/**
     * Subtitulo e submenus
     * Não é obrigatório criar esta propriedade, se for informado o subtitulo irá modificar
     * no menu principal, caso não for informado o cms retorna o nome do model
     * @var array
     */
    public static $admin = array(
                            'subtitle' =>'Contatos',
                            'submenu' => array(
                                 'zircon/modulos/contatos/categorias'    => 'Categorias' 
                                ,'zircon/modulos/contatos/subcategorias' => 'Subcategorias'
                            )
                        );

<a name="method-obligatory"></a>
## Métodos obrigatórios

Existe alguns métodos que dever ser criados:

    public static function key(){
    	...
    }

    public static function up(){
       	...
    }

    public static function down(){
       	...
    }

    public static function permissions(){
    	...
    }

    public static function columns()
    {
      	...
    }

    public static function paginate()
    {
    	...
    }

    public static function rules()
    {
        ...
    }

    public static function search(){
       	...             
    }

<a name="key"></a>
## Método Key()

Retorna o valor armazenado na propriedade public static $key

	/**
     * Retorna primary key
     * @return integer 
     */
    public static function key(){
        return static::$key;
    }

<a name="up"></a>
## Método up()

Este método é chamado ao clicar no botão de instalar no menu **Modulos**. Serve para criar as tabelas e colunas no bando de dados.
Não é necessário adicionar as funções para criação das tabelas, podendo o método ficar vazio.

	 /**
     * Criar a tabela do modulo
     * @return boolean retorna se a tabela existe
     */
    public static function up(){
        Schema::table(static::$table,function($table){
            $table->create();
            $table->increments(static::key())->unsigned();
            $table->string('nome');
        });
        return Schema::exists(static::$table);
    }

> **Obs:** O método Schema::exists() foi adicionado neste laravel para facilitar na hora de verificar se a tabela existe ou não. É de responsabilidade do desenvolvedor seguir de forma correta a criação das relações (foreign keys) entre as tabelas.

<a name="down"></a>
## Método down()

Este método serve para apagar a tabela no banco de dados quando clicado no botão desinstalar no menu **Modulos**.

	/**
     * Deleta a tabela do modulo
     * @return boolean retorna se a tabela existe
     */
    public static function down(){
        Schema::table(static::$table, function($table) {
            $table->drop();
        });
        return Schema::exists(static::$table);
    }

<a name="permissions"></a>
## Método permissions()

As permissões do modulo são definidas neste método necessárias para liberação dos botões de cadastro , edição e exclusão dos registros.
O valor 1 indica que a permissão esta liberada e 0 desativada.	

	/**
     * Permissões do modulo
     * @return array retorna as permissões do modulo
     */
    public static function permissions(){
        $permissions = array(
             static::$modulo."_edit"   => array(1,'Editar') 
            ,static::$modulo."_add"    => array(1,'Adicionar')
            ,static::$modulo."_delete" => array(1,'Deletar')
        );
        
        return  $permissions;
    }

Você também pode definir as permissões para cada grupo de usuários.

    /**
     * Permissões do modulo
     * @return array retorna as permissões do modulo
     */
    public static function permissions(){
        $permissions = array(
            'superuser' => array(
                 static::$modulo."_edit"   => array(1,'Editar') 
                ,static::$modulo."_add"    => array(1,'Adicionar')
                ,static::$modulo."_delete" => array(1,'Deletar')
            ),
            'is_admin' => array(
                 static::$modulo."_edit"   => array(1,'Editar') 
                ,static::$modulo."_add"    => array(1,'Adicionar')
                ,static::$modulo."_delete" => array(1,'Deletar')
            ),
            'user' => array(
                 static::$modulo."_edit"   => array(0,'Editar') 
                ,static::$modulo."_add"    => array(0,'Adicionar')
                ,static::$modulo."_delete" => array(0,'Deletar')
            ),
        );
        
        return  $permissions;
    }

> **Obs:** Ao clicar no botão instalar no menu **Modulos** automaticamente é criada as permissões nas tabelas necessárias e adicionada em todos os grupos de usuários, assim você não perde tempo e deixa o trabalho pesado para o CMS.

Em versões futuras poderemos adicionar permissões personalizadas, assim bloqueando métodos e outros recursos.

<a name="columns"></a>
## Método columns()

Aqui você cria os campos que serão mostrados no formulário de cadastro e edição. Este array é passado para a função Dash\Form::build($columns , $data) junto com os dados do registro. Esta função encarrega-se de criar os campos do formulário.

	/**
     * Colunas para construção do formulário
     *
     * @return array
     */
    public static function columns()
    {
        return array(
            'nome_da_coluna' => array(
                'input' => function($val = '') {
                	/**
                	 * Retorna um input hidden com o name="nome_da_coluna" , se estiver em edição adiciona o valor correspondente.
                	 * <code>
                	 * 		<input type="hidden" name="nome_da_coluna" value="1">
                	 * </code>
                	 * @type {[type]}
                	 */
                    return Form::hidden('nome_da_coluna' , $val);
                },

            ),
            // Nome da coluna
            'produtos.slug' => array(
            	// Nome do campo
                'label' =>  function() {
                	// Retorna a tag <label> com os atributos passados
                	// <label for="nome" class="large-label">Nome</label>
                    return Form::label('slug', 'URL', array( 'class' => 'large-label'));
                },
                // Esta chave sempre será input
                'input' => function($val = '') {
                	// Retorna a tag <input> com o value e os atributos passados
                	// <input class="x-large" type="text" name="slug" value="valor" id="slug">
                    return Form::text('slug', $val, array('class'=>'x-large'));
                },
            )
        );
    }

<a name="paginate"></a>
## Método paginate()

Este método retorna um objeto contendo os registros do modulo para uma listagem com paginação.

    /**
     * Paginação
     * @return object
     */
    public static function paginate()
    {
        $items = array(
            'id',
            'nome'
        );
        return DB::table(static::$table)->paginate(Config::get('zircon::settings.per_page'), $items);
    }

<a name="rules"></a>
## Validação

Os campos e validações que serão passadas para o post_save() após o envio dos dados.

    /**
     * Validação dos campos do formulário
     *
     * @return array
     */
    public static function rules()
    {
        return array(
            'nome' => 'required'
        );
    } 

Mais informações [veja](/docs/validation)

<a name="search"></a>
## Busca

Ao habilitar a opção `$search = true;` e digitar um termo para busca o mesmo é passado para este método.

     public static function search(){
        return DB::table(static::$table)
                    ->where(function($query)
                    {
                        $input = Input::all();
                        $terms = explode(' ', $input['search-query']);
                        foreach (static::columns() as $key => $value) {
                            foreach ($terms as $value) {
                                if($key=='id') continue;
                               $query->or_where($key, 'LIKE', '%'.$value.'%');
                            }
                        }                       
                    })->get();
                    
    }

<a name="method-optional"></a>
## Métodos Opcionais

Você pode criar os métodos necessários para sua aplicação. Supondo que sua aplicação necessite de mais um botão alem dos padrões de editar, adicionar e deletar. Por exemplo um botão de enviar.

Adicione as permissões do botão no método `permissions()`

    /**
     * Permissões do modulo
     * @return array retorna as permissões do modulo
     */
    public static function permissions(){
        $permissions = array(
            'superuser' => array(
                 static::$modulo."_edit"   => array(1,'Editar') 
                ,static::$modulo."_add"    => array(1,'Adicionar')
                ,static::$modulo."_delete" => array(1,'Deletar')
                ,static::$modulo."_send" => array(1,'Enviar')
            ),
            'is_admin' => array(
                 static::$modulo."_edit"   => array(0,'Editar') 
                ,static::$modulo."_add"    => array(0,'Adicionar')
                ,static::$modulo."_delete" => array(0,'Deletar')
                ,static::$modulo."_send" => array(1,'Enviar')
            ),
            'user' => array(
                 static::$modulo."_edit"   => array(0,'Editar') 
                ,static::$modulo."_add"    => array(0,'Adicionar')
                ,static::$modulo."_delete" => array(0,'Deletar')
                ,static::$modulo."_send" => array(0,'Enviar')
            )
        );
        
        return  $permissions;
    }

Ao instalar o modulo as permissões serão criadas. Você pode criar dentro do model
o método `buttons()` retornar os botões `Modulo::modulo_buttons('nome_do_modulo');`.
    
    /**
     * Retorna os botões habilitados para o usuário
     * @return {[type]} [description]
     */
    public static function buttons(){
        /**
         * Retorna os botões padrões conforme permissão do usuário
         * @type object
         */
        $buttons = Modulo::modulo_buttons(static::$modulo);
        
        /**
         * Verifica permissão do novo botão a ser criado
         * @type object
         */
        if(Modulo::modulo_user_permissions(static::$modulo,'send')){   
            $buttons->mail = '<a class="btn send"  href="#modalSend" role="button" data-key=":keyName" data-value=":keyValue" data-toggle="modal"><i class="icon-envelope"></i></a>';
        }
        return $buttons;
    }

No controller do modulo você chama o método buttons.

    public function get_index(){
        /**
         * Retorna o nome do modulo
         * @type string
         */
        $model = $this->model;

        /**
         * Passa para a chave buttons os botões habilitados para o usuário
         * @type array
         */
        $this->admin['buttons'] = $model::buttons();
        
        /**
         * Obrigatório informar a view que irá mostrar a listagem dos registros
         * quando um método padrão é chamado
         * @type array
         */
        $this->admin['content'] = $this->template('zircon.ordens.list');
        
        /**
         * Obrigatório retornar o array $this->admin contendo as informações do modulo
         * @type array
         */
        return $this->admin;
    }

> **Obs:** O método get_index() é chamado pela Classe Zircon_Core_Modulos_Controller dentro do método `__call`, quando você acessa a url do modulo por exemplo http://localhost/cms/public/zircon/modulos/contatos
