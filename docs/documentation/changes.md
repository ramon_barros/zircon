# CMS Atualizações

## Conteúdo

- [CMS 2.2](#2.2)
- [Atualização para 2.2](#upgrade-2.2)
- [CMS 2.1.1](#2.1)
- [Atualização para 2.1.1](#upgrade-2.1.1)
- [CMS 2.1](#2.1)
- [Atualização para 2.1](#upgrade-2.1)
- [CMS 2.0.5](#2.0.5)
- [Atualização para 2.0.5](#upgrade-2.0.5)
- [CMS 2.0.4](#2.0.4)
- [Atualização para 2.0.4](#upgrade-2.0.4)
- [CMS 2.0.3](#2.0.3)
- [Atualização para 2.0.3](#upgrade-2.0.3)
- [CMS 2.0.2](#2.0.2)
- [Atualização para 2.0.2](#upgrade-2.0.2)
- [CMS 2.0.1](#2.0.1)
- [Atualização para 2.0.1](#upgrade-2.0.1)
- [CMS 2.0](#2.0)
- [Atualização para 2.0](#upgrade-2.0)
- [CMS 1.9](#1.9)
- [Atualização para 1.9](#upgrade-1.9)
- [CMS 1.8](#1.8)
- [Atualização para 1.8](#upgrade-1.8)
- [CMS 1.7](#1.7)
- [Atualização para 1.7](#upgrade-1.7)
- [CMS 1.6](#1.6)
- [Atualização para 1.6](#upgrade-1.6)
- [CMS 1.5](#1.5)
- [Atualização para 1.5](#upgrade-1.5)
- [CMS 1.4](#1.4)
- [Atualização para 1.4](#upgrade-1.4)
- [CMS 1.3](#1.3)
- [Atualização para 1.3](#upgrade-1.3)
- [CMS 1.2](#1.2)
- [Atualização para 1.2](#upgrade-1.2)
- [CMS 1.1](#1.1)
- [Atualização para 1.1](#upgrade-1.1)
- [CMS 1.0](#1.0)
- [Atualização para 1.0](#upgrade-1.0)

<a name="upgrade-2.2"></a>
### Atualização para 2.2

- Atualizada para a pasta **cms**.

<a name="2.2"></a>
## CMS 2.2

- Mostra mensagem para o desenvolvedor extender o modulo para a classe Eloquent.
- Adicionado opção de especificar o tipo de função ao criar os models pelo.
- Alterado Core\Config para Zircon_Config.
- Alterado modo como é criadas as tabelas do modulos.
- Adicionado implements na classe Eloquent para métodos obrigatórios.
- Adicionado teste com zircon::generate:resource with_tests.
- Adicionado opção de criar os controllers, models e views.
- Adicionado teste para Zircon_Generate_Task.
- Adicionado Zircon_Generate_Task no CMS.

<a name="upgrade-2.1.1"></a>
### Atualização para 2.1.1

- Atualizada para a pasta **cms**.

<a name="2.1.1"></a>
## CMS 2.1.1

- Movido classes para gerar os arquivos de migrations para pasta Generator.
- Removido comentarios desnecessários dos testes de Assets.
- Adicionado view para o template bootstrap.
- Fix bug dashboard Controller Admin.

<a name="upgrade-2.1"></a>
### Atualização para 2.1

- Atualizada para a pasta **cms**.

<a name="2.1"></a>
## CMS 2.1

- Adicionado galeria de fotos no modulo de contatos.
- Adicionado modulo de Fotos, necessário efetuar testes.

<a name="upgrade-2.0.5"></a>
### Atualização para 2.0.5

- Atualizada para a pasta **cms**.

<a name="2.0.5"></a>
## CMS 2.0.5

- Fix #14> Alterado modo de criação das tabelas dos modulos.

<a name="upgrade-2.0.4"></a>
### Atualização para 2.0.4

- Atualizada para a pasta **cms**.

<a name="2.0.4"></a>
## CMS 2.0.4

- Correções nos Controllers Admin e Modulos

<a name="upgrade-2.0.3"></a>
### Atualização para 2.0.3

- Atualizada para a pasta **cms**.

<a name="2.0.3"></a>
## CMS 2.0.3

- Adicionado versão/autor nos arquivos principais do Bundle Admin.
- Fix #16> Removido "Editar senha" cadastro novos usuários.

<a name="upgrade-2.0.2"></a>
### Atualização para 2.0.2

- Atualizada para a pasta **cms**.

<a name="2.0.2"></a>
## CMS 2.0.2

- Fix #18> Corrigido bug ao cadastrar usuários.

<a name="upgrade-2.0.1"></a>
### Atualização para 2.0.1

- Atualizada para a pasta **cms**.

<a name="2.0.1"></a>
## CMS 2.0.1

- Fix>#17 Corrigido problema no cadastro de novos usuários.

<a name="upgrade-2.0"></a>
### Atualização para 2.0

- Atualizada para a pasta **cms**.

<a name="2.0"></a>
## CMS 2.0

- Adicionado alias para a Classe Core\Config.
- Correção nos teste Base Controller e CmsConfig.
- Fix bug na Classe Core\Config (Erro ao passar id inválido).
- Movido a o arquivo de rotas customizadas para pasta libraries.
- Alterado local das classes principais do CMS Zircon_Base_Controller, Model Modulos e Model Users.
- Retirado informações do README já estão na documentação.
- Adicionado opção de colocar informações no dashboard do CMS.
- Adicionado tests para CmsConfig , Controllers e Core\Config.
- Opção de especificar mais de uma url (array) para o CMS.
- Adicionado opção de customizar a url do CMS.

<a name="upgrade-1.9"></a>
### Atualização para 1.9

- Atualizada para a pasta **cms**.

<a name="1.9"></a>
## CMS 1.9

- Adicionado opção para alterar o placeholder do campo busca.
- Corrigido bug variavel referrer, caso não exista no model.
- Suporte a mysql e postgre no método padrão post_save.
- Correção do carregamento do jquery-ui.js
- Correção no placeholder na página de login.
- Correção nas permissões por grupo.
- Usuário Ezoom não é mais listado.
- Correção na instalação do sentry.

<a name="upgrade-1.8"></a>
### Atualização para 1.8

- Atualizada para a pasta **cms**.

<a name="1.8"></a>
## CMS 1.8

- Correção na criação das tabelas mysql e pgsql.
- Correção na função para salvar os registros.
- Alteração nos arquivos migrations para funcionamento no pgsql.
- Alteração na documentação das permissões.

<a name="upgrade-1.7"></a>
### Atualização para 1.7

- Atualizada para a pasta **cms**.

<a name="1.7"></a>
## CMS 1.7

- Possivel definir no modulo permissões para determinado grupo.

<a name="upgrade-1.6"></a>
### Atualização para 1.6

- Atualizada para a pasta **cms**.

<a name="1.6"></a>
## CMS 1.6

- Exemplos melhorados
- Exemplos de css e js

<a name="upgrade-1.5"></a>
### Atualização para 1.5

- Atualizada para a pasta **cms**.

<a name="1.5"></a>
## CMS 1.5

- Autoload de js
- Autoload de css(nested controllers) e template padrao

<a name="upgrade-1.4"></a>
### Atualização para 1.4

- Atualizada para a pasta **docs**.

<a name="1.4"></a>
## CMS 1.4

- Atualização da documentação models, views e controllers.

<a name="upgrade-1.3"></a>
### Atualização para 1.3

- Atualizada para a pasta **cms**.

<a name="1.3"></a>
## CMS 1.3

- Documentação do Model.
- Removido debug na função search.
- Movidas as views para suas pastas corretas.
- Modificação das variáveis retornadas para as views.
- Correção no carregamento da documentação no arquivo de rotas.
- Merge branch 'docs' of bitbucket.org:ezoom/cms into docs
- Documentação da instalação, home, rotas, models e controllers.
- Atualizações nas funções dos controllers admin, base e modulos.

<a name="upgrade-1.2"></a>
### Atualização para 1.2

- Atualizada para a pasta **cms**.

<a name="1.2"></a>
## CMS 1.2

- Inicio da documentação do CMS.

<a name="upgrade-1.1"></a>
### Atualização para 1.1

- Atualizada para a pasta **cms**.

<a name="1.1"></a>
## CMS 1.1

- Atualizações nas funções dos controllers admin, base e modulos.

<a name="upgrade-1.0"></a>
### Atualização para 1.0

- Atualizada para a pasta **cms**.

<a name="1.0"></a>
## CMS 1.0

- Correções dos controllers do cms.
- Correção no sistema de upload de arquivos.
- Adicionado botão voltar nos forms.
- Modulo básico com metodos da classe base.
- Tratamento de errors nos dados dos modulos.
- Adicionado tratamento de errors nos modulos.
- Atualização da classe base para automatizar criação dos modulos.
- Correção no template e zircon.js
- Atualização do controller modulos e correções.
- Alteração nas funções do arquivo base.
- Adicionado zircon.js e zircon.css nos templates.
- Fix #2> Alterado modo de carregamento do editor.
- Atualizado README.
- Adicionado tests para o bundle zircon.
- Fix #6> Corrigido nome do usuário no menu modulos.
- Fix #11> Corrigido editor de permissões dos modulos
- Correção na verificação das permissões dos usuários.
- Movido função modulos_permissions() do model user para modulo.
- Adicionado descrição nas permissões do modulo de contatos.
- Fix #10> Adicionado descrição nas permissões dos modulos
- Fix #7> Criado editor de permissões.
- Fix #8> Corrigido editar senha do usuário
- Removido assets do controller users e movido para base.
- Adicionado assets do cms para carregar em todos os controllers.
- Adicionado assets para o cms.
- Fix #1> Criado arquivos para configuração local
- Modificado gitignore para enviar pasta local ao repositório.
- Gerada nova chave criptografada para a aplicação.
- Adicionado bootstrap-fileupload para efeito no input file.
- Inserido chosen para efeito na tag select.
- Inserido assets style e scripts nos layouts.
- Removido editor da variavel type, mudar futuramente carregamento do editor
- Removido temporariamente o editor e adicionado nome do usuario logado
- Correção nas permissões dos usuários.
- Correção no array passado para View::make.
- Modificações nos controllers admin e base.
- Adicionado Schema::exists para verificar se a tabela existe.
- Alteração nas configurações do Sentry, para buscar as permissões no database.
- Correção nas rotas para o painel de usuários.
- Adicionado pasta com templates diferentes, opção de configuração no arquivo settings para numero de resultados, - tema utilizado, titulo, etc.
- Alteração na configuração do banco de dados.
- Correção no arquivo README e adicionado migrations para criação das tabelas.
- Adicionado templates na pasta views.
- Adicionado bundle sentry para autenticação.
- Correção nas rotas para o CMS.
- Alterado controller base e zircon.
- Removido layout para a pasta templates/default.
- Alteração nos controllers admin e base.
- Adicionado configuração do bundle Admin.
- Configuração do bundle bootstrapper.
- Adicionado bundle bootstrapper.
- Pasta das views do CMS.
- Adicionado rotas para os controllers.
- Adicionado pasta para os controllers dos modulos.
- Adicionado pasta dos controllers do cms.
- Carregamento dos controllers e models do bundle zircon.
- Alterado para carregar o bundle zircon.
- Criado estrutura do CMS.
- Adicionado Laravel.
- Adicionado arquivos de configuração.
- Adicionado informações de instalação do cms.
- Informações do CMS.
- Adicionado README.md