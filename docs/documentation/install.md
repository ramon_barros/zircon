# Instalação e Configuração

## Conteúdo

- [Requisitos Laravel](#requirements-laravel)
- [Requisitos Cms](#requirements-cms)
- [Instalação](#installation)
- [Configuração do Servidor](#server-configuration)
- [Configuração Basica](#basic-configuration)
- [Ambientes](#environments)
- [URLs Limpas](#cleaner-urls)

<a name="requirements-laravel"></a>
## Requisitos Laravel

- Apache, nginx, ou outro servidor web compatível.
- Laravel tira proveito dos recursos poderosos que se tornaram disponíveis no PHP 5.3. Consequentemente, PHP 5.3 é uma exigência.
- Laravel usa o [biblioteca FileInfo](http://php.net/manual/en/book.fileinfo.php) para detectar arquivos "mime-types. 
  Isso está incluído por padrão com PHP 5.3. No entanto, os usuários do Windows podem precisar adicionar uma linha ao seu arquivo php.ini antes do módulo Fileinfo está ativado. 
  Para mais informações confira [configuração detalhes sobre PHP.net](http://php.net/manual/en/fileinfo.installation.php).
- Laravel usa o [biblioteca Mcrypt](http://php.net/manual/en/book.mcrypt.php) para criptografia e geração de hash. Mcrypt normalmente vem pré-instalado. 
  Se você não consegue encontrar Mcrypt na saída do phpinfo () verifique o site do fornecedor de sua instalação LAMP ou confira [configuração detalhes sobre PHP.net](http://php.net/manual/en/book.mcrypt.php).

<a name="requirements-cms"></a>
## Requisitos CMS

- Bundle Sentry para autenticação
- Bundle bootstrapper para os templates

<a name="installation"></a>
## Instalação

1. [Download](http://bitbucket.org/ezoom/cms)
2. Extraia os arquivos no seu servidor local.
3. Defina o valor da **key** no arquivo **config/application.php**.
4. Verifique se o diretório `storage/views` tem permissão de escrita.
5. Crie o bando de dados que servirá para a aplicação.
6. Altere as informações do banco de dados no arquivo **application/config/database.php** ou **application/config/local/database.php**.
7. Digite os comandos abaixo para criar automaticamente as tabelas necessárias.
8. Navegue até a sua aplicação em um navegador web... ...`eu disse navegador`.

Criação das tabelas: 

	php artisan migrate:install --env=local
	php artisan migrate sentry --env=local
	php artisan migrate admin --env=local

> **Obs:** Os comandos do artisan buscam as configurações no arquivo **application/config/local/database.php** por causa do parâmetro --env=local

Se tudo estiver correto, ao acessar o endereço [http://localhost/cms/public/admin](http://localhost/cms/public/admin) você verá uma tela de login. O usuário padrão é ezoom@ezoom.com.br e a senha padrão.

### Problemas ?

Se você esta tendo problemas verifique os passos anteriores novamente e tente o seguinte:

- Verifique se as tabelas necessárias foram realmente criadas no banco de dados:
> - groups
> - laravel_migrations
> -	modulos
> -	rules
> -	users
> -	users_groups
> -	users_metadata
> -	users_suspended

- Se você estiver usando mod_rewrite, definir o valor da opção index no arquivo **application/config/application.php** ou **application/config/local/application.php** para vazio.
- Verifique se a pasta `storage` e as pastas dentro são graváveis ​​pelo seu servidor web.

<a name="server-configuration"></a>
## Configurações do Servidor

Para maiores informações [veja](/docs/install#server-configuration)

<a name="basic-configuration"></a>
## Configurações Basicas

Para maiores informações [veja](/docs/install#basic-configuration)

Já existe uma pasta **application/config/local/** onde contem as configurações para um servidor local em seu computador.

<a name="environments"></a>
## Ambientes

Para maiores informações [veja](/docs/install#environments)

<a name="cleaner-urls"></a>
## URLs Limpas

Para maiores informações [veja](/docs/install#cleaner-urls)