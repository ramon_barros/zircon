# Controllers

## Conteúdo

- [O básico](#the-basics)
- [Array Admin](#array-admin)
- [Listagem](#list)

<a name="the-basics"></a>
## O básico

Os controllers do CMS são criados na pasta **zircon/controllers/modulos/** e dentro da pasta **zircon/controllers/cms** estão os controller que não podem ser alterados.
Como as funções CRUD básicas já estão criadas na classe Zircon_Core_Base_Controller ao extender o controller a esta classe o mesmo ficará vazio.
	
	/**
	 * Modulo de exemplo
	 */
	class Zircon_Modulos_Contatos_Controller extends Zircon_Core_Base_Controller {

	}

<a name="array-admin"></a>
## Variáveis globais

No controller do modulo não é necessário retornar View::make ou $this->layout pois isto é feito automaticamente desta forma facilita a manipulação das views e com isso o cms cria um array $this->admin com as seguintes chaves:
	
	/**
	 * Recupera o model do controller acessado
	 * @type {[type]}
	 */
	$model = $this->model;

	/**
	 * Titulo da aplicação
	 * Pode ser alterado no arquivo zircon/config/settigns
	 */
	$this->admin['title']

	/**
	 * Subtitulo do modulo
	 */
	$this->admin['subtitle']

	/** 
	 * Se dentro do model do modulo contiver a variável $admin com a chave subtitle
	 * então atribui se não pega o nome do modulo
	 */
	// Exemplo model Contatos
	public static $admin = array(
			'subtitle' => 'Os Contatos'
	);

	/**
	* Colunas para construção do formulário
	*/
	$this->admin['columns']

	/**
	* Registros cadastrados no modulo
	*/
	$this->admin['data']

	/**
	* Chave primária do modulo
	*/
	$this->admin['modelKey']

	/**
	 * Submenu do modulo definido no model do modulo.
	 */
	$this->admin['submenu']
	// Exemplo model Contatos
	// Exemplo model Contatos
	public static $admin = array(
			'subtitle' => 'Os Contatos'
		   ,'zircon/contatos/categorias' =>'Contatos - Categorias'
	);

	/**
	 * Retorna a variavel $search do model (true exibe campo de busca)
	 */
	$this->admin['search']

	/**
	 * Placeholder para o campo de busca
	 */
	$this->admin['placeholder'] = 'Digite o termo da busca';

	/**
	 * Menu para adicionar registros retornado conforme as permissões do usuário
	 */
	$this->admin['addMenu']

	/**
	 * Botões de editar e exluir são retornados conforme as permissões do usuário
	 */
	$this->admin['buttons']

<a name="list"></a>
## Método index

O método index por padrão lista os registros cadastrados no modulo. Caso necessite alterar alguma variável informada acima você deve criar a função e sempre retornar $this->admin.
	
	public function get_index(){
		return $this->admin;
	}
