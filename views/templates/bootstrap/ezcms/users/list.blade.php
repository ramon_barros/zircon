{{ Dash\Messages::get_html() }}

<?php
   
   $uploader = function (){ 
        return unserialize(strtolower(serialize(\Config::get('zircon::settings.uploader')))); 
    };
 ?>

<table class="table table-striped table-hover" id="images">
    @foreach ($data->results as $column => $result)
        @if (!$column)
            <thead>  <tr>
                <?php $first = true; ?>
                @foreach ($result as $key => $value)
                    <?php if ($first) {$first = false; continue;}  ?>
                    <th> {{ $key }} </th>
                @endforeach
                <th class="align-right"> Opções </th>
            </tr> </thead>
        @endif
        <tr>
            <?php $keyValue = false; $i = 0; $len = count($result); ?>
            @foreach ($result as $key => $value)
                <?php
                    if (!$keyValue and $key == $modelKey) {
                        $keyValue = $value; continue;
                    }
                    $i++;
                ?>
                <td>
                    @if(in_array(strtolower($key) , $uploader() ))
                    <a href="{{ URL::base() . '/thumburl/thumbnail/800/inbound/'. \Config::get('zircon::settings.upload_path') . $value }}" toptions="shaded = 1, overlayClose = 1" >
                       <img src="{{ URL::base() . '/thumburl/thumbnail/100X70/outbound/'. \Config::get('zircon::settings.upload_path') . $value }} " />
                    </a>
                    @else
                        {{ Str::words(strip_tags($value), 10) }} 
                    @endif
                </td>

            @endforeach
            <td class="align-right">
                @if(isset($buttons))
                    @foreach($buttons as $button)                       
                        <?php $pattern = array('/(:controller)/','/(:keyName)/','/(:keyValue)/'); ?>
                        <?php $replacement = array($controller,$modelKey,$keyValue); ?>
                        {{ preg_replace($pattern, $replacement, $button); }}
                    @endforeach
                @else
                    <a class="btn" href="{{ URL::home() . "zircon/{$controller}/edit/{$keyValue}" }}"><i class="icon-edit"></i></a>
                    <a href="#modal" role="button" class="btn delete" data-key="{{$modelKey}}" data-value="{{$keyValue}}" data-toggle="modal"><i class="icon-remove"></i></a>
                @endif
            </td>
        </tr>
    @endforeach
</table>

{{-- $data->links() --}}

{{ $data->appends(array('sort' => 'id'))->links() }}

<div class="modal hide fade static" id="modal"  role="dialog" aria-labelledby="modal" aria-hidden="true" >
  <div class="modal-header ">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Realmente deseja deletar o registro?</h3>
  </div>
  <div class="modal-body">
    <p>Os dados deletados não poderão ser recuperados.</p>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
    @if(isset($buttons))
        {{ Form::open(URL::home() . "zircon/modulos/{$controller}/delete", 'POST', array('style'=>'display:inline' , 'id'=>'formDelete')) }}
    @else
        {{ Form::open(URL::home() . "zircon/{$controller}/delete", 'POST', array('style'=>'display:inline' , 'id'=>'formDelete')) }}
    @endif

        {{ Form::hidden('' , '') }}
        <button class="btn btn-danger">Deletar</button>
    {{ Form::close() }}
  </div>
</div>