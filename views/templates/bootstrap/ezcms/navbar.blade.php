<div class="navbar">
    <div class="navbar-inner">
        <div class="container">
            {{ HTML::link('zircon/dashboard', $title, array('class' => 'brand')) }}
            <a class="btn btn-navbar collapsed" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
            </a>
            <div class="nav-collapse navbar-responsive-collapse collapse">
                <ul class="nav">
                    @if(is_array($navBar))
                        @foreach ($navBar as $name=>$link)
                                <li class="{{ URI::is($link)?'active':'' }}">{{ HTML::link($link, $name) }}</li>
                        @endforeach
                    @endif
                </ul>
                <ul class="nav pull-right">
                    @if(isset($user))
                        <li ><a class="brand" href="#">{{ $user->first_name }}</a></li>
                    @endif
                    <li class="divider-vertical"></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="http://avatars.io/auto/admin@admin.com" style="height:34px; width:34px;  border:solid 1px #EB5F3A; margin-right:3px"  />
                        </a>
                        <ul class="dropdown-menu">
                            <li>{{ HTML::link('zircon/logout', 'Sair') }}</li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- /.nav-collapse -->
        </div>
    </div>
    <!-- /navbar-inner -->
</div>
