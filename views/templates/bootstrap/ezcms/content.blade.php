<div style="padding:10px;background: rgba(255, 255, 255, 0.9);margin-top:-18px" class="hero-unit clearfix">
    <h3 style="padding-left:10px; float:left; line-height:38px">{{$subtitle}}</h3>
    @if( isset($addMenu) and count($addMenu)>0)
        <div style="float:right; margin:8px 22px 0px 50px;" class="btn-group">
            <button class="btn btn-large dropdown-toggle" data-toggle="dropdown"> <i class=" icon-plus"></i>&nbsp;&nbsp;<span class="caret"></span></button>
            <ul class="dropdown-menu pull-right">
                 @foreach ($addMenu as $key => $row)
                    <li class="{{ URI::is("{$key}")?'active':'' }}" >
                        {{ HTML::link($key, $row) }}
                    </li>
                @endforeach

            </ul>
        </div>
    @endif
    @if( isset($search) and $search==true)
        {{ Form::open($url_search, 'POST', array('class'=>'navbar-form pull-right')) }}
            <input type="text" name="search-query" class="search-query" placeholder="{{$placeholder}}">
        {{ Form::close() }}
    @endif
</div>
<div class="tabbable tabs-left" style="padding: 10px;">
    @if( isset($submenu) and count($submenu))
        <ul class="nav nav-tabs">
            @foreach ($submenu as $key => $row)
                <li class="{{ URI::is("*{$key}*")?'active':'' }}" >
                    {{ HTML::link($key, $row) }}
                </li>
            @endforeach
        </ul>
    @endif

    <div class="tab-content">
        <div class="tab-pane active" >
            {{$content}}
        </div>

    </div>
</div>
