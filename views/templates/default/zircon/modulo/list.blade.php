{{ Dash\Messages::get_html() }}

<?php

   $uploader = function (){
        return unserialize(strtolower(serialize(\Config::get('zircon::settings.uploader'))));
    };
 ?>

<table class="table table-striped table-hover" id="images">
    @foreach ($data->results as $column => $result)
        @if (!$column)
            <thead>  <tr>
                <?php $first = true; ?>
                @foreach ($result as $key => $value)
                    <?php if ($first) {$first = false; continue;}  ?>
                    <th> {{ $key }} </th>
                @endforeach
                <th class="align-right"> Opções </th>
            </tr> </thead>
        @endif
        <tr>
            <?php $keyValue = false; $i = 0; $len = count($result); ?>
            @foreach ($result as $key => $value)
                <?php
                    if (!$keyValue and $key == $modelKey) {
                        $keyValue = $value; continue;
                    }
                    $i++;
                ?>
                <td>
                    @if($key == 'user')
                    <?php $user = unserialize($value); ?>
                    {{  $user->user_name }}
                    @else
                        @if($key == 'instalado')
                            @if($result->instalado)
                                Sim
                            @else
                                Não
                            @endif
                        @else
                            {{ Str::words(strip_tags($value), 10) }}
                        @endif
                    @endif                    
                </td>

            @endforeach
            <td class="align-right">
                @if ($result->instalado)
                    {{ HTML::link_to_action($url_modulos, 'Recriar Permissões', array('permissions',$result->id)); }}
                    {{ HTML::link_to_action($url_modulos, 'Desistalar', array('uninstall',$result->id)); }}
                @else
                    {{ HTML::link_to_action($url_modulos, 'Instalar', array('install',$result->id)); }}
                    <a class="btn" href="{{ "{$url_edit}/{$keyValue}" }}"><i class="icon-edit"></i></a>
                    <a href="#modal" role="button" class="btn delete" data-key="{{$modelKey}}" data-value="{{$keyValue}}" data-toggle="modal"><i class="icon-remove"></i></a>
                @endif
            </td>
        </tr>
    @endforeach
</table>

{{-- $data->links() --}}

{{ $data->appends(array('sort' => 'id'))->links() }}

<div class="modal hide fade static" id="modal"  role="dialog" aria-labelledby="modal" aria-hidden="true" >
  <div class="modal-header ">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Realmente deseja deletar o registro?</h3>
  </div>
  <div class="modal-body">
    <p>Os dados deletados não poderão ser recuperados.</p>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
    {{ Form::open($url_delete, 'POST', array('style'=>'display:inline' , 'id'=>'formDelete')) }}
        {{ Form::hidden('' , '') }}
        <button class="btn btn-danger">Deletar</button>
    {{ Form::close() }}
  </div>
</div>