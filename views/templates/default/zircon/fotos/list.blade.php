{{ Dash\Messages::get_html() }}
@if(isset($data))
  <ul class="thumbnails sortable">
    @foreach($data as $fotos)
  		  <li class="span3 " style="background: #fff" rel="{{ $fotos->id }}" >
            <div class="thumbnail">
              <a href="{{ URL::to("uploads/{$fotos->foto}") }}" title="{{ $fotos->alt }}" class="itens">
                {{-- \Thumburl\Html::thumbnail(\Config::get('zircon::settings.upload_path') . "$fotos->foto", '260X200', 'outbound', $fotos->alt)--}}
                {{ HTML::image(\Config::get('zircon::settings.upload_path') . "$fotos->foto",$fotos->foto, array('width'=>260)) }}
              </a>
              <div class="caption">
                <b>{{ $fotos->alt }}</b>
                <p>{{ $fotos->legenda }}</p>
                <p>
                  <a href="{{ Url::to("zircon/modulos/fotos/edit/{$fotos->id}") }}" class="btn btn-primary">Editar</a>
                  <a href="#modal" role="button" class="btn btn-danger delete" data-key="{{$modelKey}}" data-value="{{$fotos->id}}" data-toggle="modal">Deletar</a>
                </p>
              </div>
            </div>
          </li>
  	@endforeach
  </ul>
@else
<p>Nenhuma foto encontrada</p>
@endif

<div class="modal hide fade static" id="modal"  role="dialog" aria-labelledby="modal" aria-hidden="true" >
  <div class="modal-header ">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Realmente deseja deletar o registro?</h3>
  </div>
  <div class="modal-body">
    <p>Os dados deletados não poderão ser recuperados.</p>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
    @if(isset($buttons))
        {{ Form::open(URL::home() . "zircon/modulos/{$controller}/delete", 'POST', array('style'=>'display:inline' , 'id'=>'formDelete')) }}
    @else
        {{ Form::open(URL::home() . "zircon/{$controller}/delete", 'POST', array('style'=>'display:inline' , 'id'=>'formDelete')) }}
    @endif
        {{ Form::hidden($modelKey , '') }}
        
        {{ Form::hidden(URI::segment(5) , URI::segment(6)) }}
        
        <button class="btn btn-danger">Deletar</button>
    {{ Form::close() }}
  </div>
</div>

@if((isset($module) and strlen($module)>0) and (isset($rel_id) and strlen($rel_id)>0))
<a href="{{ Request::referrer() }}" class="btn">Voltar</a>
<a href="{{ Url::to("zircon/modulos/fotos/add/{$module}/{$rel_id}") }}" class="btn btn-primary right">Adicionar Imagem</a>
@endif