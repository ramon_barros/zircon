<!--
	Utilize o bootstrap
-->
<div class="well">
	<p>Utilize o menu acima para acessar as sess&otilde;es dispon&iacute;veis. Ao clicar em algum item, ser&aacute; exibida a p&aacute;gina de visualiza&ccedil;&atilde;o de conte&uacute;do respectivo &aacute; sess&atilde;o selecionada. Nesta p&aacute;gina voc&ecirc; pode realizar a inclus&atilde;o de novas informa&ccedil;&otilde;es, fazer a edi&ccedil;&atilde;o dos itens dispon&iacute;veis e at&eacute; mesmo exclu&iacute;r algo que voc&ecirc; n&atilde;o queira mais. </p>
    <p>O Sitema de gerenciamento de conte&uacute;do - <strong>eZoom Ag&ecirc;ncia Digital</strong> foi desenvolvido para que seja uma ferramenta &uacute;til e de f&aacute;cil utiliza&ccedil;&atilde;o por ter uma interface simples e intu&iacute;tiva.</p>
	<p>Qualquer d&uacute;vida, sugest&atilde;o ou problema que voc&ecirc; possa ter, enviar-nos uma mensagem atrav&eacute;s do email <a href="mailto:ezoom@ezoom.com.br">ezoom@ezoom.com.br.</a> Em breve estaremos respondendo sua mensagem.</p>
	<p class="bemVindo">Seja bem vindo!</p>
</div>