@if(isset($modulos))
  {{ Form::open_for_files(URL::home() . "zircon/{$controller}/permissions", 'POST', array('class' => 'form-horizontal')) }}     
    {{ Form::hidden($modelKey, $keyValue) }}
    
    @foreach($groups as $group)
    <?php
      $grupo = (array)json_decode($group['permissions']);
      $grupo_rule = array_keys($grupo);
      $grupo_rule = $grupo_rule[0];
    ?>
      {{ Form::hidden('grupo',$grupo_rule)}}
    @endforeach

    @foreach($modulos as $name=>$rules)
      <div class="control-group">
        <label class="label-label">{{$name}}</label>
        <div>
          @foreach($rules as $permissions=>$value)
              @if(array_key_exists('superuser',$rules) or array_key_exists('is_admin',$rules) or array_key_exists('user',$rules))
                  @if($permissions==$grupo_rule)
                    @foreach ($rules[$permissions] as $groups=>$permission)
                      @if(in_array($groups,array_keys($data)) and $data[$groups]===1)
                        {{ Form::checkbox($groups,true,true) }}
                      @else
                        {{ Form::checkbox($groups,true) }}
                      @endif
                      @if(is_array($permission))
                        <small>{{$permission[1]}}</small>
                      @endif
                    @endforeach
                  @endif
              @else
                @if(in_array($permissions,array_keys($data)) and $data[$permissions]===1)
                  {{ Form::checkbox($permissions,true,true) }}
                @else
                  {{ Form::checkbox($permissions,true) }}
                @endif
                @if(is_array($value))
                  <small>{{$value[1]}}</small>
                @endif
              @endif  
          @endforeach
        </div>
      </div>
    @endforeach
    <a href="{{ Request::referrer() }}" class="btn">Voltar</a>
    <button type="submit" class="btn">Salvar</button>
  {{ Form::close() }}    
@else
  <p>Nenhum modulo instalado.</p>
@endif

