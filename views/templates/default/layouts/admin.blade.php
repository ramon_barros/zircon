<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="{{ Config::get('application.language') }}"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="{{ Config::get('application.language') }}"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="{{ Config::get('application.language') }}"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="{{ Config::get('application.language') }}"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>{{$title}}</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width">
    <script type="text/javascript">
        var asset = function  (file) {
            return '{{asset('')}}' + file;
        };
        var url = function  (to) {
            return '{{URL::base()}}' + "/" + to;
        };
    </script>
    {{ Asset::container('bootstrapper')->styles() }}
    {{ Asset::container('bootstrapper')->scripts() }}
    {{ Asset::styles() }}
    {{ Asset::scripts() }}
</head>
<body>
    <!--[if lt IE 7]><p class=chromeframe>Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]-->
        {{ $menu }}
        {{ $content }}
      <footer>
        <!-- <p>Powered by <a href="http://ezoom.com.br" target="_bank">ezoom</a> 2012 </p> -->
      </footer>

    </div> <!-- /container -->

</body>
</html>