<?php

class Agendas extends Eloquent {
	
    public static $timestamps = false;
    /**
     * Primary key
     * @var string
     */
    public static $key = 'id';
    
    /**
     * Tabela 
     * @var string
     */
    public static $table = 'agendas';

    /**
     * Mostra o modulo no menu principal
     * @var boolean
     */
    public static $menu = true;

    /**
     * Editor utilizado
     * @var string
     */
	public static $editor = 'wysiwyg';

    /**
     * Nome do modulo
     * @var string
     */
    public static $modulo = 'agendas';

    /**
     * Mostra o campo de busca
     * @var boolean
     */
	public static $search = true;

    /**
     * Subtitulo e submenus
     * @var array
     */
    public static $admin = array(
                            'subtitle' =>'Agendas',
                            /*
                            'submenu' => array(
                                 'zircon/modulos/contatos/categorias'    => 'Categorias' 
                                ,'zircon/modulos/contatos/subcategorias' => 'Subcategorias'
                            )
                            */
                        );

    /**
     * Retorna primary key
     * @return integer 
     */
    public static function key(){
        return static::$key;
    }

    /**
     * Criar a tabela do modulo
     * @return boolean retorna se a tabela existe
     */
	public static function up(){
        return array(
            'create_'.static::$table => array(
                 static::$key.':integer'
                ,'email:string:unique'
                ,'nome:string:nullable'
            )
            ,
            'create_'.static::$table.'_fotos' => array(
                 'id:integer'
                ,'agenda_id:integer:unique'
                ,'foto_id:integer:unique'
            )
            
        );
	}

    /**
     * Deleta a tabela do modulo
     * @return boolean retorna se a tabela existe
     */
	public static function down(){
		return Schema::exists(static::$table);
	}

    /**
     * Permissões do modulo
     * @return array retorna as permissões do modulo
     */
    public static function permissions(){
        $permissions = array(
            'superuser' => array(
                 static::$modulo."_edit"   => array(1,'Editar') 
                ,static::$modulo."_add"    => array(1,'Adicionar')
                ,static::$modulo."_delete" => array(1,'Deletar')
            ),
            'is_admin' => array(
                 static::$modulo."_edit"   => array(1,'Editar') 
                ,static::$modulo."_add"    => array(1,'Adicionar')
                ,static::$modulo."_delete" => array(1,'Deletar')
            ),
            'user' => array(
                 static::$modulo."_edit"   => array(0,'Editar') 
                ,static::$modulo."_add"    => array(0,'Adicionar')
                ,static::$modulo."_delete" => array(0,'Deletar')
            ),
        );
        
        return  $permissions;
    }

    /**
     * Colunas para construção do formulário
     *
     * @return array
     */
    public static function columns()
    {
        return array(
            static::key() => array(
                'input' => function($val = '') {
                    return Form::hidden(Agendas::key() , $val);
                },

            ),
            'nome' => array(
                'name' => 'Nome',
                 'label' =>  function() {
                    return Form::label('nome', 'Nome', array( 'class' => 'large-label'));
                },
                'input' => function($val = '') {

                    return Form::text('nome', $val, array('class'=>'x-large'));
                },
            ),
        );
    }

    /**
     * Paginação
     * @return [type] [description]
     */
    public static function paginate()
    {
        $columns = array(
            'id',
            'nome'
        );
        return DB::table(static::$table)->paginate(Config::get('zircon::settings.per_page'), $columns);
    }

    /**
     * Validação dos campos do formulário
     *
     * @return array
     */
    public static function rules()
    {
        return array(
            'nome' => 'required'
        );
    }

    public static function search(){
        return DB::table(static::$modulo)
                    ->where(function($query)
                    {
                        $input = Input::all();
                        $terms = explode(' ', $input['search-query']);
                        foreach (Agendas::columns() as $key => $value) {
                            foreach ($terms as $value) {
                                if($key=='id') continue;
                               $query->or_where($key, 'LIKE', '%'.$value.'%');
                            }
                        }                       
                    })->get();
                    
    }
}