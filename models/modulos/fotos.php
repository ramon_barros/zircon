<?php

class Fotos extends Eloquent {
  
    public static $timestamps = true;
    public static $key = 'id';
    public static $table = 'fotos';
    public static $menu = false;
    public static $modulo = 'fotos';
    public static $search = true;
    public static $referrer = false;
    public static $admin = array(
        'subtitle' =>'Fotos'
    );

    /**
     * Establish the relationship between a fotos and produtos.
     *
     * @return Laravel\Database\Eloquent\Relationships\Has_Many
     */
    public function produtos()
    {
        return $this->has_many_and_belongs_to('Produtos', 'produtos_fotos');
    }

    /**
     * Retorna primary key
     * @return integer 
     */
    public static function key(){
        return static::$key;
    }

    /**
     * Criar a tabela do modulo
     * @return boolean retorna se a tabela existe
     */
  public static function up(){
    return array(
            'create_'.static::$table => array(
                 static::$key.':integer'
                ,'legenda:string'
                ,'foto:string'
                ,'ordem:integer'
            )            
        );
  }

    /**
     * Deleta a tabela do modulo
     * @return boolean retorna se a tabela existe
     */
  public static function down(){
    return Schema::exists(static::$table);
  }

    /**
     * Permissões do modulo
     * @return array retorna as permissões do modulo
     */
    public static function permissions(){
        $permissions = array(
             static::$modulo."_edit"   => array(1,'Editar') 
            ,static::$modulo."_add"    => array(1,'Adicionar')
            ,static::$modulo."_delete" => array(1,'Deletar')
        );
        
        return  $permissions;
    }

    /**
     * Colunas para construção do formulário
     *
     * @return array
     */
    public static function columns()
    {
        return array(
            static::key() => array(
                'input' => function($val = '') {
                    return Form::hidden(Fotos::key() , $val);
                }

            ),
            URI::segment(5) => array(
                'input' => function($val = '') {
                    return Form::hidden(URI::segment(5),URI::segment(6));
                }
            ),
            'ordem' => array(
                'name' => 'ordem',
                 'label' =>  function() {
                    return Form::label('ordem', 'ordem', array( 'class' => 'large-label'));
                },
                'input' => function($val = '') {
                    return Form::text('ordem', $val, array('class'=>''));
                }
            ),
            'legenda' => array(
                'name' => 'Legenda',
                 'label' =>  function() {
                    return Form::label('legenda', 'Legenda', array( 'class' => 'large-label'));
                },
                'input' => function($val = '') {

                    return Form::text('legenda', $val, array('class'=>''));
                }
            ),
            'foto' => array(
                'name' => 'foto',
                'label' =>  function() {
                    return Form::label('foto', 'Foto', array( 'class' => 'large-label'));
                },
                'input' => function($val = '') {
                    $str = '';
                    if(is_file(path('public') . \Config::get('zircon::settings.upload_path') . $val)) {
                        $str  .= HTML::image(\Config::get('zircon::settings.upload_path') . "/" . $val, '');
                        $str  .= Form::hidden('foto' , $val);
                    }
                    return Form::upload('foto',array('class'=>''));
                }
            )
        );
    }

    /**
     * Paginação
     * @return [type] [description]
     */
    public static function paginate()
    {
        $items = array(
            'id',
            'ordem'
            ,'legenda'
            ,'foto'
        );
        return DB::table(static::$table)->paginate(Config::get('zircon::settings.per_page'), $items);
    }

    public static function data()
    {
        $model = URI::segment(5);
        $id    = URI::segment(6);
        if(Schema::exists($model.'_fotos')){
            return (isset($model) and isset($id))?$model::find($id)->fotos:array();
        }else{
            return array('error'=>true,'error_msg'=>'Tabela '.$model.'_fotos não existe!');
        }
    }

    /**
     * Validação dos campos do formulário
     *
     * @return array
     */
    public static function rules()
    {
        return array(
            'foto' => 'required'
        );
    }

    public static function search(){
        return DB::table(static::$table)
                    ->where(function($query)
                    {
                        $input = Input::all();
                        $terms = explode(' ', $input['search-query']);
                        foreach (static::columns() as $key => $value) {
                            foreach ($terms as $value) {
                                if($key=='id') continue;
                               $query->or_where($key, 'LIKE', '%'.$value.'%');
                            }
                        }                       
                    })->get();
                    
    }
}