<?php

class Contatos extends Eloquent {
    
    public static $timestamps = false;
    public static $key = 'id';
    public static $table = 'contatos';
    public static $menu = true;
    public static $editor = 'wysiwyg';
    public static $modulo = 'contatos';
    public static $search = true;
    public static $referrer = false;
    
    /**
     * Establish the relationship between a produtos and fotos.
     *
     * @return Laravel\Database\Eloquent\Relationships\Has_Many
     */
    public function fotos()
    {
        return $this->has_many_and_belongs_to('Fotos', 'contatos_fotos');
    }

    /**
     * Retorna primary key
     * @return integer 
     */
    public static function key(){
        return static::$key;
    }

    /**
     * Criar a tabela do modulo
     * @relation fotos
     * @return boolean retorna se a tabela existe
     */
    public static function up(){
        return array(
            'create_'.static::$table => array(
                 static::$key.':integer'
                ,'nome:string:nullable'
            )
            ,
            'create_'.static::$table.'_fotos' => array(
                 'id:integer'
                ,'agenda_id:integer:unique'
                ,'foto_id:integer:unique'
            )
            
        );
    }

    /**
     * Deleta a tabela do modulo
     * @return boolean retorna se a tabela existe
     */
    public static function down(){
        return Schema::exists(static::$table);
    }

    /**
     * Permissões do modulo
     * @return array retorna as permissões do modulo
     */
    public static function permissions(){
        $permissions = array(
            'superuser' => array(
                 static::$modulo."_edit"   => array(1,'Editar') 
                ,static::$modulo."_add"    => array(1,'Adicionar')
                ,static::$modulo."_delete" => array(1,'Deletar')
            ),
            'is_admin' => array(
                 static::$modulo."_edit"   => array(1,'Editar') 
                ,static::$modulo."_add"    => array(1,'Adicionar')
                ,static::$modulo."_delete" => array(1,'Deletar')
            ),
            'user' => array(
                 static::$modulo."_edit"   => array(0,'Editar') 
                ,static::$modulo."_add"    => array(0,'Adicionar')
                ,static::$modulo."_delete" => array(0,'Deletar')
            ),
        );
        
        return  $permissions;
    }

    /**
     * Permissões dos compos do formulário
     * @return array retorna as permissões dos compos do formulário
     */
    public static function columns_permissions(){
        $permissions = array(
            'superuser' => array(
                'all'
            ),
            'is_admin' => array(
               'all'
            ),
            'user' => array(
                'all'
            ),
        );
        
        return  $permissions;
    }

    /**
     * Colunas para construção do formulário
     *
     * @return array
     */
    public static function columns()
    {
        return array(
            static::key() => array(
                'input' => function($val = '') {
                    return Form::hidden(static::key() , $val);
                },

            ),
            'nome' => array(
                'name' => 'Nome',
                 'label' =>  function() {
                    return Form::label('nome', 'Nome', array( 'class' => 'large-label'));
                },
                'input' => function($val = '') {

                    return Form::text('nome', $val, array('class'=>'x-large'));
                },
            )
        );
    }

    /**
     * Paginação
     * @return [type] [description]
     */
    public static function paginate()
    {
        $items = array(
            'id',
            'nome'
        );
        return DB::table(static::$modulo)->paginate(Config::get('zircon::settings.per_page'), $items);
    }

    /**
     * Validação dos campos do formulário
     *
     * @return array
     */
    public static function rules()
    {
        return array(
            'nome' => 'required'
        );
    }

    public static function search(){
        return DB::table(static::$table)
                    ->where(function($query)
                    {
                        $input = Input::all();
                        $terms = explode(' ', $input['search-query']);
                        foreach (static::columns() as $key => $value) {
                            foreach ($terms as $value) {
                                if($key=='id') continue;
                               $query->or_where($key, 'LIKE', '%'.$value.'%');
                            }
                        }                       
                    })->get();
                    
    }

    public static function buttons(){
        $buttons = Zircon_Model_Modulo::modulo_buttons(static::$modulo);
        //if(Modulo::modulo_user_permissions(static::$modulo,'fotos')){   
            $buttons->fotos = '<a class="btn" href="'.URL::to("zircon/modulos/fotos/index/:controller/:keyValue").'"><i class="icon-picture"></i></a>';
        //}
        return $buttons;
    }
}