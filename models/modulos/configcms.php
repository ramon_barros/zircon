<?php

class ConfigCms extends Eloquent {
	
    public static $timestamps = false;
    /**
     * Primary key
     * @var string
     */
    public static $key = 'id';
    
    /**
     * Tabela 
     * @var string
     */
    public static $table = 'config';

    /**
     * Mostra o modulo no menu principal
     * @var boolean
     */
    public static $menu = true;

    /**
     * Editor utilizado
     * @var string
     */
	public static $editor = 'wysiwyg';

    /**
     * Nome do modulo
     * @var string
     */
    public static $modulo = 'configcms';

    /**
     * Mostra o campo de busca
     * @var boolean
     */
	public static $search = false;

    /**
     * Texto que será mostrado no campo busca
     * @var string
     */
    public static $search_placeholder;

    /**
     * Subtitulo e submenus
     * @var array
     */
    public static $admin = array(
                            'subtitle' =>'Configuração CMS'
                        );
    /**
     * Salva o tipo de busca db ou sap
     */
    public static $search_type;
    
    /**
     * Salva os resultados da busca
     */
    public static $search_result;

    /**
     * Retorna primary key
     * @return integer 
     */
    public static function key(){
        return static::$key;
    }

    /**
     * Criar a tabela do modulo
     * @return boolean retorna se a tabela existe
     */
	public static function up(){
        return array(
            'create_'.static::$table => array(
                 static::$key.':integer'
                ,'smtp:string:nullable'
                ,'porta:string[20]:nullable'
                ,'email:string:nullable'
                ,'senha:string:nullable'
                ,'de:string:nullable'
                ,'para:string:nullable'
                ,'title:string:nullable'
            )
        );
    }

    /**
     * Deleta a tabela do modulo
     * @return boolean retorna se a tabela existe
     */
	public static function down(){
        return Schema::exists(ConfigCms::$table);
    }

    /**
     * Permissões do modulo
     * @return array retorna as permissões do modulo
     */
    public static function permissions(){
        $permissions = array(
            'superuser' => array(
                 static::$modulo."_edit"   => array(1,'Editar') 
                ,static::$modulo."_add"    => array(1,'Adicionar')
                ,static::$modulo."_delete" => array(1,'Deletar')
            ),
            'is_admin' => array(
                 static::$modulo."_edit"   => array(1,'Editar') 
                ,static::$modulo."_add"    => array(1,'Adicionar')
                ,static::$modulo."_delete" => array(1,'Deletar')
            ),
            'user' => array(
                 static::$modulo."_edit"   => array(0,'Editar') 
                ,static::$modulo."_add"    => array(0,'Adicionar')
                ,static::$modulo."_delete" => array(0,'Deletar')
            ),
        );
        
        return  $permissions;
    }

    /**
     * Colunas para construção do formulário
     *
     * @return array
     */
    public static function columns()
    {
        return array(
            static::key() => array(
                'input' => function($val = '') {
                    return Form::hidden(ConfigCms::key() , $val);
                },

            ),
            'title' => array(
                'name' => 'Nome do Sistema',
                 'label' =>  function() {
                    return Form::label('title', 'Nome do Sistema', array( 'class' => 'x-large', 'required'));
                },
                'input' => function($val = '') {
                    return Form::text('title', $val, array('class'=>''));
                },
            ),
            'smtp' => array(
                'name' => 'SMTP',
                 'label' =>  function() {
                    return Form::label('smtp', 'SMTP', array( 'class' => 'x-large', 'required'));
                },
                'input' => function($val = '') {
                    return Form::text('smtp', $val, array('class'=>''));
                },
            ),
            'porta' => array(
                'name' => 'Porta',
                 'label' =>  function() {
                    return Form::label('porta', 'Porta', array( 'class' => 'x-large', 'required'));
                },
                'input' => function($val = '') {
                    return Form::text('porta', $val, array('class'=>''));
                },
            ),
            'email' => array(
                'name' => 'E-mail',
                 'label' =>  function() {
                    return Form::label('email', 'E-mail', array( 'class' => 'x-large', 'required'));
                },
                'input' => function($val = '') {
                    return Form::text('email', $val, array('class'=>''));
                },
            ),
            'senha' => array(
                'name' => 'Senha',
                 'label' =>  function() {
                    return Form::label('senha', 'Senha', array( 'class' => 'x-large', 'required'));
                },
                'input' => function($val = '') {
                    return Form::text('senha', $val, array('class'=>''));
                },
            ),
            'de' => array(
                'name' => 'De',
                 'label' =>  function() {
                    return Form::label('de', 'De', array( 'class' => 'x-large', 'required'));
                },
                'input' => function($val = '') {
                    return Form::text('de', $val, array('class'=>''));
                },
            ),
            'para' => array(
                'name' => 'Para',
                 'label' =>  function() {
                    return Form::label('para', 'Para', array( 'class' => 'x-large', 'required'));
                },
                'input' => function($val = '') {
                    return Form::text('para', $val, array('class'=>''));
                },
            ),
            /*
            'maximo_parcelas' => array(
                'name' => 'Máximo de parcela',
                 'label' =>  function() {
                    return Form::label('maximo_parcelas', 'Máximo de parcela', array( 'class' => 'x-large', 'required'));
                },
                'input' => function($val = '') {
                    return Form::text('maximo_parcelas', $val, array('class'=>''));
                },
            ),
            // Tipo de Parcelamento PL (Parcelado pela Loja) ou PA (Parcelado pela Administradora)
            'tipo_parcelamento' => array(
                'name' => 'Tipo de Parcelamento',
                'label' =>  function() {
                    return Form::label('tipo_parcelamento', 'Tipo de Parcelamento', array( 'class' => 'large-label'));
                },
                'input' => function($val = '') {
                     $tipo_parcelamento = array();

                     $tipo_parcelamento['PL'] = 'Parcelado pela Loja';
                     $tipo_parcelamento['PA'] = 'Parcelado pela Administradora';

                    return Form::select('tipo_parcelamento', $tipo_parcelamento, $val);
                },
            ),
            */
        );
    }

    /**
     * Paginação
     * @return [type] [description]
     */
    public static function paginate()
    {
        $columns = array(
            'id'
           ,'smtp'
           ,'email'
           ,'para'
        );
        return DB::table(static::$table)->paginate(Config::get('zircon::settings.per_page'), $columns);
    }

    /**
     * Validação dos campos do formulário
     *
     * @return array
     */
    public static function rules()
    {
        return array(
           
        );
    }

    public static function search(){
        return DB::table(static::$table)
                    ->where(function($query)
                    {
                        $input = Input::all();
                        $terms = explode(' ', $input['search-query']);
                        foreach (static::columns() as $key => $value) {
                            foreach ($terms as $value) {
                                if($key=='id') continue;
                               $query->or_where($key, 'LIKE', '%'.$value.'%');
                            }
                        }                       
                    })->get();
                    
    }
}