<?php

class Zircon_Create_Config {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */    

	public function up()
    {
		Schema::create('config', function($table) {
			$table->increments('id')->unsigned();
			$table->string('smtp')->nullable();
			$table->string('porta',20)->nullable();
			$table->string('email')->nullable();
			$table->string('senha')->nullable();
			$table->string('de')->nullable();
			$table->string('para')->nullable();
			$table->string('title')->nullable();
			$table->timestamps();
	});

    }

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */    

	public function down()
    {
		Schema::drop('config');

    }

}