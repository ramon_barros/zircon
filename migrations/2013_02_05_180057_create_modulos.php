<?php

class Zircon_Create_Modulos {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('modulos',function($table){
			$table->create();
			$table->increments('id')->unsigned();
			$table->integer('cod');
			$table->string('logo')->nullable();
			$table->string('nome');
			$table->boolean('instalado')->nullable();
			$table->timestamps();
		});
		
		/**
		 * Insere rules
		 * superuser
		 * is_admin
		 * user
		 */
		DB::connection(Config::get('sentry::sentry.db_instance'))
			->table(Config::get('sentry::sentry.table.rules'))
			->insert(array('rule' => 'user', 'description' => 'Access to Limited'));

		/**
		 * Insere grupos:
		 * Superuser ( Não é visivel )
		 * Administradores
		 * Clientes
		 */
		DB::connection(Config::get('sentry::sentry.db_instance'))
			->table(Config::get('sentry::sentry.table.groups'))
			->insert(array(
					array('name' => 'Superuser', 'permissions' => '{"superuser":1}'),
					array('name' => 'Administradores', 'permissions' => '{"is_admin":1}'),
					array('name' => 'Usuários', 'permissions' => '{"user":1}')
					));

		/**
		 * Insere usuário da Ezoom
		 */
		DB::connection(Config::get('sentry::sentry.db_instance'))
			->table(Config::get('sentry::sentry.table.users'))
			->insert(array(
						 'username'				=>'zircon'
						,'email'				=>'contato@nocriz.com'
						,'password'				=>'hNDiuK7kLgwy4do339b21fcce0344d8d393874455d3a78f9fa63eb88caf66f8274523ac70cc995b3'
						,'password_reset_hash'	=>''
						,'temp_password'		=>''
						,'remember_me'			=>'3hJGTVmwv7fVE2Il3e7894b0ac2664ceaa4cf0618084dd28e2d8a2f8187576ec60bde63899668f03'
						,'activation_hash'		=>''
						,'ip_address'			=>'127.0.0.1'
						,'status'				=>'1'
						,'activated'			=>'1'
						,'permissions'			=>''
						,'created_at'			=>date('Y-m-d H:i:s')
					));

		/**
		 * Retorna o id do superuser
		 * @var string
		 */
		$user_id = DB::table(Config::get('sentry::sentry.table.users'))->where('email','=','contato@nocriz.com')->only('id');
		/**
		 * Retorna o id do grupo Superuser
		 * @var string
		 */
		$group_id = DB::table(Config::get('sentry::sentry.table.groups'))->where('name','=','Superuser')->only('id');
		
		/**
		 * Insere o usuário ezoom no grupo Superuser
		 */
		DB::connection(Config::get('sentry::sentry.db_instance'))
			->table(Config::get('sentry::sentry.table.users_groups'))
			->insert(array(
					 'user_id' => $user_id
					,'group_id' => $group_id
				));	
		/**
		 * Insere informações do usuário
		 */
		DB::connection(Config::get('sentry::sentry.db_instance'))
			->table(Config::get('sentry::sentry.table.users_metadata'))
			->insert(array(
					 'user_id' => $user_id
					,'first_name' => 'Super'
					,'last_name' => 'User'
				));	
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('modulos', function($table) {
			$table->drop();
		});

		DB::connection(Config::get('sentry::sentry.db_instance'))
			->table(Config::get('sentry::sentry.table.rules'))
			->where('rule','=','user')
			->delete();

		DB::connection(Config::get('sentry::sentry.db_instance'))
			->table(Config::get('sentry::sentry.table.groups'))
			->delete();

		DB::connection(Config::get('sentry::sentry.db_instance'))
			->table(Config::get('sentry::sentry.table.users'))
			->delete();

		DB::connection(Config::get('sentry::sentry.db_instance'))
			->table(Config::get('sentry::sentry.table.users_groups'))
			->delete();

		DB::connection(Config::get('sentry::sentry.db_instance'))
			->table(Config::get('sentry::sentry.table.users_metadata'))
			->delete();		
	}

}