<?php

return array(

     'title' => 'ZirconCMS'

    ,'editor' => 'wysiwyg'
    /*
    |--------------------------------------------------------------------------
    | Itens por página
    |--------------------------------------------------------------------------
    |Numero de itens a serem exibidos em paginações
    */

    ,'per_page' => 10

    /*
    |--------------------------------------------------------------------------
    | Inputs para upload
    |--------------------------------------------------------------------------
    |
    |Define os campos inputs que deverão ser tratados como arquivos para upload
    |
    */

    ,'uploader' => array(
         'foto' 
        ,'logo' 
        ,'file' 
    )

    /*
    |--------------------------------------------------------------------------
    | Inputs dinheiro
    |--------------------------------------------------------------------------
    |
    |Define os campos inputs que deverão ser tratados como moeda (decimal),
    |ao enviar para o banco de dados é removido a virgula e o inverso os 
    |mostrar para o usuário
    |
    */
    ,'money' => array(
        'valor'
    )

    /*
    |--------------------------------------------------------------------------
    | Inputs date
    |--------------------------------------------------------------------------
    |
    |Define os campos inputs que deverão ser tratados como data (dete),
    |ao enviar para o banco de dados é converido no formato yyyy-mm-dd e ao
    |mostrar para o usuário
    |
    */
    ,'date' => array(
         'data'
        ,'published_at'
        ,'publicado'
    )

    /*
    |--------------------------------------------------------------------------
    | Ajax
    |--------------------------------------------------------------------------
    |
    |Métodos que servirão para requisições ajax dentro do CMS
    |
    */
    ,'ajax' => array(
         'update_order'
        ,'change_status'
        ,'promo'
    )

    /*
    |--------------------------------------------------------------------------
    | Plugins
    |--------------------------------------------------------------------------
    |
    |Plugins (css e js) que serão carregados no CMS.
    |
    */
    ,'plugins' => array(
         'bootstrap-fileupload.min.css'
        ,'bootstrap-fileupload.min.js'
        ,'bootstrap-datepicker.css'
        ,'bootstrap-datepicker.js'
        ,'bootstrap-datepicker.pt-BR.js'
        ,'datepicker.js'
    )

     /*
    |--------------------------------------------------------------------------
    | upload_path
    |--------------------------------------------------------------------------
    |Local onde serão salvos os arquivos
    */

    ,'upload_path' => 'uploads/'

    /**
     * Pasta onde contem os temas
     */
    ,'view' => 'templates'

    /**
     * Tema selecionado
     */
    ,'template' => 'default'

    /**
     * layout utilizado
     */
    ,'layout' => 'layouts.admin'

    /**
     * Rotas para o CMS
     */
    ,'routes' => ''
);