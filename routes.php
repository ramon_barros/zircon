<?php
/**
 * Zircon CMS - A PHP CMS For Web Artisans
 *
 * @package  CMS
 * @version  3.0
 * @author   Ramon Barros <contato@ramon-barros.com>
 * @link     http://ramon-barros.com
 */

/**
 * Load the Routes Docs.
 */
if(file_exists(Bundle::path('zircon').'docs'.DS.'routes.php')){
    require_once Bundle::path('zircon').'docs'.DS.'routes.php';
}

/**
 * Rotas customizadas
 */
if(file_exists(Bundle::path('zircon').'libraries/Core/Routes/routes.php')){
    require_once Bundle::path('zircon').'libraries/Core/Routes/routes.php';
}

/**
 * Arquivo que define as rotas do CMS.
 */
Route::any('(:bundle)/modulos', function() {
    return Controller::call("zircon::core.modulos@index");
});

Route::any('(:bundle)/modulos/(:all?)', function($modulos = '') {
    $modulos= explode('/', $modulos);
    $method = isset($modulos[0])?$modulos[0]:'index';
    array_shift($modulos);
    return Controller::call("zircon::core.modulos@{$method}", $modulos);
});

Route::any('(:bundle)/users', function() {
    return Controller::call("zircon::core.users@index");
});

Route::any('(:bundle)/users/(:all?)', function($route = '') {
    $route= explode('/', $route);
    $method = isset($route[0])?$route[0]:'index';
    array_shift($route);
    return Controller::call("zircon::core.users@{$method}", $route);
});

Route::any('(:bundle)', function() {
    return Controller::call("zircon::core.admin@index");
});

Route::any('(:bundle)/(:all?)', function($route = '') {
    $route= explode('/', $route);
    $method = isset($route[0])?$route[0]:'index';
    array_shift($route);

    return Controller::call("zircon::core.admin@{$method}", $route);
});

/**
 * Filtro zirconcoreauth redireciona para o zircon/auth
 * zircon::core.admin
 * controllers/core/admin.php
 * Zircon_Core_Admin_Controller
 */
Route::filter('zirconcoreauth',function(){
    if (!Sentry::check()) return Redirect::to('zircon/auth');
});