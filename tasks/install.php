<?php

/**
 * Laravel CMS Install
 * 
 * 
 * @author      Ramon Barros <ramon@ezoom.com.br>
 * @version     1.0
 * @since       May 09, 2013
 *
 */

use \Laravel\CLI\Command as Command; 

class Zircon_Install_Task {

	/*
		php artisan migrate:install --env=local
		php artisan migrate sentry --env=local
		php artisan migrate admin --env=local
		php artisan migrate --env=local
	 */
	
	/**
	 * Run task
	 * @return void
	 */
	public function help() {

echo <<<EOT
\n=============INSTALL COMMANDS=============\n        
zircon::install:help [this]
zircon::install:migrate
zircon::install:sentry
zircon::install:admin
zircon::install:all
\n=====================END====================\n
EOT;

	}

    public function run()
    {
        return $this->help();
    }

	public function m(){ return $this->migrations(); }
	public function s(){ return $this->sentry(); }
	public function a(){ return $this->admin(); }
	public function all(){
		$this->migrations();
		$this->sentry();
		$this->admin();
		$this->modulos();
	}

	public function migrations(){
		return $this->create_table_migrations();
	}

	public function sentry(){
		return $this->create_table_sentry();
	}

	public function admin(){
		return $this->create_table_admin();
	}

	public function modulos(){
		return $this->create_table_modulos();
	}

	/**
	 * Create table laravel_migrations = php artisan migrate:install --env=local
	 * @return [type] [description]
	 */
	protected function create_table_migrations(){
		$laravel_migrations = Command::run(array(
									'migrate:install'
									,'--env=local'
								));
		return $laravel_migrations;
	}

	/**
	 * Create table sentry = php artisan migrate sentry --env=local
	 * @return [type] [description]
	 */
	protected function create_table_sentry(){
		$sentry = Command::run(array(
						'migrate'
						,'sentry'
						,'--env=local'
					));
		return $sentry;
	}

	/**
	 * Create table zircon = php artisan migrate zircon --env=local
	 * @return [type] [description]
	 */
	protected function create_table_admin(){
		$zircon = Command::run(array(
					'migrate'
					,'zircon'
					,'--env=local'
				));
		return $zircon;
	}

	/**
	 * Create table modules = php artisan migrate zircon --env=local
	 * @return [type] [description]
	 */
	protected function create_table_modulos(){
		$modulos = Command::run(array(
						'migrate'
						,'zircon'
						,'--env=local'
					));
	}
}