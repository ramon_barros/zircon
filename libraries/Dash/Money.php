<?php namespace Dash;

/**
 * Converte o decimal do banco de dados para R$
 *
 * @param  string  $value
 * @return string
 */

class Money {

	public static function real($value,$mod=false)
	{
		//setlocale(LC_MONETARY, 'pt_BR');
		//money_format($value,'%n');
		$real = '';
		if($mod){
			$real .= 'R$';
		}
		$real .= number_format($value, 2, ",", ".");
		return $real;
	}
}
