<?php
/**
 * Dash lib
 *
 * @category Admin
 * @package  Dash
 * @author   Ramom Barros  <contato@ramon-barros.com>
 * @license  MIT License <http://www.opensource.org/licenses/mit>
 * @link     http://link.com
 */
namespace Dash;

 /**
 * Upload Class
 *
 * @category CMS
 * @package  Dash
 * @author   Ramom Barros  <contato@ramon-barros.com>
 * @license  MIT License <http://www.opensource.org/licenses/mit>
 * @link     http://link.com
 */
class Upload
{
    /**
    * Save file
    * 
    * @param string $field 'image' fild to upload
    * @param string $path  path to file save
    * @param string $name  file name
    * 
    * @return string fila name
    */
    public static function save($field = 'image', $path = 'public/uploads', $name = false)
    {
        /**
         * Recupera $_FILE[$field] 
         * @var array
         */
        $input = \Input::file($field);
        /**
         * Verifica se existe a imagem
         */
        if ( isset($input['tmp_name']) and strlen($input['tmp_name'])>0) {
            /**
             * Recupera a pasta de upload do arquivo de configuração
             * [bundles/zircon/config/settings.php]
             * @var string
             */
            $path = path('public') . \Config::get('zircon::settings.upload_path');
            /**
             * Verifica se o diretório existe, caso contrário cria
             */
            if ( static::checkPath($path) ) {
                
                /**
                 * Recupera a extensão do arquivo
                 * @var string
                 */
                $ext = \File::extension($input['name']);
                /**
                 * Gera um nome aleatório para o arquivo
                 * @var string
                 */
                $filename = md5_file($input['tmp_name']) . '.' . $ext;

                /**
                 * Verifica se o arquivo atual é igual ao anterior
                 */
                if (\Input::get($field) != $filename) {
                    /**
                     * Se for diferente, exclui o arquivo anterior
                     */
                    \File::delete($path . '/' . \Input::get($field));
                    /**
                     * Efetua o upload do arquivo
                     */
                    if ( \Input::upload($field, $path, $filename) ) {
                        return $filename;
                    }
                } else { // Caso for igual retorna o nome
                    return \Input::get($field);
                }
                
            } else {
                return false;
            }
        } else { // Caso não exista retorna a antiga
            return \Input::get($field);
        }
        
        
    }


    /**
    * Verifica se o caminho existe, senão o cria
    * 
    * @param string $path Path to upload
    * 
    * @return boolean Path
    */
    public static function checkPath($path)
    {     
        if ( is_dir($path) ) {
            return true;
        } else {
            @chmod(path('public'), 0777);
            if ( mkdir($path) ) {
                return true;
            } else {
                return false;  
            }
            @chmod(path('public'), 0755);
        }
    }
   

}
