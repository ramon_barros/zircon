<?php namespace Dash;

/**
 * Converte o formato de data do banco de dados para dd/mm/yyyy
 *
 * @param  string  $value
 * @return string
 */

class Date {

	public static function db2view($date=null)
	{
    if(is_null($date)){
        throw new Exception("Você deve informar uma data Ex:0000-00-00");
      }
		$date = vsprintf(
              '%1$02d/%2$02d/%3$04d %4$02d:%5$02d:%6$02d' , sscanf(
                      $date,
                      '%3$d-%2$d-%1$d %4$d:%5$d:%6$d'
              )
            );
    	return str_replace(' 00:00:00', '', $date);
	}
  public static function view2db($date=null)
  {
      if(is_null($date)){
        throw new Exception("Você deve informar uma data Ex:00-00-0000");
      }
      $date = vsprintf(
                '%3$04d-%2$02d-%1$02d %4$02d:%5$02d:%6$02d' , sscanf(
                        $value,
                        '%1$d/%2$d/%3$d %4$d:%5$d:%6$d'
                )
              );
      return str_replace(' 00:00:00', '', $date);
  }
  public static function convert($date=null,$to='%3$04d-%2$02d-%1$02d %4$02d:%5$02d:%6$02d',$for='%1$d/%2$d/%3$d %4$d:%5$d:%6$d'){
    if(is_null($date)){
      throw new Exception("Você deve informar uma data");
    }
    $date = vsprintf( $for , sscanf($date,$to) );
    return str_replace(' 00:00:00', '', $date);
  }
}
