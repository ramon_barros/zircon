<?php namespace Core\Model;

///////////////////
// Class Laravel //
///////////////////
use \Laravel\Auth;
use \Laravel\Auth\Drivers\Driver;
use \Laravel\Asset;
use \Laravel\Autoloader;
use \Laravel\Blade;
use \Laravel\Bundle;
use \Laravel\Routing\Controller;
use \Laravel\Cache;
//use \Laravel\Config;
use \Laravel\Cookie;
use \Laravel\Crypter;
use \Laravel\Database;
use \Laravel\Database as DB;
use \Laravel\Database\Eloquent\Model;
use \Laravel\Event;
use \Laravel\File;
use \Laravel\Routing\Filter;
use \Laravel\Form;
use \Laravel\Hash;
use \Laravel\HTML;
use \Laravel\Input;
use \Laravel\IoC;
use \Laravel\Lang;
use \Laravel\Log;
use \Laravel\Memcached;
use \Laravel\Profiling\Profiler;
use \Laravel\URL;
use \Laravel\Redirect;
use \Laravel\Redis;
use \Laravel\Request;
use \Laravel\Response;
use \Laravel\Routing\Route;
use \Laravel\Routing\Router;
use \Laravel\Database\Schema;
use \Laravel\Section;
use \Laravel\Session;
use \Laravel\Str;
use \Laravel\CLI\Tasks\Task;
use \Laravel\CLI\Command;
use \Laravel\URI;
use \Laravel\Validator;
use \Laravel\View;
//use \Laravel\Database\Eloquent\Model as Eloquent;

/////////////////////
// Class/Model CMS //
/////////////////////
use \stdClass;
use \Zircon_Config;
use \Zircon_Model_Users;
use \Zircon_Model_Modulo;
use \CommentParser;
use \Dash\Messages as Messages;
use \ConfigCms;
use \Sentry;
use \FilesystemIterator;
use \Reflection;
use \ReflectionClass;
use \ReflectionExtension;
use \ReflectionFunction;
use \ReflectionFunctionAbstract;
use \ReflectionMethod;
use \ReflectionObject;
use \ReflectionParameter;
use \ReflectionProperty;

/**
 * Model para criação dos modulos.
 */
class ModuloInstall
{
    /**
     * Verifica se o modulo esta instalado
     * @param  integer $id id do modulo correspondente
     * @return [type]     [description]
     */
    public static function get_modulo_install($id){
        return DB::table('modulos')->where('id', '=', $id)->only('instalado');
    }

     /**
     * Retorna modulos instalados
     * @return object
     */
    public static function modulos_installed(){
        return DB::table('modulos')->where('instalado', '=', 1)->get();
    }
}