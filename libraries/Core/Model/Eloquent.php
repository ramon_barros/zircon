<?php namespace Core\Model;

///////////////////
// Class Laravel //
///////////////////
use \Laravel\Auth;
use \Laravel\Auth\Drivers\Driver;
use \Laravel\Asset;
use \Laravel\Autoloader;
use \Laravel\Blade;
use \Laravel\Bundle;
use \Laravel\Routing\Controller;
use \Laravel\Cache;
//use \Laravel\Config;
use \Laravel\Cookie;
use \Laravel\Crypter;
use \Laravel\Database;
use \Laravel\Database as DB;
use \Laravel\Database\Eloquent\Model;
use \Laravel\Event;
use \Laravel\File;
use \Laravel\Routing\Filter;
use \Laravel\Form;
use \Laravel\Hash;
use \Laravel\HTML;
use \Laravel\Input;
use \Laravel\IoC;
use \Laravel\Lang;
use \Laravel\Log;
use \Laravel\Memcached;
use \Laravel\Profiling\Profiler;
use \Laravel\URL;
use \Laravel\Redirect;
use \Laravel\Redis;
use \Laravel\Request;
use \Laravel\Response;
use \Laravel\Routing\Route;
use \Laravel\Routing\Router;
use \Laravel\Database\Schema;
use \Laravel\Section;
use \Laravel\Session;
use \Laravel\Str;
use \Laravel\CLI\Tasks\Task;
use \Laravel\URI;
use \Laravel\Validator;
use \Laravel\View;

/////////////////////
// Class/Model CMS //
/////////////////////
use \stdClass;
use \Sentry;
use \Zircon_Config;
use \Zircon_Model_Users;
use \Zircon_Model_Modulo;
use \ConfigCms;
use \FilesystemIterator;

interface IntefaceModulos {
    public static function key();
    public static function up();
    public static function down();
    public static function permissions();
    public static function columns();
    public static function paginate();
    public static function rules();
    public static function search();
}

abstract class Eloquent extends \Laravel\Database\Eloquent\Model implements IntefaceModulos {

    public static $timestamps = false;
    public static $key = 'id';
    public static $menu = true;
    public static $search = false;
    public static $referrer = false;

    /**
     * Retorna primary key
     * @return integer 
     */
    public static function key(){
        return static::$key;
    }

    /**
     * Permissões do modulo
     * @return array retorna as permissões do modulo
     */
    public static function permissions(){
        $permissions = array(
             static::$modulo."_edit"   => array(1,'Editar') 
            ,static::$modulo."_add"    => array(1,'Adicionar')
            ,static::$modulo."_delete" => array(1,'Deletar')
        );
        
        return  $permissions;
    }
}