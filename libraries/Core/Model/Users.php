<?php namespace Core\Model;

///////////////////
// Class Laravel //
///////////////////
use \Laravel\Auth;
use \Laravel\Auth\Drivers\Driver;
use \Laravel\Asset;
use \Laravel\Autoloader;
use \Laravel\Blade;
use \Laravel\Bundle;
use \Laravel\Routing\Controller;
use \Laravel\Cache;
use \Laravel\Config;
use \Laravel\Cookie;
use \Laravel\Crypter;
use \Laravel\Database;
use \Laravel\Database as DB;
use \Laravel\Database\Eloquent\Model;
use \Laravel\Event;
use \Laravel\File;
use \Laravel\Routing\Filter;
use \Laravel\Form;
use \Laravel\Hash;
use \Laravel\HTML;
use \Laravel\Input;
use \Laravel\IoC;
use \Laravel\Lang;
use \Laravel\Log;
use \Laravel\Memcached;
use \Laravel\Profiling\Profiler;
use \Laravel\URL;
use \Laravel\Redirect;
use \Laravel\Redis;
use \Laravel\Request;
use \Laravel\Response;
use \Laravel\Routing\Route;
use \Laravel\Routing\Router;
use \Laravel\Database\Schema;
use \Laravel\Section;
use \Laravel\Session;
use \Laravel\Str;
use \Laravel\CLI\Tasks\Task;
use \Laravel\URI;
use \Laravel\Validator;
use \Laravel\View;
//use \Laravel\Database\Eloquent\Model as Eloquent;

/////////////////////
// Class/Model CMS //
/////////////////////
use \stdClass;
use \ConfigCms;
use \Sentry;
use \Zircon_Model_Users;
use \FilesystemIterator;
/**
 * Users
 *
 */
class Users extends \Laravel\Database\Eloquent\Model
{

    /**
     * special fields to form
     *
     * @var string
     */
    public static $type = '';
    public static $table = 'users';
    public static $modulo = 'users';
    /**
     * Subtitulo e submenus
     * @var array
     */
    public static $admin = array(
                            'subtitle' =>'Usuários'
                           );
    /**
     * Indicates if the model has update and creation timestamps.
     *
     * @var bool
     */
    public static $timestamps = true;


    public $user;
    public function __construct()
    {
        $this->user = Sentry::user($this->id);
    }

    /**
     * Permissões do modulo
     * @return array retorna as permissões do modulo
     */
    public static function permissions(){
        $permissions = array(
            'superuser' => array(
                 static::$modulo."_edit"   => array(1,'Editar') 
                ,static::$modulo."_add"    => array(1,'Adicionar')
                ,static::$modulo."_delete" => array(1,'Deletar')
            ),
            'is_admin' => array(
                 static::$modulo."_edit"   => array(1,'Editar') 
                ,static::$modulo."_add"    => array(1,'Adicionar')
                ,static::$modulo."_delete" => array(1,'Deletar')
            ),
            'user' => array(
                 static::$modulo."_edit"   => array(0,'Editar') 
                ,static::$modulo."_add"    => array(0,'Adicionar')
                ,static::$modulo."_delete" => array(0,'Deletar')
            ),
        );
        
        return  $permissions;
    }

    /**
     * fields to be used in the form
     *
     * @return array
     */
    public static function columns()
    {
        return array(

            'id' => array(
                'input' => function($val = '') {
                    return Form::hidden('id' , $val);
                },

            ),

            'first_name' => array(
                'first_name' => 'Nome',
                'label' => function() {
                    return Form::label('first_name', 'Nome', array('class' => 'large-label'));
                },
                'input' => function($val = '') {
                    return Form::text('first_name', $val, array('class'=>''));
                },
            ),

            'last_name' => array(
                'last_name' => 'Sobrenome',
                'label' => function() {
                    return Form::label('last_name', 'Sobrenome', array('class' => 'large-label'));
                },
                'input' => function($val = '') {
                    return Form::text('last_name', $val, array('class'=>''));
                },
            ),

            'email' => array(
                'email' => 'E-mail',
                'label' => function() {
                    return Form::label('email', 'E-mail', array('class' => 'large-label'));
                },
                'input' => function($val = '') {
                    return Form::email('email', $val, array('class'=>''));
                },
            ),

            'edit_password' => array(
                'edit_password' => 'Editar senha',
                'label' => function() {
                    return Form::label('edit_password','Editar senha',array('class'=>'large-label'));
                },
                'input' => function(){
                    return Form::checkbox('edit_password',true);
                }
            ),

            'password' => array(
                'password' => 'Senha',
                'label' => function() {
                    return Form::label('password', 'Senha', array( 'class' => 'large-label'));
                },
                'input' => function($val = '') {
                    return Form::password('password', array('class'=>'password'));
                },
            ),

            'status' => array(
                'name' => 'Status',
                'label' =>  function() {
                    return Form::label('status', 'Status', array( 'class' => 'large-label'));
                },
                'input' => function($val = '') {
                     $status = array();
                     // foreach (Grupo::all() as $value) {
                     //     $grupos[$value->id] = $value->nome;
                     // }
                     $status[1] = 'Ativo';
                     $status[0] = 'Inativo';

                    return Form::select('status', $status, $val);
                },
            ),

            'grupo' => array(
                'name' => 'Grupo',
                'label' =>  function() {
                    return Form::label('grupo', 'Grupo', array( 'class' => 'large-label'));
                },
                'input' => function($val = '') {
                     $grupos = array();
                     foreach (Sentry::group()->all() as $group) {
                        if($group['name']!='Ezoom'){
                            $grupos[$group['id']] = $group['name'];
                        }
                     }
                    return Form::select('grupo', $grupos, $val);
                },
            ),

        );
    }

    public static function paginate()
    {
        $items = array(
            'users.id',
            'users_metadata.first_name as nome',
            'users_metadata.last_name as sobrenome',
            'users.email',
            //'users.status as status',
        );

        return DB::table('users')
            ->left_join('users_metadata', 'users.id', '=', 'users_metadata.user_id')
            ->left_join('users_groups','users_groups.user_id','=','users.id')
            ->where('users_groups.group_id','<>','1')
            ->paginate(Config::get('zircon::settings.per_page'), $items);
    }

    public static function buttons(){
        $buttons = new StdClass;
        $buttons->permissions = '<a class="btn" href="'.URL::home().'zircon/users/permissions/:keyValue"><i class="icon-lock"></i></a>';
        $buttons->edit   = '<a class="btn" href="'.URL::home().'zircon/users/edit/:keyValue"><i class="icon-edit"></i></a>';
        $buttons->delete = '<a href="#modal" role="button" class="btn delete" data-key=":keyName" data-value=":keyValue" data-toggle="modal"><i class="icon-remove"></i></a>';
        return $buttons;
    }

    public function get_first_name()
    {
        return $this->user->get('metadata.first_name');
    }
    public function set_first_name($data)
    {
        return $user->update(array(
            'metadata' => array(
                'first_name' => $data
            )
        ));
    }

    public function get_last_name()
    {
        return $this->user->get('metadata.last_name');
    }

    public static function get_user($id)
    {   
        $user = Sentry::user((int)$id)->get();
        $userData = new StdClass;
        $userData->id           = $user['id'];
        $userData->first_name   = $user['metadata']['first_name'];
        $userData->last_name    = $user['metadata']['last_name'];
        $userData->email        = $user['email'];
        $userData->status       = $user['status'];
        $userData->grupo       = Zircon_Model_Users::get_user_group($id);
        
        return  $userData;
    }

    public static function get_user_permissions($id){
        /**
         * Recupera as informações do usuário
         * @var array
         */
        $user = Sentry::user((int)$id)->get();
        /**
         * Caso as permissões individuais do usuário não for null retorna
         */
        if(strlen($user['permissions'])>0){
            return $user['permissions'];
        }else{ // Caso contrário retorna as permissões do grupo
            return Sentry::group(Zircon_Model_Users::get_user_group($id))->permissions();
        }
    }

    public static function get_user_group($id){
        $groups = Sentry::user((int)$id)->groups();;
        $grupo = array();
        foreach ($groups as $group) {
            return $group['id'];
        }
    }

    /**
     * validation rules of the form
     *
     * @return array
     */
    public static function rules()
    {
        $uri = URI::segment(3);
        if($uri=='add'){
            return array(
                'first_name' => 'required',
                'last_name'  => 'required',
                'email'      => 'required|email|unique:users',
                'password'   => 'required_with:edit_password',
                'status'     => 'required',
                'grupo'      => 'required'
            );
        }else{
            return array(
                'first_name' => 'required',
                'last_name'  => 'required',
                'email'      => 'required|email',
                'password'   => 'required_with:edit_password',
                'status'     => 'required',
                'grupo'      => 'required'
            );
        }
    }
}