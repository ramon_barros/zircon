<?php namespace Core\Model;

///////////////////
// Class Laravel //
///////////////////
use \Laravel\Auth;
use \Laravel\Auth\Drivers\Driver;
use \Laravel\Asset;
use \Laravel\Autoloader;
use \Laravel\Blade;
use \Laravel\Bundle;
use \Laravel\Routing\Controller;
use \Laravel\Cache;
//use \Laravel\Config;
use \Laravel\Cookie;
use \Laravel\Crypter;
use \Laravel\Database;
use \Laravel\Database as DB;
use \Laravel\Database\Eloquent\Model;
use \Laravel\Event;
use \Laravel\File;
use \Laravel\Routing\Filter;
use \Laravel\Form;
use \Laravel\Hash;
use \Laravel\HTML;
use \Laravel\Input;
use \Laravel\IoC;
use \Laravel\Lang;
use \Laravel\Log;
use \Laravel\Memcached;
use \Laravel\Profiling\Profiler;
use \Laravel\URL;
use \Laravel\Redirect;
use \Laravel\Redis;
use \Laravel\Request;
use \Laravel\Response;
use \Laravel\Routing\Route;
use \Laravel\Routing\Router;
use \Laravel\Database\Schema;
use \Laravel\Section;
use \Laravel\Session;
use \Laravel\Str;
use \Laravel\CLI\Tasks\Task;
use \Laravel\CLI\Command;
use \Laravel\URI;
use \Laravel\Validator;
use \Laravel\View;
//use \Laravel\Database\Eloquent\Model as Eloquent;

/////////////////////
// Class/Model CMS //
/////////////////////
use \stdClass;
use \Zircon_Base_Controller;
use \Zircon_Config;
use \Zircon_Model_Users;
use \Zircon_Model_Modulo;
use \Zircon_Modulo_Install;
use \CommentParser;
use \Dash\Messages as Messages;
use \ConfigCms;
use \Sentry;
use \FilesystemIterator;
use \Reflection;
use \ReflectionClass;
use \ReflectionExtension;
use \ReflectionFunction;
use \ReflectionFunctionAbstract;
use \ReflectionMethod;
use \ReflectionObject;
use \ReflectionParameter;
use \ReflectionProperty;

/**
 * Model para criação dos modulos.
 */
class Modulo extends \Laravel\Database\Eloquent\Model
{

    /**
     * special fields to form
     *
     * @var string
     */
    public static $type = '';
    public static $table = 'modulos';
    public static $modulo = 'modulos';
    /**
     * Subtitulo e submenus
     * @var array
     */
    public static $admin = array(
                            'subtitle' =>'Modulos'
                           );
    /**
     * Indicates if the model has update and creation timestamps.
     *
     * @var bool
     */
    public static $timestamps = true;
  
    /**
     * Permissões do modulo
     * @return array retorna as permissões do modulo
     */
    public static function permissions(){
        $permissions = array(
            'superuser' => array(
                 static::$modulo."_edit"   => array(1,'Editar') 
                ,static::$modulo."_add"    => array(1,'Adicionar')
                ,static::$modulo."_delete" => array(1,'Deletar')
            ),
            'is_admin' => array(
                 static::$modulo."_edit"   => array(1,'Editar') 
                ,static::$modulo."_add"    => array(1,'Adicionar')
                ,static::$modulo."_delete" => array(1,'Deletar')
            ),
            'user' => array(
                 static::$modulo."_edit"   => array(0,'Editar') 
                ,static::$modulo."_add"    => array(0,'Adicionar')
                ,static::$modulo."_delete" => array(0,'Deletar')
            ),
        );
        
        return  $permissions;
    }

    /**
     * fields to be used in the form
     *
     * @return array
     */
    public static function columns()
    {
        return array(
            'id' => array(
                'input' => function($val = '') {
                    return Form::hidden('id' , $val);
                },

            ),
             'nome' => array(
                'name' => 'Nome',
                 'label' =>  function() {
                    return Form::label('nome', 'Nome', array( 'class' => 'large-label'));
                },
                'input' => function($val = '') {

                    return Form::text('nome', $val, array('class'=>'x-large'));
                },
            ),
        );
    }

    public static function paginate()
    {
        $items = array(
            'id',
            'nome',
            'instalado'
        );
        return DB::table('modulos')->paginate(Zircon_Config::get('zircon::settings.per_page'), $items);
    }


    /**
     * validation rules of the form
     *
     * @return array
     */
    public static function rules()
    {
        return array(
            'nome' => 'required'
        );
    }

    /**
     * Cria as mensagem de erro para o desenvolvedor
     * @param  array  $call       
     * @param  string $controller controller acessado
     * @param  string $method     método acessado
     * @return array             retorna mensagens de erro.
     */
    public static function check_modulos_call($call=array(),$controller,$method){
        if(is_array($call)){
            if(!isset($call['title'])){
                $messages = 'Algum problema no titulo controller ('.$controller.') no método '.$method.'()';
            }
            if(!isset($call['subtitle'])){
                $call['subtitle'] = 'Você deve informar um subtitulo no controller ('.$controller.') no método '.$method.'()';
            }
            if(!isset($call['content'])){
                $call['content'] = 'Você deve informar uma view no controller ('.$controller.') no método '.$method.'()';
            }
        }else{
            $call['error'] = 'Error';
        }        
        return $call;          
    }

    /**
     * Verifica as permissões do usuário correspoendente ao modulo
     * caso o usuário não tiver permissões é verificado o grupo
     * @param  string $modulo nome do modulo
     * @param  string $param  add, edit e delete
     * @return boolean  true ou false
     */
    public static function modulo_user_permissions($modulo,$param){
        $permission = "{$modulo}_{$param}";

        /**
         * Superuser tem permissão total
         */
        if(Sentry::user()->has_access('superuser')){
            if(Zircon_Model_Modulo::check_permisson_buttons($modulo,'superuser',$permission)==false){
                return false;
            }
            return true;
        }

        /**
         * Administrador não tem permissão total, verifica cada permissão _add, _edit, _delete
         */
        if(Sentry::user()->has_access('is_admin') and Sentry::user()->has_access($permission)){
            if(Zircon_Model_Modulo::check_permisson_buttons($modulo,'is_admin',$permission)==false){
                return false;
            }
            return true;
        }

        /**
         * Administrador não tem permissão total, verifica cada permissão _add, _edit, _delete
         */
        if(Sentry::user()->has_access('user') and Sentry::user()->has_access($permission)){
            if(Zircon_Model_Modulo::check_permisson_buttons($modulo,'user',$permission)==false){
                return false;
            }
            return true;
        }            
        return false;
    }

    /**
     * Retorna os botões de adicionar, editar e deletar
     * @param  string $modulo nome do modulo
     * @return object         retorna os botões disponiveis
     */
    public static function modulo_buttons($modulo){
        $url_cms = URI::segment(1);
        $buttons = new StdClass;
        if(Zircon_Model_Modulo::modulo_user_permissions($modulo,'edit')){
            if(isset($modulo::$button_edit)){
                $buttons->edit = $modulo::$button_edit;
            }else{
                $buttons->edit = '<a class="btn" href=":url_edit/:keyValue"><i class="icon-edit"></i></a>';
            }
        }
        if(Zircon_Model_Modulo::modulo_user_permissions($modulo,'delete')){ 
            if(isset($modulo::$button_delete)){
                $buttons->delete = $modulo::$button_delete;
            }else{
                $buttons->delete = '<a class="btn delete" href="#modal" role="button" data-key=":keyName" data-value=":keyValue" data-toggle="modal"><i class="icon-remove"></i></a>';
            }
        }
        if(Zircon_Model_Modulo::modulo_user_permissions($modulo,'fotos')){
            if(isset($modulo::$button_fotos)){
                $buttons->fotos = $modulo::$button_fotos;
            }else{
                $buttons->fotos = '<a class="btn" href="'.URL::home().$url_cms.'/modulos/fotos/index/:url_controller/:keyValue"><i class="icon-picture"></i></a>';
            }
        }

        return $buttons;
    }

    /**
     * Verifica se existe permissão referente no método permissons do modulo
     * @param  string $modulo     model do modulo
     * @param  string $group      grupo de usuário (superuser,is_admin,user)
     * @param  string $permission permissão (produtos_edit,categorias_delete,produtos_categorias_fotos)
     * @return boolean            retorna true se existe e false se não existe.
     */
    public static function check_permisson_buttons($modulo,$group,$permission){
        $permissions_modulo = $modulo::permissions();
        if(isset($permissions_modulo) and isset($permissions_modulo[$group]) and isset($permissions_modulo[$group][$permission]) ){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Verifica permissões dos campos do formulários
     * @param  string $model model do modulo acessado
     * @return object
     */
    public static function columns_permissions($model){
        if(method_exists($model,'columns') and count($model::columns())>0){
            $columns = $model::columns();
        }else{
            $columns = array();
        }
        if(count($columns)>0 and method_exists($model,'columns_permissions') and count($model::columns_permissions())>0){
            $columns_permissions = $model::columns_permissions();
            $permissions = array();
            $groups = array('superuser','is_admin','user');
            foreach ($columns_permissions as $rule=>$value) {
                if(in_array($rule, $groups)){
                    foreach ($value as $k=>$v) {
                        $permissions[$v] = 0;
                    }
                }else{
                   $permissions[$value] = 0;
                }            
            }
        }
        return $columns;
    }

    /**
     * Retorna os campos do formulário para Dash\Form
     * @param  string $model model do modulo acessado
     * @return object
     */
    public static function columns_add($model){
        $obj =  Zircon_Model_Modulo::columns_permissions($model);
        $data = new StdClass();
        foreach ($obj as $key => $val) {
            $data->$key = false;
        }
        return $data;
    }

    /**
     * Recupera as permissões dos modulos instalados
     * @return array retorna um array contendo as permissões dos modulos.
     */
    public static function modulos_permissions()
    {
        $permissions = array();
        $modulos = Zircon_Modulo_Install::modulos_installed();
        foreach ($modulos as $key => $modulo) {
            $model = $modulo->nome;
            $permissions[$model] = $model::permissions();
        }
        return $permissions;
    }

    /**
     * Remove as permissões para cada grupo
     * @param  array  $modulo_permissions permissões do modulo
     * @return array permissões para cada grupo
     */
    private static function modulo_permissions_uninstall($modulo_permissions){
        $permissions = array();
        $groups = array('superuser','is_admin','user');
        foreach ($modulo_permissions as $rule=>$value) {
            if(in_array($rule, $groups)){
                foreach ($value as $permission => $value) {
                    $affected = DB::table('rules')->where('rule', '=', $permission)->delete();
                    $permissions[$permission]=$value[0];
                }
            }else{
                $affected = DB::table('rules')->where('rule', '=', $rule)->delete();
                $permissions[$rule]=$value[0];
            }
        }
        return $permissions;
    }

    /**
     * Retorna os dashboard de cada modulo na home do CMS
     * @return html
     */
    public static function modulos_dashboard(){
        $modulos = Zircon_Modulo_Install::modulos_installed();
        $content=array();
        if(count($modulos)>0){
            foreach ($modulos as $modulo) {
                $model = $modulo->nome;
                $controller = strtolower($model);
                $response = Controller::call("zircon::modulos.{$controller}@dashboard");
                if($response->status()==200){
                    $content[] = $response->content;
                }
            }
        }
        return implode('',$content);
    }

    /**
     * Instala as permissões conforme passado no modulo
     * @param  array $modulo_permissions 
     * @return array
     */
    private static function modulo_permissions_install($modulo_permissions=array()){
        $permissions = array();
        $groups = array('superuser','is_admin','user');
        foreach ($modulo_permissions as $rule=>$value) {
            if(in_array($rule, $groups)){
                $permissions[$rule] = Zircon_Model_Modulo::permissions_groups($value);
            }else{
                $permissions[$rule] = Zircon_Model_Modulo::permissions_all_groups($rule,$value);
            }            
        }
        return $permissions;
    }

    /**
     * Cria/Atualiza as permissões de vários grupos
     * @param  array $group_permissions
     * @return array 
     */
    private static function permissions_groups($group_permissions=array()){
        $permissions = array();
        foreach ($group_permissions as $rule=>$value) {
            $permissions[$rule] = Zircon_Model_Modulo::permissions_all_groups($rule,$value);
        }
        return $permissions;
    }

    /**
     * Cria/Atualiza as permissões na tabela rules
     * @param  string $rule  permissão
     * @param  array  $value valor e descrição
     * @return integer retorna o valor da permissão 1=habilitado 0=desabilitado
     */
    private static function permissions_all_groups($rule,$value){
        $permissions = array();
        if(is_array($value)){
            $valor = $value[0];
            $description = $value[1];
        }else{
            $valor = $value;
            $description = preg_replace('/[_]/',' ' , $rule);
        }
        $select = DB::table('rules')->where('rule', '=', $rule)->first();
        if ($select) {
            $affected = DB::table('rules')
                            ->where('rule', '=', $rule)
                            ->update( array (
                                   'rule' => $rule,
                                   'description' => $description
                                ) 
                            );  
        }else{
            $affected = DB::table('rules')->insert(array(
                            'rule' => $rule,
                            'description' => $description
                        ));
        }
        return $valor;
    }

    /**
     * Verifica se a table existe
     * @param  string $table nome da tabela do modulo
     * @return boolean       retorna true ou false
     */
    public static function table_modulo_exists($table){
        return Schema::exists($table);
    }   

    public static function modulo_install($id){
        /**
         * Retorna o nome do modulo
         * @var string
         */
        $modulo = DB::table('modulos')->where('id', '=', $id)->only('nome');
        Log::write('info', "{$modulo} - Instalação do modulo iniciada.");

        /**
         * Caminho onde será criado o controller
         * @var string
         */
        $controller_file_path = Bundle::path('zircon') . strtolower("controllers/modulos/{$modulo}".EXT);

        /**
         * Verifica se o arquivo do modulo existe
         */
        if ( !File::exists($controller_file_path) ) {
            /**
             * Primeira letra do nome do modulo maiuscula
             * @var string
             */
            $controller = ucfirst($modulo);
            $artisan=array('zircon::generate:controller');
            array_push($artisan, $controller);
            
            \Laravel\CLI\Command::run($artisan);
        }

        /**
         * Caminho do arquivo onde esta o modulo
         * @var string
         */
        $file_path = Bundle::path('zircon') . strtolower("models/modulos/{$modulo}".EXT);

        /**
         * Verifica se o arquivo do modulo existe
         */
        if ( File::exists($file_path) ) {
            
            /**
             * Primeira letra do nome do modulo maiuscula
             * @var string
             */
            $class = ucfirst($modulo);
            
            $reflection = new ReflectionClass($class);
            $parent = $reflection->getParentClass();
            /**
             * Verifica se o model esta extendendo a classe Eloquent
             */
            if($parent and $parent->getName()=='Core\Model\Eloquent'){

                /**
                 * Verifica se já existe a tabela do modulo
                 */
                if(self::table_modulo_exists(strtolower($modulo))===false)
                {
                    $install = $class::up();
                    $comment = CommentParser::parse($reflection->getMethod('up')->getDocComment());
                    foreach ($comment as $key => $relation) {
                        if($key=='relation'){
                            foreach ($relation as $mod_relation) {
                                $class_relation = ucfirst($mod_relation);
                                $reflection_relation = new ReflectionClass($class_relation);
                                $parent_relation = $reflection_relation->getParentClass();
                                
                                /**
                                 * Cria as tabelas principais da relação antes de criar as foreign keys
                                 */
                                if($parent_relation and $parent_relation->getName()=='Core\Model\Eloquent'){
                                    //$status = self::create_table_modules( $class_relation::up() );
                                }

                                /**
                                 * Cria as tabelas de relação automaticamente com as foreign keys
                                 */
                                $table_relation = 'create_'.$class::$table.'_';
                                $table_relation .= $class_relation::$table;
                                if( !in_array($table_relation, array_keys($install)) ){
                                  
                                  $r = array($table_relation => array(
                                       "id:integer"
                                      ,$class_relation::$table."_".$class_relation::key().":integer:unsigned"
                                      ,$class::$table."_".$class::key().":integer:unsigned"
                                      ,$class_relation::$table."_".$class_relation::key().":foreign:references['".$class_relation::key()."']:on['".$class_relation::$table."']:on_delete['restrict']"
                                      ,$class::$table."_".$class::key().":foreign:references['".$class::key()."']:on['".$class::$table."']:on_delete['restrict']"
                                    )
                                  );
                                  $install = array_merge($install,$r);
                                }
                            }
                        }
                    }
                    $create_tables = self::create_table_modules( $install );
                }

                if(self::create_modules_permission($class)){
                    Messages::add('sucesso', "{$modulo} - Permissões do modulo criadas.");
                }

                Log::write('info', "{$modulo} - Instalado com sucesso.");
                return DB::table('modulos')->where('id', '=', $id)->update(array('instalado' => '1'));
            }else{
                $interfaces=array();
                $reflection = new ReflectionClass('Eloquent');
                foreach ($reflection->getInterfaceNames() as $InterfaceNames) {
                    $interface = new ReflectionClass($InterfaceNames);
                    foreach ($interface->getMethods() as $InterfaceMethod) {
                        $interfaces[]=$InterfaceMethod->getName();
                    }
                }
                Messages::add('error', "O modulo deve extender a classe Eloquent e possuir os métodos:".print_r($interfaces,true));
                Log::write('error', "O modulo deve extender a classe Eloquent e possuir os métodos:".print_r($interfaces,true));
            }
        }else{
            Messages::add('error', "O arquivo [{$file_path}] não existe!");
            Log::write('info', "{$modulo} - O arquivo [{$file_path}] não existe!");

            /**
             * Nova versão será criado os arquivo automaticamente
             */
            //Messages::add('info','Criando arquivos necessários.');
            
            return false;
        }        
    }

    private static function create_modules_permission($modulo=null){
        Log::write('info', "{$modulo} - Criando permissões do modulo.");
        /**
         * Retora as permissões do modulo a ser instalado
         * @var array
         */
        $modulo_permissions = $modulo::permissions();
        
        /**
         * Verifica cada permissão no banco de dados
         */
        $modulo_permissions = Zircon_Model_Modulo::modulo_permissions_install($modulo_permissions);

        /**
         * Retorna todos os grupos cadastrados
         * @var array
         */
        $groups = Sentry::group()->all();
        foreach ($groups as $group) {
            /**
             * Retorna as permissões de um grupo
             * @var array
             */
            $group_permissions = (array)json_decode(Sentry::group($group['name'])->permissions());
            var_dump($group_permissions);
            
            foreach ($modulo_permissions as $rule => $value) {
                if(is_array($value) and in_array($rule,array_keys($group_permissions)) ){
                    /**
                     * Junta as permissões já existentes no grupo com as novas do modulo
                     * @var array
                     */
                    $permissions = array_merge($group_permissions,$value);
                }elseif(!is_array($value)){
                     /**
                     * Junta as permissões já existentes no grupo com as novas do modulo
                     * @var array
                     */
                    $permissions = array_merge($group_permissions,$modulo_permissions);
                }
            }

            /**
             * Atualiza as permissões do grupo
             */
            $affected = DB::table('groups')->where('name','=',$group['name'])->update(array('permissions'=>json_encode($permissions)));
        }
        if($affected){
            Log::write('info', "{$modulo} - Permissões criadas com sucesso.");
            return true;
        }else{
            Log::write('error', "{$modulo} - Ocorreu algum erro ao criar as permissões [{$affected}].");
            return false;
        }
    }

    private static function create_table_modules($install){
        if(is_array($install)){
            //$num_tables = count($install);
            foreach ($install as $key => $value) {
                /**
                 * Verifica se a chave corresponde as parametros e se a tabela existe
                 */
                if(preg_match('/(create_|update_|add_|delete_)/', $key)){
                    $pattern = array('/(create_)/','/(update_)/','/(add_)/','/(delete_)/');
                    $table = preg_replace($pattern, '', $key);
                    if( Schema::exists($table) === false){
                        $artisan=array('zircon::generate:migration');
                        array_push($artisan, $key);
                        if(is_array($value)){
                            foreach ($value as $columns) {
                                array_push($artisan, $columns);
                            }
                        }
                        \Laravel\CLI\Command::run($artisan);
                        \Laravel\CLI\Command::run(array('migrate'));
                    }else {
                        return false;
                    }                                
                }
            }
            //Log::write('info', "{$modulo} - Tabelas do modulo criada.");
            return true;
        }else{
            //Messages::add('error', "Você deve retornar um array no método {$class}::up().");
            //Log::write('error', "Você deve retornar um array no método {$class}::up().");
            return false;
        }   
    }

    /**
     * Faz a desinstalação do modulo, removendo a tabela e migrations
     * @param  integer $id 
     * @return boolean
     */
    public static function modulo_uninstall($id=null)
    {
        $modulo = DB::table('modulos')->where('id', '=', $id)->only('nome');
        $file_path = Bundle::path('zircon') . strtolower("models/modulos/{$modulo}".EXT);
        if ( File::exists($file_path) ) {
            
            $class = ucfirst($modulo);
            $reflection = new ReflectionClass($class);

            /**
             * Verifica se já existe a tabela do modulo
             */
            if(self::table_modulo_exists(strtolower($modulo))===true){
                $install = $class::up();
                /**
                 * Adicionado a tabela da relações caso não existir
                 * @var array
                 */
                $comment = CommentParser::parse($reflection->getMethod('up')->getDocComment());
                foreach ($comment as $key => $relation) {
                    if($key=='relation'){
                        foreach ($relation as $mod_relation) {
                            $class_relation = ucfirst($mod_relation);
                            $reflection_relation = new ReflectionClass($class_relation);
                            $parent_relation = $reflection_relation->getParentClass();

                            /**
                             * Cria as tabelas de relação automaticamente com as foreign keys
                             */
                            $table_relation = 'create_'.$class::$table.'_';
                            $table_relation .= $class_relation::$table;
                            if( !in_array($table_relation, array_keys($install)) ){
                              
                              $r = array($table_relation => array(
                                   "id:integer"
                                  ,$class_relation::$table."_".$class_relation::key().":integer:unsigned"
                                  ,$class::$table."_".$class::key().":integer:unsigned"
                                  ,$class_relation::$table."_".$class_relation::key().":foreign:references['".$class_relation::key()."']:on['".$class_relation::$table."']:on_delete['restrict']"
                                  ,$class::$table."_".$class::key().":foreign:references['".$class::key()."']:on['".$class::$table."']:on_delete['restrict']"
                                )
                              );
                              $install = array_merge($install,$r);
                            }
                        }
                    }
                }

                self::drop_table_modules($class::down(),$install);
            }

            if(self::remove_modules_permission($class)){
                Messages::add('sucesso', "{$modulo} - Permissões do modulo removidas.");
            }

            return DB::table('modulos')->where('id', '=', $id)->update(array('instalado' => '0'));
        }else{
            Messages::add('error', 'O arquivo ['.$file_path.'] não existe!');
            return false;
        }        
    }

    private static function remove_modules_permission($modulo=null){
        /**
         * Retora as permissões do modulo a ser instalado
         * @var array
         */
        $modulo_permissions = $modulo::permissions();

        $modulo_permissions = Zircon_Model_Modulo::modulo_permissions_uninstall($modulo_permissions);

        /**
         * Retorna todos os grupos cadastrados
         * @var array
         */
        $groups = Sentry::group()->all();
        foreach ($groups as $group) {
            /**
             * Retorna as permissões de um grupo
             * @var array
             */
            $group_permissions = (array)json_decode(Sentry::group($group['name'])->permissions());

            foreach ($modulo_permissions as $rule => $value) {
                if(is_array($value) and in_array($rule,array_keys($group_permissions)) ){
                    foreach ($value as $key=>$value) {
                        unset($group_permissions[$key]);
                    }
                }else{
                    foreach ($modulo_permissions as $key=>$value) {
                        unset($group_permissions[$key]);
                    }
                }
            }
            
            /**
             * Atualiza as permissões do grupo
             */
            $affected = DB::table('groups')->where('name','=',$group['name'])->update(array('permissions'=>json_encode($group_permissions)));
        }
        if($affected){
            Log::write('info', "{$modulo} - Permissões removidas com sucesso.");
            return true;
        }else{
            Log::write('error', "{$modulo} - Ocorreu algum erro ao remover as permissões [{$affected}].");
            return false;
        }
    }

    public static function drop_table_modules($uninstall,$install){
        if($uninstall){           
            if(is_array($install)){
                $install = array_reverse($install);
                foreach ($install as $table => $value) {
                    $pattern = array('/(create_)/','/(update_)/','/(add_)/','/(delete_)/');
                    $modulotable = preg_replace($pattern, '', $table);
                    
                    /**
                     * Remove a tabela se ela existir
                     */
                    if( Schema::exists($modulotable) ){
                        Schema::table($modulotable, function($table) {
                            $table->drop();
                        });
                    }
                    
                    /**
                     * Remove os arquivos migrations e atualiza a tabela laravel_migrations
                     */
                    $laravel_migrations = DB::table('laravel_migrations')->or_where('name', 'LIKE', '%'.$table.'%')->first();
                    if($laravel_migrations){
                        $file_migrations = Bundle::path('zircon').'migrations'.DS.$laravel_migrations->name.EXT;
                        if(File::exists($file_migrations)){
                            if(File::delete($file_migrations)){
                               $delete_file_migrations = DB::table('laravel_migrations')->where('name','=',$laravel_migrations->name)->delete();
                            }                              
                        }
                    }
                }
            } else {
                Messages::add('error', "Você deve retornar um array no método {$class}::up().");
                Log::write('error', "Você deve retornar um array no método {$class}::up().");
                return false;
            }
        }    
    }

    public static function reload_modulo_permission($id=null) {
        $modulo = DB::table('modulos')->where('id', '=', $id)->only('nome');
        $file_path = Bundle::path('zircon') . strtolower("models/modulos/{$modulo}".EXT);
        if ( File::exists($file_path) ) {
            
            $class = ucfirst($modulo);
            $reflection = new ReflectionClass($class);

            if(self::remove_modules_permission($class)){
                if(self::create_modules_permission($class)){
                    return true;
                }
            }
        }else{
            Messages::add('error', 'O arquivo ['.$file_path.'] não existe!');
            return false;
        }
    }
}