<?php namespace Core\Generator;

/**
 * Laravel Generator
 * 
 * Rapidly create models, views, migrations + schema, assets, tests, etc.
 *
 * USAGE:
 * Add this file to your Laravel application/tasks directory
 * and call the methods with: php artisan generate:[model|controller|migration] [args]
 * 
 * See individual methods for additional usage instructions.
 * 
 * @author      Jeffrey Way <jeffrey@jeffrey-way.com>
 * @license     haha - whatever you want.
 * @version     0.8
 * @since       July 26, 2012
 *
 */

use \InvalidArgumentException as Argument;
use \Laravel\Bundle as Bundle;
use \Laravel\File as File;
use \Laravel\CLI\Command as Command;
use \Laravel\IoC as Ioc;
use \Core\Generator\Template as Template;

class Controller {

    /*
     * The content for the generate file
     */
    public static $content;

	/**
     * Generate a controller file with optional actions.
     *
     * USAGE:
     * 
     * php artisan generate:controller Admin
     * php artisan generate:controller Admin index edit
     * php artisan generate:controller Admin index index:post restful
     * 
     * @param  $args array  
     * @return string
     */
    public function run($args)
    {
        if ( empty($args) ) {
            echo "Error: Please supply a class name, and your desired methods.\n";
            return;
        }

        // Name of the class and file
        $class_name = str_replace('.', '/', ucwords(array_shift($args)));

        // Where will this file be stored?
        $file_path = $this->path("controllers","admin").'modulos'.DS. strtolower("$class_name.php");

        // Admin/panel => Zircon_Panel
        $class_name = $this->prettify_class_name($class_name);

        // Begin building up the file's content
        static::$content = Template::new_class('Zircon_Modulos_'.$class_name . '_Controller', 'Zircon_Base_Controller');

        $content = '';
        // Let's see if they added "restful" anywhere in the args.
        if ( $restful = $this->is_restful($args) ) {

            $args = array_diff($args, array('restful'));
            $content .= 'public $restful = true;';
        }

        // Now we filter through the args, and create the funcs.
        foreach($args as $method) {
            // Were params supplied? Like index:post?
            if ( strpos($method, ':') !== false ) {
                list($method, $verb) = explode(':', $method);
                $content .= Template::func("{$verb}_{$method}");
            } else {
                $action = $restful ? 'get' : 'action';

                $content .= Template::func("{$action}_{$method}");
            }
        }

        // Add methods/actions to class.
        $this->add_after_content('{', $content);

        // Prettify
        $this->prettify();

        // Create the file
        $this->write_to_file($file_path);
    }

    public function path($dir,$bundle=DEFAULT_BUNDLE)
    {
        return Bundle::path($bundle) . $dir . DS;
    }

    /**
     * Crazy sloppy prettify. TODO - Cleanup
     *
     * @param  $content string  
     * @return string
     */
    protected function prettify()
    {
        $content = self::$content;

        $content = str_replace('<?php ', "<?php\n\n", $content);
        $content = str_replace('{}', "\n{\n\n}", $content);
        $content = str_replace('public', "\n\n\tpublic", $content);
        $content = str_replace("() \n{\n\n}", "()\n\t{\n\n\t}", $content);
        $content = str_replace('}}', "}\n\n}", $content);

        // Migration-Specific
        $content = preg_replace("/[ ]{0,}?\/\*\*[ ]{0,}?/", "\n\n\t/**", $content);
        $content = preg_replace("/[ ]{1,}\*/", "\t *", $content);
        $content = preg_replace('/ ?Schema::/', "\n\t\tSchema::", $content);
        $content = preg_replace('/\$table(?!\))/', "\n\t\t\t\$table", $content);
        $content = str_replace('});}', "\n\t\t});\n\t}", $content);
        $content = str_replace(');}', ");\n\t}", $content);
        $content = str_replace("() {", "()\n\t{", $content);

        self::$content = $content;
    }

    /**
     * Prepares the $name of the class
     * Admin/panel => Zircon_Panel
     *
     * @param $class_name string
     */
    protected function prettify_class_name($class_name)
    {
        return preg_replace_callback('/\/([a-zA-Z])/', function($m) {
            return "_" . strtoupper($m[1]);
        }, $class_name);
    }

    protected function is_restful($args)
    {
        $restful_pos = array_search('restful', $args);
        return $restful_pos !== false;
    }

    public function add_after($where, $to_add, $content)
    {
        // return preg_replace('/' . $where . '/', $where . $to_add, $content, 1);
        return str_replace($where, $where . $to_add, $content);
    }

    protected function add_after_content($where, $to_add)
    {
        static::$content = str_replace($where, $where . $to_add, static::$content);

    }
    /**
     * Write the contents to the specified file
     *
     * @param  $file_path string
     * @param $content string
     * @param $type string [model|controller|migration]  
     * @return void
     */
    protected function write_to_file($file_path,  $success = '')
    {
        $success = $success ?: "Create: $file_path.\n";

        if ( File::exists($file_path) ) {
            // we don't want to overwrite it
            echo "Warning: File already exists at $file_path\n";
            return;
        }

        // As a precaution, let's see if we need to make the folder.
        File::mkdir(dirname($file_path));

        if ( File::put($file_path, self::$content) !== false ) {
            echo $success;
        } else {
            echo "Whoops - something...erghh...went wrong!\n";
        }
    }

    public function artisan_run(){
        return Command::run(array('migrate'));
    }

    public function artisan_rollback(){
        return Command::run(array('migrate:rollback'));
    }
}