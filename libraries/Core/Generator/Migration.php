<?php namespace Core\Generator;

/**
 * Laravel Generator
 * 
 * Rapidly create models, views, migrations + schema, assets, tests, etc.
 *
 * USAGE:
 * Add this file to your Laravel application/tasks directory
 * and call the methods with: php artisan generate:[model|controller|migration] [args]
 * 
 * See individual methods for additional usage instructions.
 * 
 * @author      Jeffrey Way <jeffrey@jeffrey-way.com>
 * @license     haha - whatever you want.
 * @version     0.8
 * @since       July 26, 2012
 *
 */

use \InvalidArgumentException as Argument;
use \Laravel\Bundle as Bundle;
use \Laravel\File as File;
use \Laravel\CLI\Command as Command;
use \Laravel\IoC as Ioc;
use \Core\Generator\Template as Template;

class Migration {

    /*
     * The content for the generate file
     */
    public static $content;

	/**
     * Generate a migration file + schema
     *
     * INSTRUCTIONS:
     * - Separate each word with an underscore
     * - Name your migrations according to what you're doing
     * - Try to use the `table` keyword, to hint at the table name: create_users_table
     * - Use the `add`, `create`, `update` and `delete` keywords, according to your needs.
     * - For each field, specify its name and type: id:integer, or body:text
     * - You may also specify additional options, like: age:integer:nullable, or email:string:unique
     *
     *
     * USAGE OPTIONS
     *
     * php artisan generate:migration create_users_table
     * php artisan generate:migration create_users_table id:integer email:string:unique age:integer:nullable
     * php artisan generate:migration add_user_id_to_posts_table user_id:integer
     * php artisan generate:migration delete_active_from_users_table active:boolean
     *
     * @param  $args array  
     * @return string
     */
    public function run($args)
    {
        if ( empty($args) ) {
            echo "Error: Please provide a name for your migration.\n";
            return;
        }

        /**
         * The migrate task is responsible for running database migrations
         * as well as migration rollbacks. We will also create an instance
         * of the migration resolver and database classes, which are used
         * to perform various support functions for the migrator.
         */
        if(! IoC::registered('task: migrate'))
        {
            IoC::register('task: migrate', function()
            {
                $database = new \Laravel\CLI\Tasks\Migrate\Database;
                $resolver = new \Laravel\CLI\Tasks\Migrate\Resolver($database);
                return new \Laravel\CLI\Tasks\Migrate\Migrator($resolver, $database);
            }); 
        }

        foreach ($args as $class => $columns) {
            
            // Determine what the table name should be.
            $table_name = $this->parse_table_name($class);

            // Capitalize where necessary: a_simple_string => A_Simple_String
            $class_name = implode('_', array_map('ucwords', explode('_', $class)));

            // Let's create the path to where the migration will be stored.
            $file_path = $this->path("migrations","admin") . date('Y_m_d_His') . strtolower("_$class_name.php");

            $this->generate_migration($class_name, $table_name, $columns);

            $this->write_to_file($file_path);
        }
    }

    /**
     * Figure out what the name of the table is.
     *
     * Fetch the value that comes right before "_table"
     * Or try to grab the very last word that comes after "_" - create_*users*
     * If all else fails, return a generic "TABLE", to be filled in by the user.
     *
     * @param  $class_name string  
     * @return string
     */
    protected function parse_table_name($class_name)
    {
        $pattern = array('/(create_)/','/(update_)/','/(add_)/','/(delete_)/');
        return preg_replace($pattern, '', $class_name);
    }

    /**
     * Try to determine what type of table action should occur.
     * Add, Create, Delete??
     *
     * @param  $class_name string  
     * @return aray
     */
    protected function parse_action_type($class_name)
    {
         // What type of action? Creating a table? Adding a column? Deleting?
        if ( preg_match('/delete|update|add(?=_)/i', $class_name, $matches) ) {
            $table_action = 'table';
            $table_event = strtolower($matches[0]);
        } else {
            $table_action = $table_event = 'create';
        }

        return array($table_action, $table_event);
    }

    /**
     * Creates the content for the migration file.
     *
     * @param  $class_name string
     * @param  $table_name string
     * @param  $args array
     * @return void
     */
    protected function generate_migration($class_name, $table_name, $args)
    {
        // Figure out what type of event is occuring. Create, Delete, Add?
        list($table_action, $table_event) = $this->parse_action_type($class_name);
        
        // Now, we begin creating the contents of the file.
        static::$content = Template::new_class('Zircon_'.$class_name);

        /* The Migration Up Function */
        $up = $this->migration_up($table_event, $table_action, $table_name, $args);
       
        /* The Migration Down Function */
        $down = $this->migration_down($table_event, $table_action, $table_name, $args);

        // Add both the up and down function to the migration class.
        $this->add_after_content('{', $up . $down);

        return $this->prettify();
    }

    protected function migration_up($table_event, $table_action, $table_name, $args)
    {
        $comment = '* Make changes to the database.
                    *
                    * @return void';
        $up = Template::comment($comment);
        $up .= Template::func('up');

        // Insert a new schema function into the up function.
        $up = $this->add_after('{', Template::schema($table_action, $table_name), $up);

        // Create the field rules for for the schema
        if ( $table_event === 'create' ) {
            //$fields = $this->set_column('increments', 'id') . ';';
            $fields = $this->add_columns($args);
            $fields .= $this->set_column('timestamps', null) . ';';
        }

        else if ( $table_event === 'delete' ) {
            $fields = $this->drop_columns($args);
        }

        else if ( $table_event === 'add' || $table_event === 'update' ) {
            $fields = $this->add_columns($args);
        }

        // Insert the fields into the schema function
        return $this->add_after('function($table) {', $fields, $up);
    }


    protected function migration_down($table_event, $table_action, $table_name, $args)
    {
        $comment = '* Revert the changes to the database.
                    *
                    * @return void';
        $down = Template::comment($comment);
        $down .= Template::func('down');

        if ( $table_event === 'create' ) {
           $schema = Template::schema('drop', $table_name, false);

           // Add drop schema into down function
           $down = $this->add_after('{', $schema, $down);
        } else {
            // for delete, add, and update
            $schema = Template::schema('table', $table_name);
        }

        if ( $table_event === 'delete' ) {
            $fields = $this->add_columns($args);

            // add fields to schema
            $schema = $this->add_after('function($table) {', $fields, $schema);
            
            // add schema to down function
            $down = $this->add_after('{', $schema, $down);
        }

        else if ( $table_event === 'add' ) {
            $fields = $this->drop_columns($args);

            // add fields to schema
            $schema = $this->add_after('function($table) {', $fields, $schema);

            // add schema to down function
            $down = $this->add_after('{', $schema, $down);

        }

        else if ( $table_event === 'update' ) {
            // add schema to down function
            $down = $this->add_after('{', $schema, $down);
        }

        return $down;
    }

    protected function increment()
    {
        return "\$table->increments('id')->unsigned()";
    }

    protected function set_column($type, $field = '',$default=null)
    {
        return empty($field)
            ? "\$table->$type()"
            : (empty($default)?"\$table->$type('$field')":"\$table->$type('$field',$default)");
    }

    protected function add_option($option,$default=null)
    {
        return "->{$option}({$default})";
    }

    /**
     * Add columns
     *
     * Filters through the provided args, and builds up the schema text.
     *
     * @param  $args array  
     * @return string
     */
    protected function add_columns($args)
    {
        $content = '';

        // Build up the schema
        foreach( $args as $arg ) {
            // Like age, integer, and nullable
            //@list($field, $type, $setting) = explode(':', $arg);
            $fields = explode(':', $arg);
            $field = array_shift($fields);
            $type = array_shift($fields);
            $settings = $fields;          

            if ( !$type ) {
                echo "There was an error in your formatting. Please try again. Did you specify both a field and data type for each? age:int\n";
                die();
            }

            // Primary key check
            if ( $field === 'id' and $type === 'integer' ) {
                $rule = $this->increment();
            } else {

                if(preg_match('/\[([^\]]+)\]/', $type,$match)){
                    $type = preg_replace('/\[([^\]]+)\]/', '', $type);
                    $default = $match[1];
                }else{
                    $default = null;
                }
                $rule = $this->set_column($type, $field,$default);

                if ( !empty($settings) ) {
                    foreach ($settings as $setting) {
                        if(preg_match('/\[([^\]]+)\]/', $setting,$match)){
                            $setting = preg_replace('/\[([^\]]+)\]/', '', $setting);
                            $default = $match[1];
                        }else{
                            $default = null;
                        }
                       $rule .= $this->add_option($setting,$default);
                    }                    
                }
            }

            $content .= $rule . ";";
        }

        return $content;
    }

    /**
     * Drop Columns
     *
     * Filters through the args and applies the "drop_column" syntax
     *
     * @param $args array  
     * @return string
     */
    protected function drop_columns($args)
    {
        $fields = array_map(function($val) {
            $bits = explode(':', $val);
            return "'$bits[0]'";
        }, $args);
       
        if ( count($fields) === 1 ) {
            return "\$table->drop_column($fields[0]);";
        } else {
            return "\$table->drop_column(array(" . implode(', ', $fields) . "));";
        }
    }

    public function path($dir,$bundle=DEFAULT_BUNDLE)
    {
        return Bundle::path($bundle) . $dir . DS;
    }

    /**
     * Crazy sloppy prettify. TODO - Cleanup
     *
     * @param  $content string  
     * @return string
     */
    protected function prettify()
    {
        $content = self::$content;

        $content = str_replace('<?php ', "<?php\n\n", $content);
        $content = str_replace('{}', "\n{\n\n}", $content);
        $content = str_replace('public', "\n\n\tpublic", $content);
        $content = str_replace("() \n{\n\n}", "()\n\t{\n\n\t}", $content);
        $content = str_replace('}}', "}\n\n}", $content);

        // Migration-Specific
        $content = preg_replace("/[ ]{0,}?\/\*\*[ ]{0,}?/", "\n\n\t/**", $content);
        $content = preg_replace("/[ ]{1,}\*/", "\t *", $content);
        $content = preg_replace('/ ?Schema::/', "\n\t\tSchema::", $content);
        $content = preg_replace('/\$table(?!\))/', "\n\t\t\t\$table", $content);
        $content = str_replace('});}', "\n\t\t});\n\t}", $content);
        $content = str_replace(');}', ");\n\t}", $content);
        $content = str_replace("() {", "()\n\t{", $content);

        self::$content = $content;
    }

    public function add_after($where, $to_add, $content)
    {
        // return preg_replace('/' . $where . '/', $where . $to_add, $content, 1);
        return str_replace($where, $where . $to_add, $content);
    }

    protected function add_after_content($where, $to_add)
    {
        static::$content = str_replace($where, $where . $to_add, static::$content);

    }
    /**
     * Write the contents to the specified file
     *
     * @param  $file_path string
     * @param $content string
     * @param $type string [model|controller|migration]  
     * @return void
     */
    protected function write_to_file($file_path,  $success = '')
    {
        $success = $success ?: "Create: $file_path.\n";

        if ( File::exists($file_path) ) {
            // we don't want to overwrite it
            echo "Warning: File already exists at $file_path\n";
            return;
        }

        // As a precaution, let's see if we need to make the folder.
        File::mkdir(dirname($file_path));

        if ( File::put($file_path, self::$content) !== false ) {
            echo $success;
        } else {
            echo "Whoops - something...erghh...went wrong!\n";
        }
    }

    public function artisan_run(){
        return Command::run(array('migrate'));
    }

    public function artisan_rollback(){
        return Command::run(array('migrate:rollback'));
    }
}