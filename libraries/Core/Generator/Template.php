<?php namespace Core\Generator;

/**
 * Laravel Generator
 * 
 * Rapidly create models, views, migrations + schema, assets, tests, etc.
 *
 * USAGE:
 * Add this file to your Laravel application/tasks directory
 * and call the methods with: php artisan generate:[model|controller|migration] [args]
 * 
 * See individual methods for additional usage instructions.
 * 
 * @author      Jeffrey Way <jeffrey@jeffrey-way.com>
 * @license     haha - whatever you want.
 * @version     0.8
 * @since       July 26, 2012
 *
 */

use \InvalidArgumentException as Argument;

class Template {
    public static function test($class_name, $test)
    {
        return <<<EOT
    public function test_{$test}()
    {
        \$response = Controller::call('{$class_name}@$test'); 
        \$this->assertEquals('200', \$response->foundation->getStatusCode());
        \$this->assertRegExp('/.+/', (string)\$response, 'There should be some content in the $test view.');
    }
EOT;
    }

    public static function comment($comment){
        return <<<EOT
    /**
     $comment
     */
EOT;
    }

    public static function func($func_name,$params=null,$is='public')
    {   if(!is_null($params)){
            $params = (array)$params;
            $params = (isset($params) and count($params)>0)?implode(',', $params):$params;
        }
        return <<<EOT
    {$is} function {$func_name}({$params})
    {

    }
EOT;
    }

    public static function new_class($name, $extends_class = null)
    {
        $content = "<?php class $name";
        if ( !empty($extends_class) ) {
            $content .= " extends $extends_class";
        }
        $content .= ' {}';
        return $content;
    }

    public static function schema($table_action, $table_name, $cb = true)
    {
        $content = "Schema::$table_action('$table_name'";
        return $cb
            ? $content . ', function($table) {});'
            : $content . ');';
    }
}