<?php namespace Core\Base;

class CommentParser {

	/**
     * name for the embedded data
     *
     * @var string
     */
    public static $embeddedDataName = 'properties';

	/**
     * Regular Expression pattern for finding the embedded data and extract
     * the inner information. It is used with preg_match.
     *
     * @var string
     */
    public static $embeddedDataPattern
        = '/```(\w*)[\s]*(([^`]*`{0,2}[^`]+)*)```/ms';

    /**
     * Pattern will have groups for the inner details of embedded data
     * this index is used to locate the data portion.
     *
     * @var int
     */
    public static $embeddedDataIndex = 2;
    /**
     * Delimiter used to split the array data.
     *
     * When the name portion is of the embedded data is blank auto detection
     * will be used and if URLEncodedFormat is detected as the data format
     * the character specified will be used as the delimiter to find split
     * array data.
     *
     * @var string
     */
    public static $arrayDelimiter = ',';

	/**
     * character sequence used to escape \@
     */
    const escapedAtChar = '\\@';

    /**
     * character sequence used to escape end of comment
     */
    const escapedCommendEnd = '{@*}';

    /**
     * Comment information is parsed and stored in to this array.
     *
     * @var array
     */
    private static $data = array();

	/**
     * Parse the comment and extract the data.
     *
     * @static
     *
     * @param      $comment
     * @param bool $isPhpDoc
     *
     * @return array associative array with the extracted values
     */
	public static function parse($comment, $isPhpDoc = true)
    {
    	$s = new self();
        if ($isPhpDoc) {
            $comment = static::removeCommentTags($comment);
        }
        $s->extractData($comment);
        return static::$data;
    }

    /**
     * Removes the comment tags from each line of the comment.
     *
     * @static
     *
     * @param $comment PhpDoc style comment
     *
     * @return string comments with out the tags
     */
    public static function removeCommentTags($comment)
    {
        $pattern = '/(^\/\*\*)|(^\s*\**[ \/]?)|\s(?=@)|\s\*\//m';
        return preg_replace($pattern, '', $comment);
    }

    /**
     * Extracts description and long description, uses other methods to get
     * parameters.
     *
     * @param $comment
     *
     * @return array
     */
    private function extractData($comment)
    {
        //to use @ as part of comment we need to
        $comment = str_replace(
            array(self::escapedCommendEnd, self::escapedAtChar),
            array('*/', '@'),
            $comment);

        $description = array();
        $longDescription = array();
        $params = array();

        $mode = 0; // extract short description;
        $comments = preg_split("/(\r?\n)/", $comment);
        // remove first blank line;
        array_shift($comments);
        $addNewline = false;
        foreach ($comments as $line) {
            $line = trim($line);
            $newParam = false;
            if (empty ($line)) {
                if ($mode == 0) {
                    $mode++;
                } else {
                    $addNewline = true;
                }
                continue;
            } elseif ($line{0} == '@') {
                $mode = 2;
                $newParam = true;
            }
            switch ($mode) {
                case 0 :
                    $description[] = $line;
                    if (count($description) > 3) {
                        // if more than 3 lines take only first line
                        $longDescription = $description;
                        $description[] = array_shift($longDescription);
                        $mode = 1;
                    } elseif (substr($line, -1) == '.') {
                        $mode = 1;
                    }
                    break;
                case 1 :
                    if ($addNewline) {
                        $line = ' ' . $line;
                    }
                    $longDescription[] = $line;
                    break;
                case 2 :
                    $newParam
                        ? $params[] = $line
                        : $params[count($params) - 1] .= ' ' . $line;
            }
            $addNewline = false;
        }
        $description = implode(' ', $description);
        $longDescription = implode(' ', $longDescription);
        $description = preg_replace('/\s+/ms', ' ', $description);
        $longDescription = preg_replace('/\s+/ms', ' ', $longDescription);
       	foreach ($params as $key => $line) {
            list(, $param, $value) = preg_split('/\@|\s/', $line, 3) + array('', '', '');
            list($value, $embedded) = self::parseEmbeddedData($value);
            $value = array_filter(preg_split('/\s+/ms', $value));
            self::parseParam($param, $value, $embedded);
        }
    }

    /**
     * Parses the inline php doc comments and embedded data.
     *
     * @param $subject
     *
     * @return array
     * @throws Exception
     */
    private function parseEmbeddedData($subject)
    {	
        $data = array();

        while (preg_match('/{@(\w+)\s([^}]*)}/ms', $subject, $matches)) {
            $subject = str_replace($matches[0], '', $subject);
            if ($matches[2] == 'true' || $matches[2] == 'false') {
                $matches[2] = $matches[2] == 'true';
            }
            if ($matches[1] != 'pattern'
                && false !== strpos($matches[2], static::$arrayDelimiter)
            ) {
                $matches[2] = explode(static::$arrayDelimiter, $matches[2]);
            }
            $data[$matches[1]] = $matches[2];
        }
        return array($subject, $data);
    }

    /**
     * Parse parameters that begin with (at)
     *
     * @param       $param
     * @param array $value
     * @param array $embedded
     */
    private function parseParam($param, array $value, array $embedded)
    {
        $allowMultiple = false;
        switch ($param) {
            case 'return' :
                $value = self::formatReturn($value);
                break;
            case 'relation' :
                $value = self::formatRelation($value);
                break;
            case 'link':
            case 'example':
            case 'todo':
                $allowMultiple = true;
            default :
                $value = implode(' ', $value);
        }
   		if (!empty($embedded)) {
            if (is_string($value)) {
                $value = array('description' => $value);
            }
            $value[self::$embeddedDataName] = $embedded;
        }
        if (empty (static::$data[$param])) {
            if ($allowMultiple) {
                static::$data[$param] = array(
                    $value
                );
            } else {
                static::$data[$param] = $value;
            }
        } elseif ($allowMultiple) {
            static::$data[$param][] = $value;
        } elseif ($param == 'param') {
            $arr = array(
                static::$data[$param],
                $value
            );
            static::$data[$param] = $arr;
        } else {
            if (!is_string($value) && isset($value[self::$embeddedDataName])
                && isset(static::$data[$param][self::$embeddedDataName])
            ) {
                $value[self::$embeddedDataName]
                    += static::$data[$param][self::$embeddedDataName];
            }
            static::$data[$param] = $value + static::$data[$param];
        }
    }

    private function formatReturn(array $value)
    {
        $data = explode('|', array_shift($value));
        $r = array(
            'type' => count($data) == 1 ? $data[0] : $data
        );
        $r['description'] = implode(' ', $value);
        return $r;
    }

    private function formatRelation(array $value)
    {
        $data = explode('|', array_shift($value));
        return $data;
    }
}