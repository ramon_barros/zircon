<?php namespace Core\Base;

//use \Laravel\Routing\Controller as Controller;

///////////////////
// Class Laravel //
///////////////////
use \Laravel\Auth;
use \Laravel\Auth\Drivers\Driver;
use \Laravel\Asset;
use \Laravel\Autoloader;
use \Laravel\Blade;
use \Laravel\Bundle;
use \Laravel\Cache;
//use \Laravel\Config;
use \Laravel\Cookie;
use \Laravel\Crypter;
use \Laravel\Database;
use \Laravel\Database as DB;
use \Laravel\Database\Eloquent\Model;
use \Laravel\Event;
use \Laravel\File;
use \Laravel\Routing\Filter;
use \Laravel\Form;
use \Laravel\Hash;
use \Laravel\HTML;
use \Laravel\Input;
use \Laravel\IoC;
use \Laravel\Lang;
use \Laravel\Log;
use \Laravel\Memcached;
use \Laravel\Profiling\Profiler;
use \Laravel\URL;
use \Laravel\Redirect;
use \Laravel\Redis;
use \Laravel\Request;
use \Laravel\Response;
use \Laravel\Routing\Route;
use \Laravel\Routing\Router;
use \Laravel\Database\Schema;
use \Laravel\Section;
use \Laravel\Session;
use \Laravel\Str;
use \Laravel\CLI\Tasks\Task;
use \Laravel\URI;
use \Laravel\Validator;
use \Laravel\View;

/////////////////////
// Class/Model CMS //
/////////////////////
use \stdClass;
use \Zircon_Config;
use \Zircon_Model_Users;
use \Zircon_Model_Modulo;
use \Zircon_Modulo_Install;
use \Sentry;
use \ConfigCms;
use \FilesystemIterator;

/**
 * Métodos utilizado em todos os controllers
 */
class Controller extends \Laravel\Routing\Controller {

  /**
   * Indicates if the controller uses RESTful routing.
   *
   * @var bool
   */
  public $restful = true;
    
  /**
   * The layout being used by the controller.
   *
   * @var string
   */
  public $layout;
  public $view;
  public $template;
  public $type_layout;
  public $model;
  public $table;
  public $method;
  public $routes;
  public $controller;
  public $controllers;
  public $controllers_call;
  public $controllers_call_bundle;
  public $plugins=array();

  /**
   * Dados que serão enviados a view
   *
   * @var array
   */
  public $data = array();

  public $admin = array();

  /**
   * Catch-all method for requests that can't be matched.
   *
   * @param  string    $method
   * @param  array     $parameters
   * @return Response
   */
  public function __call($method, $parameters)
  {
          return Response::error('404');
  }

  /**
   * Contrutor da classe
   * Recupera informações necessários para o funcionamento dos modulos
   */
  public function __construct()
  {
    /**
     * Verifica se esta logado
     */
    $this->filter('before', 'zirconcoreauth')->except(array('auth'));

    $this->controllers = Controller::detect('zircon');
    $this->controllers_call = isset($this->controllers_call)?$this->controllers_call:$this->current_controller_call();
    $this->controllers_call_bundle = isset($this->controllers_call_bundle)?$this->controllers_call_bundle:$this->current_controller_call(false);
    $this->controller = isset($this->controller)?$this->controller:$this->current_controller();
    $this->model =  isset($this->model)?$this->model:$this->current_model();
    $model = $this->model;
    $this->url_controller = isset($this->url_controller)?$this->url_controller:$this->current_url_controller();
    $this->table = $model::$table;

    /**
     * Recupera pasta view do template do CMS (template)
     * @var string
     */
    $this->view = Zircon_Config::cms("zircon::settings.view");

    /**
     * Recupera pasta template do CMS (bootstrap|default)
     * @var string
     */
    $this->template = Zircon_Config::cms("zircon::settings.template");

    /**
     * Recupera layout padrão para o CMS (layouts.admin)
     * @var string
     */
    $this->type_layout = Zircon_Config::cms("zircon::settings.layout");

    /**
     * Cria caminho completo do layout padrão para a View:make
     * @var string
     */
    $this->layout = $this->template($this->type_layout);

    /**
     * Cria os links internos para o CMS.
     */
    $this->set_routes();

    /**
     * Chama o método que adiciona os assets aos modulos
     */
    $this->add_assets();

    /**
     * Recupera informações do controller atual
     */
    $this->config_controller();

    /**
     * Chama o método que verifica as permissões dos usuários
     */
    $this->check_permissions(); // Permissões do CMS
    $this->check_permissions_modules(); // Permissões dos modulos


    /**
     * Recupera informações do modulo selecionado
     */
    $this->config_modulo();

    /**
     * Chama o método que carrega os plugins/extensões para o CMS
     */
    $this->load_plugins();

    /**
     * Chama o método que carrega os assets da pasta template automaticamente
     */
    $this->load_assets();

    $this->check_path_writable();

    /**
     * Construtor da Classe Controller
     * Deve ficar aqui pois a variável $this->layout deve receber o valor antes;
     */
    parent::__construct();
  }

  public function current_controller_call($bundleShow=true){
    $controllers = array();
    foreach ($this->controllers as $con) {
        if(preg_match('/zircon::core/',$con)==true){
          $controller = preg_replace('/zircon::core\./', '', $con);
          if($bundleShow){
            $bundle = $con;
          }else{
            $bundle = preg_replace('/zircon::core\./', '', $con);
          }
          if($controller=='admin'){
            $controllers[$bundle] = preg_replace('/zircon::core\./', '', $con);
          }else{
            $controllers[$bundle] = 'zircon/'.preg_replace('/zircon::core\./', '', $con);
          }
        }else{
          if($bundleShow){
            $bundle = $con;
          }else{
            $bundle = preg_replace('/zircon::modulos\./', '', $con);
          }
          $controller = 'zircon/modulos/'.preg_replace('/zircon::modulos\./', '', $con);
          $controllers[$bundle] = str_replace('.', '/', $controller);
        }       
    }
    //var_dump($controllers);
    /**
     * Recupera a url atual
     * @var array
     */
    $uri = explode('/',URI::current());

    /**
     * O primeiro parametro da url é sempre zircon
     */
    $uri[0] = 'zircon';

    /**
     * Recupera o numero de parametros na url atual
     * @var integer
     */
    $count_uri_current = count($uri);

    /**
     * Percorre os controllers e suas urls para identificar
     * o contoller atual
     */
    $priorities = array();
    foreach ($controllers as $url) {
      /**
       * Transforma a url do controller em um array
       * @var array
       */
      $uri_url = explode('/', $url);

      /**
       * Quantidade de parametros na url do controller
       * @var integer
       */
      $count_uri_url = count($uri_url);
      
      /**
       * Compara as diferenças do array do controller e o array da url atual
       * @var array
       */
      $comp = array_diff_assoc($uri_url, $uri);

      /**
       * Se a diferença for menos ou igual a 0 então armazena os dados em um array
       */
      if(count($comp)<=0){
        //var_dump($url);
        /**
         * Recupera o nome do controller
         * @var array
         */
        $controller = array_search($url, $controllers);
        /**
         * Armazena a quantidade de parametros e o nome do controller
         */
        $priorities[$count_uri_url] = $controller;
        /*
        usort($priorities, function($a, $b)
        {
          if ($a == $b) 
          return 0;
          else
          return ($a > $b) ? -1 : 1;
        });
        */       
      }
    }

    krsort($priorities);
    /**
     * Retorna o maior item do array
     */
    $current = current($priorities);
    return $current?$current:"zircon::core.admin";
  }

  public function current_url_controller(){
    /**
     * Retorna o modulo acessado
     */
    $modulo = $this->controllers_call_bundle;
    $url_modulo = str_replace('.', '/', $modulo);
    return $url_modulo;
  }

  public function current_controller(){
    /**
     * Retorna o controller do modulo acessado
     */
    $controller = $this->controllers_call;
    if(preg_match('/zircon::core/',$controller)==true){
      $controller = preg_replace('/zircon::core./', '', $controller);
    }else{
      $controller = preg_replace('/zircon::modulos./', '', $controller);
    }
    $current_controller_modulo = str_replace('.', '_', $controller);
    return $current_controller_modulo;
  }

  public function current_model(){
    /**
     * Retorna o controller do modulo acessado
     */
    $controller = $this->controllers_call;
    if(preg_match('/zircon::core/',$controller)==true){
      if($controller=='zircon::core.modulos'){
        return 'Zircon_Model_Modulo';
      }elseif($controller=='zircon::core.users' or $controller=='zircon::core.admin'){
       return 'Zircon_Model_Users';
      }
    }else{
      $controller = preg_replace('/zircon::modulos\./', '', $controller);
      return str_replace('.', '_', $controller);
    }
  }

  /**
   * Cria as url do CMS baseado nas rotas informadas no arquivo settings
   */
  public function set_routes(){
    $url_cms = URI::segment(1);
    $this->routes = new stdClass;
    $this->routes->login = $url_cms.'/auth';
    $this->routes->index = $url_cms.'/index';
    $this->routes->users = $url_cms.'/users';
    $this->routes->modulos = $url_cms.'/modulos';
    $this->routes->dashboard = $url_cms.'/dashboard';
    if($this->controller=='admin'){
      $this->routes->edit = "{$this->url_controller}/edit";
      $this->routes->save = "{$this->url_controller}/save";
      $this->routes->delete = "{$this->url_controller}/delete";
      $this->routes->search = "{$this->url_controller}/search";
    }elseif($this->controller=='modulos' or $this->controller=='users'){
      $this->routes->edit = "{$this->url_controller}/edit";
      $this->routes->save = $url_cms."/{$this->url_controller}/save";
      $this->routes->delete = $url_cms."/{$this->url_controller}/delete";
      $this->routes->search = $url_cms."/{$this->url_controller}/search";
    }else{
      $this->routes->edit = $url_cms."/{$this->url_controller}/edit";
      $this->routes->save = $url_cms."/{$this->url_controller}/save";
      $this->routes->delete = $url_cms."/{$this->url_controller}/delete";
      $this->routes->search = $url_cms."/{$this->url_controller}/search";
    }
  }

  /**
   * Retorna a url de login
   * @return string
   */
  public function url_login(){
    return (isset($this->routes->login) and strlen($this->routes->login)>0)?$this->routes->login:'zircon/auth';
  }

  /**
   * Retorna a url da página inicial do CMS
   * @return string
   */
  public function url_index(){
    return (isset($this->routes->index) and strlen($this->routes->index)>0)?$this->routes->index:'zircon/index';
  }

  /**
   * Retorna a url da página de usuários
   * @return string
   */
  public function url_users(){
    return (isset($this->routes->users) and strlen($this->routes->users)>0)?$this->routes->users:'zircon/users';
  }

  /**
   * Retorna a url da página de modulos
   * @return string
   */
  public function url_modulos(){
    return (isset($this->routes->modulos) and strlen($this->routes->modulos)>0)?$this->routes->modulos:'zircon/modulos';
  }

  /**
   * Retorna a url da home do CMS
   * @return string
   */
  public function url_dashboard(){
    return (isset($this->routes->dashboard) and strlen($this->routes->dashboard)>0)?$this->routes->dashboard:'zircon/dashboard';
  }

  /**
   * Retorna a url para editar registro
   * @return string
   */
  public function url_edit(){
    return (isset($this->routes->edit) and strlen($this->routes->edit)>0)?$this->routes->edit:"{$this->url_controller}/edit";
  }

  /**
   * Retorna a url para salvar um registro
   * @return string
   */
  public function url_save(){
    return (isset($this->routes->save) and strlen($this->routes->save)>0)?$this->routes->save:"{$this->url_controller}/save";
  }

  /**
   * Retorna a url para deletar um registro
   * @return string
   */
  public function url_delete(){
    return (isset($this->routes->delete) and strlen($this->routes->delete)>0)?$this->routes->delete:"{$this->url_controller}/delete";
  }

  /**
   * Retorna a url de busca
   * @return string
   */
  public function url_search(){
    return (isset($this->routes->search) and strlen($this->routes->search)>0)?$this->routes->search:"{$this->url_controller}/search";
  }

  /**
   * Verifica as permissões dos usuário em todos os controllers
   * @return void
   */
  private function check_permissions()
  {
    /**
     * Verifica se o usuário esta logado
     */
    if(Sentry::check())
    {
        /**
         * Verifica se o usuário é adminitrador
         */
        if(Sentry::user()->has_access('is_admin')){
            $this->admin['menu'] = array_merge($this->admin['menu'], array('Usuários'=>$this->url_users()));
            if(URI::segment(2)=='users'){
                $this->admin['addMenu'] = array($this->url_users().'/add' => 'Adicionar Novo');
            }
        }
        /**
         * Verifica se o usuário é BOSS
         */
        if(Sentry::user()->has_access('superuser')){
            $this->admin['menu']  = array_merge($this->admin['menu'], array('Modulos'=>$this->url_modulos(),'Usuários'=>$this->url_users()));
            if(URI::segment(2)=='users'){
                $this->admin['addMenu'] = array($this->url_users().'/add' => 'Adicionar Novo');
            }
            if(URI::segment(2)=='modulos'){
                $this->admin['addMenu'] = array($this->url_modulos().'/add' => 'Adicionar Novo');
            }                           
        }
        /**
         * Retorna informações do usuário logado
         */
        $this->admin['user'] = Zircon_Model_Users::get_user(Sentry::user()->get('id'));          
    }
  }

  /**
   * Verifica as permissões e so o modulo é mostrado no menu principal
   * @return [type] [description]
   */
  private function check_permissions_modules()
  {
    /**
     * Verifica se o usuário esta logado
     */
    if(Sentry::check())
    {
      $this->modulos = Zircon_Modulo_Install::modulos_installed();
      if(is_array($this->modulos))
      {
        foreach ($this->modulos as $modulos) 
        {
            
          $model = $modulos->nome;

          /**
           * Verifica se o modulo será visivel no menu principal
           * @var boolean
           */
          if($model::$menu===true){
            /**
             * Verifica se o modulo contem a variável $admin
             * e se contem subtitle
             */
            if(isset($model::$admin) and isset($model::$admin['subtitle'])){
              $title = $model::$admin['subtitle'];
            }else{
              $title = $modulos->nome;
            }
            $addModulo = array($title=>strtolower($this->url_modulos()."/{$modulos->nome}"));
            $this->admin['menu'] = array_merge($this->admin['menu'], $addModulo);
          }
        }
      }
    }
  }

  public function config_controller(){
    $model = $this->model;

    $this->admin['controller'] = $this->current_controller();
    $this->admin['url_controller'] = $this->current_url_controller();
    /**
    * Titlo da aplicação definido em:
    * [bundles/zircon/config/settings.php]
    * @var array
    */
    $this->admin['title'] = Zircon_Config::cms("zircon::settings.title");
    
    /**
     * Subtitulo do modulo
     */
    $this->admin['subtitle'] = (isset($model::$admin) and isset($model::$admin['subtitle']))?$model::$admin['subtitle']:ucfirst(str_replace('-', ' ', $model));
    /**
     * Verifica se existe um submenu
     */
    if(isset($model::$admin) and isset($model::$admin['submenu'])){
      $this->admin['submenu'] = $model::$admin['submenu'];
    }    
    
    /**
     * Recupera link/url para o menu Home conforme rota especificada.
     */
    $this->admin['menu'] = array(
      'Home'=> $this->url_dashboard()
    );

    /**
     * Chave primária do model do controller
     */
    $this->admin['modelKey'] = $model::key();

    /**
     * Colunas para construção do formulário
     */
    $this->admin['columns'] = $model::columns();

    /**
     * Registros cadastrados no modulo
     */
    $this->admin['data'] = $model::paginate();

    /**
     * Recupera link/url para o formulário (edit|add) conforme rota.
     */
    $this->admin['url_save'] = $this->url_save();
    
    /**
     * Recupera link/url editar para a listagem.
     */
    $this->admin['url_edit'] = $this->url_edit();

    /**
    * Recupera link/url deletar para a listagem.
    */
    $this->admin['url_delete'] = $this->url_delete();

    $this->admin['url_modulos'] = $this->url_modulos();
  }

  public function config_modulo(){
    /**
     * Verifica se o usuário esta logado
     * Verifica se o controller é diferente de modulo
     * Verifica se o model é diferente de Zircon_Model_Modulo
     */
    if(($this->model=='Zircon_Model_Modulo' or $this->model=='Zircon_Model_Users') and ($this->controller=='modulos' or $this->controller=='users')){
      return false;
    }elseif(Sentry::check()){

      $model = $this->model;

      /**
       * Retorna o modulo acessado
       */
      $url_modulo = $this->current_url_controller();
      //$modulo = (isset($model::$modulo) and strlen($model::$modulo)>0)?$model::$modulo:'';

      /**
       * Subtitulo do modulo
       */
      $this->admin['subtitle'] = (isset($model::$admin) and isset($model::$admin['subtitle']))?$model::$admin['subtitle']:ucfirst(str_replace('-', ' ', $model));

      /**
       * Colunas para construção do formulário
       */
      $this->admin['columns'] = $model::columns();

      /**
       * Registros cadastrados no modulo
       */
      $this->admin['data'] = $model::paginate();

      $this->admin['url_search'] = URL::home() . $this->url_modulos()."/{$url_modulo}/search";
      $this->admin['url_save'] = URL::home() . $this->url_modulos()."/{$url_modulo}/save";
      $this->admin['url_edit'] = URL::home() . $this->url_modulos()."/{$url_modulo}/edit";
      $this->admin['url_delete'] = URL::home() . $this->url_modulos()."/{$url_modulo}/delete";
     
      /**
       * Chave primária do modulo
       */
      $this->admin['modelKey'] = $model::key();
      
      /**
       * Verifica se existe um submenu
       */
      if(isset($model::$admin) and isset($model::$admin['submenu'])){
        $this->admin['submenu'] = $model::$admin['submenu'];
      }      
      
      /**
       * Verifica se foi acessado o modulo
       */
      if( !is_null($url_modulo) and preg_match('/'.str_replace('/', '\/', $url_modulo).'/', URI::current()) )
      {
        /**
         * Verifica se o campo de busca esta visivel
         */
        if(isset($model::$search)){
            $this->admin['search'] = $model::$search;
            $this->admin['placeholder'] = isset($model::$search_placeholder)?$model::$search_placeholder:'Digite o termo da busca';
        }
        /**
         * Verifica se esta ativado o editor
         * @var string
         */
        if(isset($model::$editor) and $model::$editor == Zircon_Config::get('zircon::settings.editor')){
          $this->add_editor();
        }
      
        /**
         * Verifica se o usuário tem permissão para adicionar registros
         */
        if(Zircon_Model_Modulo::modulo_user_permissions(strtolower($model),'add')){
            $this->admin['addMenu'] = array(strtolower($this->url_modulos()."/{$url_modulo}/add") => 'Adicionar Novo');
        }
        /**
         * Caso o usuário estiver dentro do modulo no modos de edição e inclusão
         * não mostra o botão de adicionar novo registro
         */
        if(URI::segment(4)=='edit' or URI::segment(4)=='add' or URI::segment(3)=='fotos'){
            $this->admin['addMenu'] = array();
        }
        /**
         * Recupera os botões de editar e deletar referente as permissões do usuário
         */
        $this->admin['buttons'] = Zircon_Model_Modulo::modulo_buttons(strtolower($model));
      }
    }        
  }

  /**
   * Lista os registros do modulo
   * @return array dados do modulo
   */
  public function get_index()
  {
    $this->admin['content'] = $this->template('zircon.list');
    return $this->admin;
  }
  
  /**
   * Formulário de cadastro
   * @return array dados do modulo
   */
  public function get_add()
  {
      $model = $this->model;
      $this->admin['content'] = $this->template('zircon.modulos.form');     
      $this->admin['data'] = Zircon_Model_Modulo::columns_add($model);

      return $this->admin;
  }

  /**
   * Formulário de edição
   * @param  string $id chave primária que será alterada
   * @return array dados do modulo
   */
  public function get_edit($id=null) 
  {
    $model = $this->model;
    $this->admin['content'] = $this->template('zircon.modulos.form');
    if (!($this->admin['data'] = $model::find($id))) {
        return Response::error('404');
    }
    $this->convertdb2view();
    return $this->admin;
  }

   /**
   * Página inicial do CMS
   * @return html
   */
  public function get_dashboard(){
      return View::make($this->template('zircon.dashboard'));
  }

  /**
   * Busca de registros dos modulos
   * @return array dados do modulo
   */
  public function post_search() {
    $model = $this->model;
    $this->admin['content'] = $this->template('zircon.search');
    $this->admin['data'] = $model::search();
    return $this->admin;
  }

  /**
   * Metodo padrão para salvar informações dos formulários
   * @return [type] [description]
   */
  public function post_save()
  {
      $model = $this->model;

      $input = Input::all();
      $new = false;
      $relationship = false;
      if(isset($input[$model::$key]) and strlen($input[$model::$key])>0){
        if (!($data = $model::find($input[$model::$key]))) {
          $data = new $model();
          $new = true;
        }
        $table = $data->table();
      }else{
        $data = new $model();
        $new = true;
        $table = $data->table();
      }
      
      $validation = Validator::make($input, $model::rules());

      if ($validation->fails()) {
          $this->admin['error'] =  $validation->errors->all();
          $this->messages('error',$this->admin['error']);
          return Redirect::to(Request::referrer())->with_input();
      } else {
         $fields = array();

         $db = Zircon_Config::get('database.default');
         switch ($db) {
           case 'pgsql':
             $sql = "SELECT column_name FROM information_schema.columns WHERE table_name = '{$table}'";
             //$lastId = DB::connection('pgsql')->pdo->lastInsertId();
             
              foreach (DB::query($sql) as $field) {
                  $fields[] = $field->column_name;
              }
             break;
           case 'sqlite':
             $sql = "SELECT column_name FROM information_schema.columns WHERE table_name = '{$table}'";
             //$lastId = DB::connection('sqlite')->pdo->lastInsertId();
             break;
           case 'sqlsrv':
             $sql = "SELECT column_name FROM information_schema.columns WHERE table_name = '{$table}'";
             //$lastId = DB::connection('sqlsrv')->pdo->lastInsertId();
             break;
           case 'mysql':
           default:
             $sql = "SHOW FIELDS FROM {$table}";
             //$lastId = DB::connection('mysql')->pdo->lastInsertId();
              foreach (DB::query($sql) as $field) {
                  $fields[] = $field->field;
              }
             break;
         }
                
          foreach ($input as $key => $value) {
              if($key == "csrf_token") continue;
              if($key == $model::$key) continue;

                if (in_array($key, $fields)) {

                    /**
                     * Verifica se o campo é um arquivo para upload
                     */
                    if ( in_array($key, Zircon_Config::get('zircon::settings.uploader'))) {
                      $data->$key = \Dash\Upload::save($key);
                      
                      /**
                       * Verifica se o campo é um decimal e troca a
                       */
                    }elseif ( in_array($key, Zircon_Config::get('zircon::settings.money'))) {
                      $data->$key = str_replace(',', '.', $value);
                    }elseif ( in_array($key, Zircon_Config::get('zircon::settings.date'))) {
                      $data->$key = vsprintf(
                                      '%3$04d-%2$02d-%1$02d %4$02d:%5$02d:%6$02d' , sscanf(
                                              $value,
                                              '%1$d/%2$d/%3$d %4$d:%5$d:%6$d'
                                      )
                                    );
                    } elseif(gettype($value) == 'string') {
                        if($key == 'slug') {
                            $data->$key = Str::slug($value);
                        } else {
                            $data->$key = $value;
                        }
                    }
                } else {
                    $relationship = true;
                }

          }

          $data->save();
          if ($relationship) {
              $lastId = $new?$lastId = DB::connection('mysql')->pdo->lastInsertId():$input[$model::$key];
              $data = $model::find($lastId);
              foreach ($input as $key => $value) {
                  if($key == "csrf_token") continue;
                  if (!in_array($key, $fields)) {
                      $data->$key()->sync(is_array($value)?$value:array($value));
                  }
              }
          }

          if(isset($model::$messages) and is_array($model::$messages)){
            $type = 'success';//$model::$messages['edit']['type'];
            $messages = '';//$model::$messages['edit']['msg'];
          }else{
            $type = 'info';
            $messages = 'Dados atualizados';
          } 
          $this->messages($type,$messages);
      }
      if(isset($model::$referrer) and $model::$referrer==true){
        return Redirect::to(Request::referrer());
      }else{
        return Redirect::to(URL::home().$this->url_modulos().'/'.$this->url_controller);
      }
  }

  public function post_delete()
  {
  
  $model = $this->model;
  $input = Input::all();
  $id = $input[$model::key()];
  $path = path('public') . Zircon_Config::get('zircon::settings.upload_path');
  if($id){
      if (!( $data = $model::find($id))) {
          $data = new $model();
      }
      $fields = $this->get_fields_db($data);
      /**
       * Deleta a relação entre as tabelas
       */
      foreach ($input as $key => $value) {
        if($key == "csrf_token") continue;
        if($key == $model::$key) continue;
        if (!in_array($key, $fields)) {
            $data->$key()->delete();
        }
      }  

      /**
       * Apaga o arquivo ( foto, logo, file ) caso houver
       */
      foreach ($fields as $field) {
        if($field == "csrf_token") continue;
        if($field == $model::$key) continue;
        if (in_array($field, Zircon_Config::get('zircon::settings.uploader'))
        and isset($data->$field) 
        and \Dash\Upload::checkPath($path) ) {
            File::delete($path . '/' . $data->$field);
        }
      }
      
      /**
       * Deleta o registro da tabela principal
       */
      $data->delete();                     
      
      $this->messages('success','Registro deletado com sucesso!');
  }else{
      $this->messages('error','ID não informado');
  }
  return Redirect::to(Request::referrer());
  }

  public function get_fields_db($data){
    $table = $data->table();
    $db = Zircon_Config::get('database.default');
    $fields = array();
    switch ($db) {
     case 'pgsql':
       $sql = "SELECT column_name FROM information_schema.columns WHERE table_name = '{$table}'";
       //$lastId = DB::connection('pgsql')->pdo->lastInsertId();
       
        foreach (DB::query($sql) as $field) {
            $fields[] = $field->column_name;
        }
       break;
     case 'sqlite':
       $sql = "SELECT column_name FROM information_schema.columns WHERE table_name = '{$table}'";
       //$lastId = DB::connection('sqlite')->pdo->lastInsertId();
       break;
     case 'sqlsrv':
       $sql = "SELECT column_name FROM information_schema.columns WHERE table_name = '{$table}'";
       //$lastId = DB::connection('sqlsrv')->pdo->lastInsertId();
       break;
     case 'mysql':
     default:
       $sql = "SHOW FIELDS FROM {$table}";
       //$lastId = DB::connection('mysql')->pdo->lastInsertId();
        foreach (DB::query($sql) as $field) {
            $fields[] = $field->field;
        }
       break;
    }
    return $fields;
  }

  public function convertdb2view(){
    /**
     * Recupera colunas no banco de dados
     * @var array
     */
    $fields = $this->get_fields_db($this->admin['data']);
    foreach ($fields as $field) {
      if(isset($this->admin['data']->$field)){
        if ( in_array($field, Zircon_Config::get('zircon::settings.money')) ) {
          //setlocale(LC_MONETARY, 'pt_BR');
          //$this->admin['data']->$field = money_format($this->admin['data']->$field,'%n');
          $this->admin['data']->$field = $this->money($this->admin['data']->$field);
        } elseif( in_array($field, Zircon_Config::get('zircon::settings.date')) ){
          $this->admin['data']->$field = $this->date($this->admin['data']->$field);
        }
      }
    }
  }

  public function money($value){
    return number_format($value, 2, ",", ".");
  }

  public function date($value){
    $date = vsprintf(
              '%1$02d/%2$02d/%3$04d %4$02d:%5$02d:%6$02d' , sscanf(
                      $value,
                      '%3$d-%2$d-%1$d %4$d:%5$d:%6$d'
              )
            );
    return str_replace(' 00:00:00', '', $date);
  }

  public function check_path_writable(){
    if(Request::env()=='local'){
      $path_migrations = Bundle::path('zircon').'migrations';
      if( !is_writable( $path_migrations ) ){
          $this->messages('error','A pasta '.$path_migrations.' deve ter permissão de escrita, somente no desenvolvimento!');
      }
      $path_controllers = Bundle::path('zircon').'controllers'.DS.'modulos';
      if( !is_writable( $path_controllers ) ){
          $this->messages('error','A pasta '.$path_controllers.' deve ter permissão de escrita, somente no desenvolvimento!');
      }
    }else{
      $path_migrations = Bundle::path('zircon').'migrations';
      if( is_writable( $path_migrations ) ){
          $this->messages('error','A pasta '.$path_migrations.' não deve ter permissão de escrita em produção, somente no desenvolvimento!');
      }
      $path_controllers = Bundle::path('zircon').'controllers'.DS.'modulos';
      if( is_writable( $path_controllers ) ){
          $this->messages('error','A pasta '.$path_controllers.' não deve ter permissão de escrita em produção, somente no desenvolvimento!');
      }
    }
    /**
     * Storage
     */
    $path_storage_cache = path('storage').'cache';
    if( !is_writable( $path_storage_cache ) ){
        $this->messages('error','A pasta '.$path_storage_cache.' deve ter permissão de escrita!');
    }
    $path_storage_logs = path('storage').'logs';
    if( !is_writable( $path_storage_logs ) ){
        $this->messages('error','A pasta '.$path_storage_logs.' deve ter permissão de escrita!');
    }
    $path_storage_views = path('storage').'views';
    if( !is_writable( $path_storage_views ) ){
        $this->messages('error','A pasta '.$path_storage_views.' deve ter permissão de escrita!');
    }
  }

  /**
   * Criar as mensagens de notificação para o usuário.
   * @param  string $type tipo de mensagem a ser mostrada ( heading, success, danger, error, info, block, )
   * @param  string $msg  mensagem a ser mostrada
   */
  public function messages($type='info',$msg=''){
    \Dash\Messages::add($type,$msg);
  }

  public function upload($key){
    $input = Input::all();
    if ( in_array($key, Zircon_Config::get('zircon::settings.uploader') )) {
        return \Dash\Upload::save($key);
    } elseif(gettype($input[$key]) == 'string') {
        if($key == 'slug') {
            return Str::slug($input[$key]);
        } else {
            return $input[$key];
        }

    }
    return false;
  }
  /**
   * Mostra mensagem de log no profile, bom para debug.
   * @param  string $type [description]
   * @param  string $msg  [description]
   */
  public function log($type='info',$msg=''){
    Log::write($type,$msg);
  }

  /**
   * Monta o caminho onde se encontra a view.
   * @param  string $view nome da view
   * @return string       retorna o caminho da view a ser passado para View::make
   */
  public function template($view){
    return "zircon::{$this->view}.{$this->template}.{$view}";
  }

  /**
   * Carrega automaticamente os scripts e estilos do tema selecionado
   * @param  string $directory 
   * @return [type]            [description]
   */
  public function load_assets($directory=null){
    if(is_null($directory))
    {
      $directory = path('public').'bundles'.DS.'admin'.DS.$this->view.DS.$this->template;      
    }
    $url = URL::base();
    if(is_dir($directory)){
      $items = new FilesystemIterator($directory, FilesystemIterator::SKIP_DOTS);

      foreach ($items as $item)
      {
              if ($item->isDir() and ($item->getBasename()==='js' or $item->getBasename()==='css'))
              {
                      $this->load_assets($item->getRealPath());
              }else{
                  $file = $item->getBasename();
                  $ext = File::extension($file);
                  if($ext=='js' or $ext=='css'){
                    $path = str_replace('\\','/',str_replace(path('public'),'',$directory));
                    Asset::add(str_replace('.', '-', $file),$url.'/'.$path.'/'.$file);
                  }
              }
      }
    }else{
      $this->log('error','Diretório não existe load_assets():'.$directory);
    }
  }
  /**
   * Adiciona css e js do CMS em todos os controllers.
   */
  public function add_assets(){
    /*
    Asset::add('sortable', 'libs/ui/js/jquery-ui.js');
    Asset::add('chosen', 'libs/chosen/chosen.jquery.min.js');
    Asset::add('chosen', 'libs/chosen/chosen.css');
    Asset::add('chosen_run', 'libs/chosen/run.js');
    */
  }

  public function load_plugins($directory=null){
    $plugins = Zircon_Config::get('zircon::settings.plugins');
    if(is_null($directory))
    {
      $directory = path('public').'bundles'.DS.'plugins';      
    }
    if(is_dir($directory))
    {
      $url = URL::base();
      if(is_dir($directory))
      {
        $items = new FilesystemIterator($directory, FilesystemIterator::SKIP_DOTS);
        foreach ($items as $item)
        {
          if ($item->isDir())
          {
                  $this->load_plugins($item->getRealPath());
          }else{
              $file = $item->getBasename();
              $ext = File::extension($file);
              if(in_array($file,$plugins) and ($ext=='js' or $ext=='css')){
                $key = array_search($file, $plugins);
                $path = str_replace('\\','/',str_replace(path('public'),'',$directory));
                /**
                 * Adiciona os arquivos dos plugins em um array conforme a ordem
                 * do arquivo de configuração
                 */
                $this->plugins[$key] = array(str_replace('.', '-', $file)=>$url.'/'.$path.'/'.$file);
              }
          }
        }
        ksort($this->plugins);
        foreach ($this->plugins as $plugin) {
          $name = key($plugin);
          $file = current($plugin);
          Asset::add($name,$file);
        }
      }
    }
  }

  public function add_editor(){
    Asset::add('redactor', 'libs/redactor/redactor/redactor.css');
    Asset::add('redactor', 'libs/redactor/redactor/redactor.js');
    Asset::add('redactor_lang', 'libs/redactor/redactor/pt_br.js');
    Asset::add('redactor_run', 'libs/redactor/redactor/run.js');
  }
}