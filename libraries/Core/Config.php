<?php namespace Core;

/**
 * CMS Config
 *
 * @package		config
 * @category    config, laravel
 * @author		Ramon Barros
 * @copyright	Copyright (c) 2012, Ramon Barros.
 * @license		http://www.ramon-barros.com/
 * @link		http://public.ramon-barros.com/
 * @since		Version 1.0
 * @filesource  Config.php
 */

use \InvalidArgumentException as Argument;
use \Laravel\Bundle as Bundle;
use \Laravel\File as File;
use \Laravel\Database\Schema as Schema;
use \ConfigCms as ConfigCms;

/**
 * Config carregamento das configurações
 * @abstract
 * @author Ramon Barros
 * @package system
 * @subpackage config
 * @category api,config, design pattern
 */
class Config extends \Laravel\Config {

	/**
	 * Recupera configuração da tabela config ou do arquivo de configuração
	 * @param  string $config 
	 * @return string|array
	 */
	public static function cms($config=null){

		/**
		 * Verifica se foi passado uma string
		 */
		if(isset($config) and strlen($config)>0){

			/**
			 * Bundle (admin|cielo|pagseguro) ou application
			 * @var string
			 */
			$name = Bundle::name($config);

			/**
			 * Diretório do Bundle admin CMS
			 * @var string
			 */
		    $path = Bundle::path('zircon');

		    /**
		     * Verifica se existe :: na configuração passada, indicando um Bundle
		     */
		    if(preg_match('/[\:\:]/', $config)){ // Adicionado o nome do Bundle Ex.: zircon::
		      $bundle = $name."::";
		    }else{ // Caso contrário deixa NULL
		      $bundle = null;
		    }

		    /**
		     * Retorna um array contendo os segmentos (pastas e chave)
		     * @var array
		     */
		    $segments = explode('.', Bundle::element($config));

		    /**
		     * O ultimo parametro é a chave ou coluna no bando de dados
		     * @var string
		     */
		    $key = end($segments);
		    
		    /**
		     * Verifica se o model configcms existe
		     * @var object
		     */
		    $return = Config::get($bundle.implode('.', $segments));
		    if(File::exists($path.'models'.DS.'modulos'.DS.'configcms'.EXT)){
		      /**
		       * Verifica se a tabela config existe
		       */
		      if(isset(ConfigCms::$table) and Schema::exists(ConfigCms::$table)){
		        /**
		         * Recupera infomações do banco de dados
		         * @var object
		         */
		        $config = ConfigCms::first();
		        if(isset($config->id)){
		          /**
		           * Retorna o titulo da aplicação (db ou file)
		           * @var string
		           */
		          $return = (isset($config->$key) and strlen($config->$key)>0)?$config->$key:Config::get($bundle.implode('.', $segments));
		        }
		      }
		    }
		    return $return;
		}else{
			throw new Argument("Você deve informar uma chave para retornar a configuração.");
		}
	}
}