<?php namespace Core;

/**
 * CMS Install
 *
 * @package     config
 * @category    config, laravel
 * @author      Ramon Barros
 * @copyright   Copyright (c) 2012, Ramon Barros.
 * @license     http://www.ramon-barros.com/
 * @link        http://public.ramon-barros.com/
 * @since       Version 1.0
 * @filesource  Config.php
 */

use \InvalidArgumentException as Argument;
use \Laravel\Bundle;
use \Laravel\File;
use \Laravel\Database\Schema;

/**
 * Instalar/Atualizar o CMS
 * @abstract
 * @author Ramon Barros
 * @package zircon
 * @subpackage installer
 * @category core
 */
class Installer {

    /**
     * Controller
     * @var object
     */
    private $currentVersion;
    private $lastVersion;
    private $controller;
    
    private $parseUrl;
    private $host = "https://bitbucket.org"; //https://github.com
    private $timeout = 30;
    private $user = "ramon_barros"; //rbarros
    private $repositori = "zircon"; //zircon
    
    private $urlFiles = "raw/master"; //master
    private $fileNameVersion = ".version"; //.version
    private $fileTypeVersion = ".json"; //.json

    private $urlDownload = "get"; //archive
    private $fileDownload = "master"; // master
    private $fileType = ".zip"; // .zip

    public function __construct() {
        $this->controller = new \Core\Base\Controller();
        $file = $this->download('for latest versions', false, true);
        //$this->checkNewVersion();
    }
    
    public function getPath($path=null){
        return Bundle::path('zircon').$path;
    }

    public function getFile($file=null){
        return File::get($file);
    }

    public function getFileExt($file=null){
        return File::extension($file);
        //return pathinfo($file, PATHINFO_EXTENSION)
    }

    public function getCurrentVersion(){
        $file = $this->fileNameVersion.$this->fileTypeVersion;
        $path = $this->getPath($file);
        $ext = $this->getFileExt( $path );
        if( $ext === "json" ) {
            $fileversion = json_decode( $this->getFile( $path ) );
            $this->currentVersion = $fileversion->info->version;
        } else if( $ext === "xml"){
            $fileversion = simplexml_load_string( $this->getFile( $path ) );
            $this->currentVersion = (string)$fileversion->version;
        }
        return $this->currentVersion;
    }

    public function getLastVersion() {
        $file = $this->fileNameVersion.$this->fileTypeVersion;
        $ext = $this->getFileExt( $file );
        $content = $this->download('Checking for latest versions', true, false);
        if( $ext === "json" ) {
            $fileversion = json_decode( $content );
            $this->lastVersion = $fileversion->version;
        } else if( $ext === "xml"){
            $fileversion = simplexml_load_string( $content );
            $this->lastVersion = (string)$fileversion->version;
        }
        return $this->lastVersion;
    }

    /**
     * Verifica a versão do ZirconCMS
     * @return [type] [description]
     */
    public function checkNewVersion() {
        if (version_compare($this->getCurrentVersion(), $this->getLastVersion()) < 0) {
          echo "Existe uma nova versão {$this->lastVersion}";
        } else {
          echo "Versão do CMS atualizada atual:{$this->currentVersion} ultima:{$this->lastVersion}";
        }
    }
    
    private function createUrlDownload($version=true){
        if($version === true){
            $urlFile = trim($this->host,'/').'/'.trim($this->user,'/').'/'.trim($this->repositori,'/').'/'.trim($this->urlFiles,'/').'/'.$this->fileNameVersion.$this->fileTypeVersion;
        } else if($version === false) {
            $urlFile = trim($this->host,'/').'/'.trim($this->user,'/').'/'.trim($this->repositori,'/').'/'.trim($this->urlDownload,'/').'/'.$this->fileDownload.$this->fileType;
        }
        if( preg_match('/(http[s]?:\/\/)github.com/', $this->host, $matches) ){
            $this->host = $matches[1].'codeload.github.com';
            if($version === false) {
                $urlFile = trim($this->host,'/').'/'.trim($this->user,'/').'/'.trim($this->repositori,'/').'/'.trim($this->fileType,'.').'/'.$this->fileDownload;
            }
        }
        $this->parseUrl($urlFile);
        return $urlFile;
    }

    private function download($title, $version = true, $savefile = true) {
    
        $urlFile = $this->createUrlDownload($version);

        echo $downloadstatusclass = "download-".strtolower(str_replace(' ', '-', $title));
        
        if(gettype($fp = $this->checkConnect()) == "resource") {
          stream_set_timeout($fp, 10);

          $fl = null;
          $filename = null;
          $filesize = null;
          $header = null;
          $content = null;

          $out = "GET {$this->parseUrl['path']} HTTP/1.1\r\n".
                 "Host: {$this->parseUrl['host']}\r\n".
                 "Content-type: text/html\r\n".
                 "Connection: close\r\n\r\n";

          fwrite($fp, $out);
          //fputs($fp, $out, strlen($out));

          while($data = @fgets($fp)) {
            if ($this->downloadCheckTimeOut($fp, $title)) return false;
            
            if($data == "\r\n") break;
              
            $header .= $data;
          }

          preg_match('/filename=(.*)/i',$header, $matches);
          
          if (isset($matches[1])) {
            $filename = str_replace('"','',$matches[1]);
          }
          
          preg_match('/Content-Length:(.*)/i',$header, $matches);
          if (isset($matches[1])) {
            $filesize = (int)$matches[1];
          }

          if ($savefile) {
            $filename = path('storage').'work'.DS.'zircon'.$this->fileType;
            $fl = fopen($filename, 'wb');
          }

          $time = null;
          $percentage = 0;
          $downloadsize = 0;

          $cnt = 0;
          while (!feof($fp)) {
            if ($filesize) {
              $percentage = round($downloadsize * 100 / $filesize);
            }

            if ($this->downloadCheckTimeOut($fp, $title)) { 
                return false;
            }
              
            if (!$time || time() - $time > 1) {
              echo $percentage."%";
              $time = time();
            }
            
            $data = fread($fp, 8192);
            $downloadsize += strlen($data);
            
            if (strlen($data) == 0) {
              echo $percentage."%";
              break;
            }

            if ($fl){
                fwrite($fl, $data, 8192);
                $cnt += $data;
                if ($cnt >= $filesize) break;
            } else {
                $content .= $data;
            }
          }
          
          fclose($fp);
          if ($fl) fclose($fl);

          if ($savefile) {
            return $filename;
          }
          return $content;
        }
    }

    private function parseUrl($url=null) {
        $parts = parse_url($url);
        if (!empty($parts['query'])) {
            $parts['path'] .= '?' . $parts['query'];
        }
        return $this->parseUrl = $parts;
    }

    private function checkConnect(){
        if(isset($this->parseUrl['scheme']) and $this->parseUrl['scheme'] === 'https' ) {
            $host = 'ssl://'.$this->parseUrl['host'];
            $port = 443;
        } else {
            $host = $this->parseUrl['host'];
            $port = 80;
        }
        $fp = fsockopen($host, $port, $errno, $errstr, $this->timeout);
        if (!$fp) {
          return false;
        }
        return $fp;
    }

    private function downloadCheckTimeOut($fp, $title) {
        $status = socket_get_status($fp);
        if ($status["timed_out"]) {
          return true;
        }
        return false;
    }
}