<?php

/**
 * Verifica se o model configcms existe
 * @var object
 */
$routes = Config::get('zircon::settings.routes');
$filemodel = File::get(path('bundle').'zircon/models/modulos/configcms.php');
if($filemodel){
  /**
   * Verifica se a tabela config existe
   */
  if(Schema::exists(ConfigCms::$table)){
    /**
     * Recupera infomações do banco de dados
     * @var object
     */
    $config = ConfigCms::first();
    if(isset($config->id)){
      /**
       * Retorna o titulo da aplicação (db ou file)
       * @var string
       */
      $routes = (isset($config->routes) and strlen($config->routes)>0)?$config->routes:Config::get('zircon::settings.routes');
    }
  }
}

if(isset($routes) and is_array($routes) and count($routes)>0){
	foreach ($routes as $route) {
		routes($route);
	}
}elseif(isset($routes) and strlen($routes)>0){
	routes($routes);
}

function routes($route){
	if(isset($route) and strlen($route)>0 and is_null(Bundle::path($route))){

        /**
		 * Arquivo que define as rotas do CMS.
		 */
		Route::any($route.'/modulos', function() {
			return Controller::call("zircon::core.modulos@index");
		});

		Route::any($route.'/modulos/(:all?)', function($modulos = '') {
		    $modulos= explode('/', $modulos);
		    $method = isset($modulos[0])?$modulos[0]:'index';
		    array_shift($modulos);
		    return Controller::call("zircon::core.modulos@{$method}", $modulos);
		});

		Route::any($route.'/users', function() {
		    return Controller::call("zircon::core.users@index");
		});

		Route::any($route.'/users/(:all?)', function($route = '') {
		    $route= explode('/', $route);
		    $method = isset($route[0])?$route[0]:'index';
		    array_shift($route);
		    return Controller::call("zircon::core.users@{$method}", $route);
		});

		Route::any($route, function() {
		    return Controller::call("zircon::core.admin@index");
		});

		Route::any($route.'/(:all?)', function($route = '') {
		    $route= explode('/', $route);
		    $method = isset($route[0])?$route[0]:'index';
		    array_shift($route);

		    return Controller::call("zircon::core.admin@{$method}", $route);
		});

        Route::filter('pattern: '.$route.'/','auth');

        Route::filter('auth',function() use($route) {
            if (!Sentry::check()) return Redirect::to($route.'/auth');
        });
	}
}