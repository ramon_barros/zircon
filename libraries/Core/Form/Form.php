<?php namespace Core\Form;

class Form extends \Laravel\Form {
	
	/**
	 * Create a HTML upload input element.
	 *
	 * @param  string  $name
	 * @param  array   $attributes
	 * @return string
	 */
	public static function upload($name, $attributes = array())
	{
		$input = '<div class="fileupload fileupload-new" data-provides="fileupload">
					  <div class="input-append">
					    <div class="uneditable-input span3">
					        <i class="icon-file fileupload-exists"></i> 
					        <span class="fileupload-preview"></span>
					    </div>
					    <span class="btn btn-file">
					        <span class="fileupload-new">Selecione o arquivo</span>
					        <span class="fileupload-exists">Alterar</span>'
					        . static::file($name,$attributes).
					    '</span>
					        <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remover</a>
					  </div>
					</div>';
		return $input;
	}
}