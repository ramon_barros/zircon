<?php

class Zircon_Core_Admin_Controller extends Zircon_Base_Controller {

    public function get_index(){       
        $this->layout->title = $this->admin['title'];
        $this->admin['content'] = View::make($this->template('zircon.home')).Zircon_Model_Modulo::modulos_dashboard();
        $this->admin['subtitle'] = 'Home';
        $this->layout->content = $this->admin['content'];
        $this->layout->menu = View::make($this->template('zircon.menu'))->with($this->admin);
        $this->layout->content = View::make($this->template('zircon.content'))->with($this->admin);
    }

    /**
     * Form Login
     *
     * Exibe o formulário de login ou redireciona para tela
     * inicial do admin caso o usuário já esteja logado
     *
     * @return mixed
     */
    public function get_auth()
    {               
        if (Sentry::check()) {
            $this->messages('info','Você já esta logado');
            return Redirect::to($this->url_index());
        }
        $this->layout->title = $this->admin['title'].' Login';
        $this->admin['subtitle'] = 'Login';
        $this->admin['content'] = '';
        $this->layout->menu = '';
        $this->layout->content = View::make($this->template('zircon.login'))->with($this->admin);
    }

    /**
     * Login User
     *
     * @return redirect
     */
    public function post_auth()
    {
        $input = Input::all();
        if ($input['csrf_token'] == Session::token()) {
            try {
                $valid_login = Sentry::login($input['username'], $input['password'], true);
                if ($valid_login) {
                    $this->messages('sucess','Logado!');
                } else {
                    $this->messages('error','Usuário ou senha inválidos.');
                }
            } catch (Sentry\SentryException $e) {
                $this->messages('error',$e->getMessage());
            }
        } else {
            $this->messages('error','token ERROR');
        }
        return Redirect::to($this->url_login())->with_input();
    }

     /**
     * Logout User
     *
     * @return redirect
     */
    public function get_logout()
    {
        Sentry::logout();
        $this->messages('info','A sessão foi destruida');
        return Redirect::to($this->url_login());
    }

    /**
     * Dashbord Home Page
     *
     * @return view
     */
    public function get_dashboard()
    {
        $this->admin['subtitle'] = 'Home';
        $this->admin['content'] = View::make($this->template('zircon.home')).Zircon_Model_Modulo::modulos_dashboard();
        $this->layout->title  = $this->admin['title'];
        $this->layout->menu = View::make($this->template('zircon.menu'))->with($this->admin);
        $this->layout->content = View::make($this->template('zircon.content'))->with($this->admin);
   }
}