<?php

class Zircon_Core_Modulos_Controller extends Zircon_Base_Controller 
{  
    /*
     * Load method from controller
    */
    public function __call($controller, $parameters)
    {   
        if(preg_match('/(action_|get_|post_)/', $controller))
        {
            $patterns = array('/^action_/', '/^get_/', '/^post_/');
            $controller =  preg_replace($patterns, '', $controller);

            /**
             * Verifica se algum método servirá para requisição ajax
             */
            if(Request::ajax() and in_array($method,Config::get('zircon::settings.ajax')))
            {
                $controller = $this->current_controller_call();
                if($controller){
                    $controller_array = explode('.', $controller);
                   
                    if (count($controller_array)>1 and !isset($parameters[1])) {
                        $method = 'index';
                    } else if(count($controller_array)<=1 and !isset($parameters[0])) { 
                        $method = 'index';
                    }else if(count($controller_array)>1){
                        $method = $parameters[1];
                    }else {
                        $method = $parameters[0];
                    }
                    
                    return Controller::call("zircon::modulos.{$controller}@{$method}" , $parameters);
                }else{
                    return Controller::call("zircon::modulos.{$controller}@{$method}" , $parameters);
                }
            }
            
            if ($controller != 'modulos') {
                
                $controller = $this->controllers_call_bundle;
                $url_controller = $this->current_url_controller();

                if($controller){
                    $controller_array = explode('.', $controller);

                    if (count($controller_array)>1 and !isset($parameters[1])) {
                        $method = 'index';
                    } else if(count($controller_array)<=1 and !isset($parameters[0])) { 
                        $method = 'index';
                    }else if(count($controller_array)>1){
                        $method = $parameters[1];
                        unset($parameters[1]);
                        foreach ($controller_array as $value) {
                            $key = array_search($value, $parameters);
                            if($key!==false){
                                unset($parameters[$key]);
                            }
                        }
                    }else {
                        $method = $parameters[0];
                        unset($parameters[0]);
                    }
                    $call = Controller::call("zircon::modulos.{$controller}@{$method}" , $parameters);
                    $controller = $this->controller;
                }else{
                    if (!isset($parameters[0])) {
                        $method = 'index';
                    } else {
                        $method = $parameters[0];
                        unset($parameters[0]);
                    }
                    $call = Controller::call("zircon::modulos.{$controller}@{$method}" , $parameters);
                }        
                if (!$call->content) {
                    return  $call;
                } elseif (isset($call->content->view) and $call->content->view == "error.404") {
                    return Response::error('404');
                } elseif(isset($call->content->errors)){
                    return View::make($this->template('error.404'))->with('messages',$controller.'@'.$method.' deve retornar $this->admin; ');
                }
                $call = $call->content;
                $call['controller'] = $controller;
                $call['url_controller'] = $url_controller;
                $call['method'] = $method;
                $call['parameters'] = $parameters;
                $call = array_merge($call,Zircon_Model_Modulo::check_modulos_call($call,$controller,$method));
                
                $this->layout->title = $this->admin['title']." | " . $call['subtitle'];
                $this->layout->menu = View::make($this->template('zircon.menu'))->with($call);
                $this->layout->content = View::make($this->template('zircon.content'))->with($call)->nest('content', $call['content'] , $call);
            } else {
               return Response::error('404');
            }
        }else{
            return Response::error('404');
        }
    }

   /**
    * Lista os modulos cadastrados
    * [GET zircon/modulos]
    */
   public function get_index()
    {
        $model = 'Zircon_Model_Modulo';
        $this->layout->title   = $this->admin['title'].' | Modulos';
        $this->admin['content'] = $this->template('zircon.modulo.list');
        //Log::write('info', 'data = '.json_encode($this->data));
        $this->layout->menu = View::make($this->template('zircon.menu'))->with($this->admin);
        $this->layout->content = View::make($this->template('zircon.content'))->with($this->admin)->nest('content', $this->admin['content'] , $this->admin );
    }

   /**
    * Função para edição dos modulos cadastrados
    * @param  integer $id id do modulo cadastrado/instalado
    * [GET zircon/modulos/edit/id]
    */
   public function get_edit($id=null){
        /**
         * Modulo para manupulação do banco de dados
         * [bundles/zircon/libraries/Core/Model/Modulo.php]
         * @var object
         */
        $model = 'Zircon_Model_Modulo';

        /**
         * Titulo da página definido em:
         * [bundles/zircon/config/settings.php]
         * @var array
         */
        $this->layout->title   = $this->admin['title'];
        $this->admin['content'] = $this->template('zircon.form');

        /**
         * Busca as informações do registro pela chave primária
         */
        if (!($this->admin['data'] = $model::find($id))) {
            return Response::error('404');
        }

        $this->layout->menu = View::make($this->template('zircon.menu'))->with($this->admin);
        $this->layout->content = View::make($this->template('zircon.content'))->with($this->admin)->nest('content', $this->admin['content'] , $this->admin );      
    }

    public function get_add()
    {
        /**
         * Modulo para manupulação do banco de dados
         * [bundles/zircon/libraries/Core/Model/Modulo.php]
         * @var object
         */
        $model = 'Zircon_Model_Modulo';

        /**
         * Titulo da página definido em:
         * [bundles/zircon/config/settings.php]
         * @var array
         */
        $this->layout->title   = $this->admin['title'];
        $this->admin['content'] = $this->template('zircon.form');
                
        $obj =  $model::columns();
        $data = new StdClass();
        foreach ($obj as $key => $val) {
            $data->$key = false;
        }
        $this->admin['data'] = $data;

        $this->layout->menu = View::make($this->template('zircon.menu'))->with($this->admin);
        $this->layout->content = View::make($this->template('zircon.content'))->with($this->admin)->nest('content', $this->admin['content'] , $this->admin );      

    }


    /**
     * Cadastra/Atualiza os modulos
     * @return object redireciona para a uri informada
     * [POST zircon/modulos/save]
     */
    
   public function post_save(){
        /**
         * Modulo para manupulação do banco de dados
         * [bundles/zircon/libraries/Core/Model/Modulo.php]
         * @var object
         */
        $model = 'Zircon_Model_Modulo';
      $input = Input::all();
      $new = false;
      $relationship = false;
      if(isset($input[$model::$key]) and strlen($input[$model::$key])>0){
        if (!($data = $model::find($input[$model::$key]))) {
          $data = new $model();
          $new = true;
        }
        $table = $data->table();
      }else{
        $data = new $model();
        $new = true;
        $table = $data->table();
      }
      
      $validation = Validator::make($input, $model::rules());

      if ($validation->fails()) {
          $this->admin['error'] =  $validation->errors->all();
          $this->messages('error',$this->admin['error']);
      } else {
         $fields = array();

         $db = Config::get('database.default');
         switch ($db) {
           case 'pgsql':
             $sql = "SELECT column_name FROM information_schema.columns WHERE table_name = '{$table}'";
             //$lastId = DB::connection('pgsql')->pdo->lastInsertId();
             
              foreach (DB::query($sql) as $field) {
                  $fields[] = $field->column_name;
              }
             break;
           case 'sqlite':
             $sql = "SELECT column_name FROM information_schema.columns WHERE table_name = '{$table}'";
             //$lastId = DB::connection('sqlite')->pdo->lastInsertId();
             break;
           case 'sqlsrv':
             $sql = "SELECT column_name FROM information_schema.columns WHERE table_name = '{$table}'";
             //$lastId = DB::connection('sqlsrv')->pdo->lastInsertId();
             break;
           case 'mysql':
           default:
             $sql = "SHOW FIELDS FROM {$table}";
             //$lastId = DB::connection('mysql')->pdo->lastInsertId();
              foreach (DB::query($sql) as $field) {
                  $fields[] = $field->field;
              }
             break;
         }
                
          foreach ($input as $key => $value) {
              
              if($key == "csrf_token") continue;
              if($key == $model::$key) continue;

                if (in_array($key, $fields)) {

                    if ( in_array($key, Config::get('zircon::settings.uploader'))) {
                        $data->$key = \Dash\Upload::save($key);

                    } elseif(gettype($value) == 'string') {
                        if($key == 'slug') {
                            $data->$key = Str::slug($value);
                        } else {
                            $data->$key = $value;
                        }
                    }
                } else {
                    $relationship = true;
                }

          }
          
          $data->save();
          if ($relationship) {
              $lastId = $new?$lastId = DB::connection('mysql')->pdo->lastInsertId():$input[$model::$key];
              $data = $model::find($lastId);
              foreach ($input as $key => $value) {
                  if($key == "csrf_token") continue;
                  if (!in_array($key, $fields)) {
                      $data->$key()->sync(is_array($value)?$value:array($value));
                  }
              }
          }

          if(isset($model::$messages) and is_array($model::$messages)){
            $type = 'success';//$model::$messages['edit']['type'];
            $messages = '';//$model::$messages['edit']['msg'];
          }else{
            $type = 'info';
            $messages = 'Dados atualizados';
          } 
          $this->messages($type,$messages);
      }
      if(isset($model::$referrer) and $model::$referrer==true){
        return Redirect::to(Request::referrer());
      }else{
        return Redirect::to($this->url_modulos());
      }
   }

   /**
    * Deleta os modulos
    * @return object redireciona para a uri informada
    * [POST zircon/modulos/delete]
    */
   public function post_delete()
    {

        /**
         * Modulo para manipulação do banco de dados
         * [bundles/zircon/libraries/Core/Model/Modulo.php]
         * @var object
         */
        $model = 'Zircon_Model_Modulo';

        $input = Input::all();

        if (($data = $model::find($input[$model::$key]))) {

            $data->delete();
            $this->messages('info','Registro deletado');
            return Redirect::to(Request::referrer());
        } else {
            $this->messages('error','Erro ao deletar o registro');
            return Redirect::to(Request::referrer());
        }

    }

   /**
    * Instala o modulo cadastrado, e cria o banco de dados
    * @param  integer $id id do modulo a ser instalado
    * @return  object
    */
   public function get_install($id=null){
         /**
         * Modulo para manipulação do banco de dados
         * [bundles/zircon/libraries/Core/Model/Modulo.php]
         * @var object
         */
        $model = 'Zircon_Model_Modulo';

        $instalado = Zircon_Modulo_Install::get_modulo_install($id);
        if(!$instalado){
            $modulo = $model::modulo_install($id);    
            if($modulo){
                $this->messages('info','Modulo instalado com sucesso!');
             }else{
                $this->messages('error','Não foi possivel instalar o modulo!');
             }           
        }else{
            $this->messages('error','O modulo já esta instalado!');
        }
        return Redirect::to($this->url_modulos());
    }

    /**
     * Remove o banco de dados do modulo
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function get_uninstall($id=null){
        /**
         * Modulo para manipulação do banco de dados
         * [bundles/zircon/libraries/Core/Model/Modulo.php]
         * @var object
         */
        $model = 'Zircon_Model_Modulo';

        $instalado = Zircon_Modulo_Install::get_modulo_install($id);
        if($instalado){
            $model::modulo_uninstall($id);
            $this->messages('info','Modulo desistalado com sucesso!');
        }else{
            $this->messages('error','O modulo já esta desinstalado!');
        }
        return Redirect::to($this->url_modulos());
    }
    
    /**
     * Recria as permissões do modulo
     * @param  integer $id Id do modulo
     * @return [type]     [description]
     */
    public function get_permissions($id=null){
        /**
         * Modulo para manipulação do banco de dados
         * [bundles/zircon/libraries/Core/Model/Modulo.php]
         * @var object
         */
        $model = 'Zircon_Model_Modulo';

        $instalado = Zircon_Modulo_Install::get_modulo_install($id);
        if($instalado){
            $model::reload_modulo_permission($id);
            $this->messages('info','Permissões recriadas com sucesso!');
        }else{
            $this->messages('error','Não foi possível recriar as permissões!');
        }
        return Redirect::to($this->url_modulos());
    }
}