<?php

class Zircon_Core_Users_Controller extends Zircon_Base_Controller {

    public function get_index(){
        
        /**
         * Modulo para manupulação do banco de dados
         * [bundles/zircon/models/Admin/users.php]
         * @var object
         */
        $model = 'Zircon_Model_Users';

        $this->layout->title   = $this->admin['title'].' | Usuários';
        $this->admin['content'] = $this->template('zircon.users.list'); 
        $this->admin['buttons'] = $model::buttons();

        $this->layout->menu = View::make($this->template('zircon.menu'))->with($this->admin);
        $this->layout->content = View::make($this->template('zircon.content'))->with($this->admin)->nest('content', $this->admin['content'] , $this->admin );
     }

    public function get_edit($id=null)
    {
        /**
         * Modulo para manupulação do banco de dados
         * [bundles/zircon/models/Admin/users.php]
         * @var object
         */
        $model = 'Zircon_Model_Users';

        $this->layout->title   = $this->admin['title'].' | Usuários';
        $this->admin['content'] = $this->template('zircon.form');
        $this->admin['keyValue'] = $id;
        if (!($this->admin['data'] = $model::get_user($id))) {
            return Response::error('404');
        }
    
        $this->layout->menu = View::make($this->template('zircon.menu'))->with($this->admin);
        $this->layout->content = View::make($this->template('zircon.content'))->with($this->admin)->nest('content', $this->admin['content'] , $this->admin );
    }

    public function get_permissions($id=null){
        /**
         * Modulo para manupulação do banco de dados
         * [bundles/zircon/models/Admin/users.php]
         * @var object
         */
        $model = 'Zircon_Model_Users';

        $this->layout->title   = $this->admin['title'].' | Usuários';
        $this->admin['content'] = $this->template('zircon.users.permissions');
        $this->admin['data'] = (array)json_decode(Zircon_Model_Users::get_user_permissions($id));
        $this->admin['keyValue'] = $id;
        $this->admin['modulos'] = Zircon_Model_Modulo::modulos_permissions();
        $this->admin['user'] = Zircon_Model_Users::get_user(Sentry::user((int)$id)->get('id'));
        $this->admin['groups'] = Sentry::user((int)$id)->groups();
        /*
        if (!($this->admin['data'] = $model::get_user($id))) {
            return Response::error('404');
        }
        */
        /*
        $this->data = array(
             'title'         => $this->admin['title']
            ,'subtitle'      => 'Permissões'
            ,'content'       => $this->template('zircon.users.permissions')
            ,'submenu'       => array()
            ,'menu'          => $this->admin['menu']
            ,'addMenu'       => $this->admin['addMenu']
            ,'data'          => (array)json_decode(Zircon_Model_Users::get_user_permissions($id))
            ,'modelKey'      => $model::key()
            ,'keyValue'      => $id
            ,'modulos'       => Zircon_Model_Modulo::modulos_permissions()
            ,'controller'    => 'users'
            ,'method'        => ''
            ,'parameters'    => ''
            ,'user'          => Zircon_Model_Users::get_user(Sentry::user((int)$id)->get('id'))
            ,'groups'        => Sentry::user((int)$id)->groups()
            ,'url_save'      => $this->url_save()
        );
        */
       
        $this->layout->menu = View::make($this->template('zircon.menu'))->with($this->admin);
        $this->layout->content = View::make($this->template('zircon.content'))->with($this->admin)->nest('content', $this->admin['content'] , $this->admin );
    }

    public function get_add()
    {
        /**
         * Modulo para manupulação do banco de dados
         * [bundles/zircon/models/Admin/users.php]
         * @var object
         */
        $model = 'Zircon_Model_Users';

        /**
         * Titulo da página definido em:
         * [bundles/zircon/config/settings.php]
         * @var array
         */
        $this->layout->title   = $this->admin['title'];
        $this->admin['content'] = $this->template('zircon.form');
        
        $obj =  $model::columns();
        $data = new StdClass();
        foreach ($obj as $key => $val) {
            $data->$key = false;
        }
        $this->admin['data'] = $data;
        $this->admin['columns'] = $obj;
        /**
         * Dados passados para a view
         * @var array
         */
        /*
        $this->data =  array(
             'title'         => $this->admin['title'] // Titulo da aplicação
            ,'subtitle'      => 'Usuários'  // Subtitulo
            ,'content'       => $this->template('zircon.form')
            ,'submenu'       => array()
            ,'menu'          => $this->admin['menu']
            ,'addMenu'       => $this->admin['addMenu']
            ,'data'          => $data
            ,'modelKey'      => $model::key()
            ,'columns'       => $model::columns()
            ,'controller'    => 'users'
            ,'method'        => ''
            ,'parameters'    => ''
            ,'user'          => Zircon_Model_Users::get_user(Sentry::user()->get('id'))
            ,'url_save'      => $this->url_save()
        );*/
        $uri = URI::segment(3);
        if($uri=='add'){
            unset($this->admin['columns']['edit_password']);
        }
        $this->layout->menu = View::make($this->template('zircon.menu'))->with($this->admin);
        $this->layout->content = View::make($this->template('zircon.content'))->with($this->admin)->nest('content', $this->admin['content'] , $this->admin );
    }

    public function post_delete()
    {
        /**
         * Modulo para manupulação do banco de dados
         * [bundles/zircon/models/Admin/users.php]
         * @var object
         */
        $model = 'Zircon_Model_Users';

        $input = Input::all();

        if ( $data = Sentry::user((int)$input[$model::$key]) ) {
            $data->delete();
            $this->messages('info', 'Registro excluído com sucesso');
            return Redirect::to(Request::referrer());
        } else {
            $this->messages('info', 'Falha ao excluir o registro');
            return Redirect::to(Request::referrer());
        }

    }

    public function post_save()
    {
        /**
         * Modulo para manupulação do banco de dados
         * [bundles/zircon/models/Admin/users.php]
         * @var object
         */
        $model = 'Zircon_Model_Users';
        
        $input = Input::all();
        $new = false;

        $messages = array(
            'required_with' => 'O :attribute deve ser preenchido/selecionado.',
        );
        $validation = Validator::make($input, $model::rules(),$messages);
        if ($validation->fails()) {
            $this->admin['error'] =  $validation->errors->all();
            $this->messages('error', $this->admin['error']);
        } else {
            try {
                $vars = array(
                     'email'     => $input['email']
                    ,'status'    => $input['status']
                    ,'metadata'  => array(
                         'first_name' => $input['first_name']
                        ,'last_name'  => $input['last_name']
                    )
                );
        
                if(isset($input['edit_password']) || strlen($input['id'])<=0){
                    $vars['password'] = $input['password'];
                }

                if(strlen($input[$model::$key])>0){
                    $user = Sentry::user((int)$input[$model::$key]);
                }else{
                    $user = false;
                }
    
                if ($user) {
                    $user->remove_from_group($model::get_user_group((int)$input[$model::$key]));
                    $user->add_to_group($input['grupo']);
                    $result = $user->update($vars);
                } else {
                    $result = Sentry::user()->create($vars);
                    Sentry::user($result)->add_to_group($input['grupo']);
                }
                if ($result) {
                    $this->messages('info', 'Dados atualizados');
                    return Redirect::to($this->url_users());
                } else {
                    $this->messages('error', 'Erro');
                }
            } catch (Sentry\SentryException $e) {
                $this->messages('error', $e->getMessage());
            }
        }
        return Redirect::to(Request::referrer());
    }

    public function post_permissions(){
        /**
         * Modulo para manupulação do banco de dados
         * [bundles/zircon/models/Admin/users.php]
         * @var object
         */
        $model = 'Zircon_Model_Users';
        $permissions = array();

        /**
         * Recupera os dados enviados para o formulário
         * @var array
         */
        $input = Input::all();
        
        /**
         * Guarda o grupo
         * @var string
         */
        $grupo = $input['grupo'];
        /**
         * Guarda o id do usuário a ser alterado
         * @var integer
         */
        $id = (int)$input['id'];

        /**
         * Remove o grupo e o id do POST
         */
        unset($input['grupo']);
        unset($input['id']);

        /**
         * Guarda o grupo que o usuário pertence
         * (superuser,is_admin e user)
         */
        $permissions[$grupo]=1;

        /**
         * Recupera as permissões dos modulos instalados
         * e verifica se existe nas permissões enviadas
         */        
        foreach (Zircon_Model_Modulo::modulos_permissions() as $modulo => $rules) {
            foreach($rules as $permission=>$value){
                if(array_key_exists('superuser',$rules) or array_key_exists('is_admin',$rules) or array_key_exists('user',$rules)){
                    if($permission==$grupo){
                        foreach ($rules[$permission] as $groups=>$per){
                          if(is_array($rules)){
                            if(in_array($groups,array_keys($input))){
                                $permissions[$groups]=1;
                            }else{
                                $permissions[$groups]=0;
                            }
                          }
                        }
                    }
                }else{
                    if(is_array($rules)){
                        if(in_array($permission,array_keys($input))){
                            $permissions[$permission]=1;
                        }else{
                            $permissions[$permission]=0;
                        }
                    }
                }                
            }
        }

        try
        {
            /**
             * Atualiza as permissões individual do usuario
             */
            if (Sentry::user($id)->update_permissions($permissions))
            {
                /**
                 * Mostra mensagem de sucesso
                 */
                $this->messages('sucess','Permissões atualizadas com sucesso!');
            }else{
                /**
                 * Mostra mensagem de erro
                 */
                $this->messages('error','Não foi possível atualizar as permissões do usuário.');
            }
        }
        catch (Sentry\SentryException $e)
        {
            $errors = $e->getMessage(); // catch errors
            $this->messages('error',$errors);
        }
        /**
         * Retorna para a listagem dos usuários
         */
        return Redirect::to($this->url_users());
    }
}