<?php

class Zircon_Modulos_Fotos_Controller extends Zircon_Base_Controller {

    public function get_index()
    {
        $model = $this->model;      
        $this->admin['content'] = $this->template('zircon.fotos.list');
        $this->admin['data'] = $model::data();
        $this->admin['module'] = URI::segment(5);
        $this->admin['rel_id'] = URI::segment(6);
        if((!isset($this->admin['module']) and strlen($this->admin['module'])<=0) 
        and (!isset($this->admin['rel_id']) and strlen($this->admin['rel_id'])<=0)){
            $this->messages('error','Este modulo não pode ser acessado diretamente!');
        }
        if(isset($this->admin['data']['error']) and $this->admin['data']['error']===true){
            $this->messages('error',$this->admin['data']['error_msg']);
            $this->admin['data'] = array();
            $this->admin['module'] = '';
            $this->admin['rel_id'] = '';
        }
       
        return $this->admin;
    }

    public function post_save(){
        $model = $this->model;
        $input = Input::all();
        
        $validation = Validator::make($input, $model::rules());

        if ($validation->fails()) {
            $this->admin['error'] =  $validation->errors->all();
            $this->messages('error',$this->admin['error']);
        }else{

            $columns = array(
                             'ordem'    => $input['ordem']
                            ,'legenda'  => $input['legenda']
                            ,'foto'     => $this->upload('foto')
                        );
            

            $id = strlen($input[$model::key()])>0?$input[$model::key()]:0;
            
            $select = DB::table($model::$table)->where($model::key(), '=', $id)->first();
            
            if ($select) {
                $columns['updated_at'] = date('Y-m-d H:m:i');
                DB::table($model::$table)
                        ->where($model::key(), '=', $input[$model::key()])
                        ->update($columns);
                $this->messages('success','Foto atualizada com sucesso!');
            }else{
                $columns['created_at'] = date('Y-m-d H:m:i');
                $columns['updated_at'] = date('Y-m-d H:m:i');
                $id = DB::table($model::$table)->insert_get_id($columns);
                $this->messages('success','Foto cadastrada com sucesso!');
            }

            $data = $model::find($id);
            foreach ($input as $key => $value) {
              if($key == "csrf_token") continue;
              if($key == $model::$key) continue;
              if (!in_array($key, array_keys($columns))) {
                $data->$key()->sync(is_array($value)?$value:array($value));
              }
            }
        }
        return Redirect::to(Request::referrer());
    }

    public function post_update_order(){
        $this->filter('before', 'auth');
        $decoded = json_decode(Input::get('data'));
        if($decoded){
            foreach($decoded as $ordem=>$id){
                if($img = Fotos::find($id)){
                   $img->ordem = $ordem;
                   $img->save();
                }
            }
        }
        return true;
    }
}