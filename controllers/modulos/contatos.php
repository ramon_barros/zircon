<?php

/**
 * Modulo de exemplo
 */
class Zircon_Modulos_Contatos_Controller extends Zircon_Base_Controller {

	public function get_index(){
		$model = $this->model;
		$this->admin['content'] = $this->template('zircon.list');
		$this->admin['buttons'] = $model::buttons();
		return $this->admin;
	}

	public function get_dashboard(){
		return View::make($this->template('modulos.contatos.dashboard'));
	}
}