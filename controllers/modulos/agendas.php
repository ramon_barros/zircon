<?php

/**
 * Modulo de exemplo
 */
class Zircon_Modulos_Agendas_Controller extends Zircon_Base_Controller {

	public function get_index(){
		$this->admin['content'] = $this->template('zircon.list');
		return $this->admin;
	}
}