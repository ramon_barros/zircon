<?php

/**
 * Modulo de exemplo
 */
class Zircon_Modulos_ConfigCms_Controller extends Zircon_Base_Controller {

	public function get_dashboard(){
		return View::make($this->template('modulos.configcms.dashboard'));
	}
}